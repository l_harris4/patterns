#include "cCamera.h"
#include <glm\glm.hpp>
#include <glm\mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp> // glm::translate, glm::rotate, glm::scale, glm::perspective
#include <glm/gtc/type_ptr.hpp> // glm::value_ptr

cCamera::cCamera()
{
	type = eMode::FLY_CAMERA;
}

glm::mat4 cCamera::getViewMatrix()
{
	// Based on the mode, calculate the view matrix
	switch (type)
	{
	case eMode::FOLLOW_CAMERA:
		glm::mat4 matView = glm::lookAt(this->modelData.position,
			this->followTarget->GetModelData()->position,
			glm::vec3(0.0f, 1.0f, 0.0f)); // UP
		return matView;
		break;
	case eMode::FLY_CAMERA:
		// Use same process as with drawing an object:
		// Combine transform with rotation, and return that
		glm::mat4 matCamView = glm::mat4(1.0f);

		glm::mat4 trans = glm::mat4x4(1.0f);
		trans = glm::translate(trans, this->modelData.position);
		matCamView = matCamView * trans;

		// Like many things in GML, the conversion is done in the constructor
		glm::mat4 postRotQuat = glm::mat4(this->modelData.qOrientation);
		matCamView = matCamView * postRotQuat;

		return matCamView;
		break;
	}
	// You need to check what you are doign with your life!
	// return the identity matrix
	return glm::mat4(1.0f);
}
