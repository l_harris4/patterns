#ifndef CFOLLOWCURVECOMMAND_HG
#define CFOLLOWCURVECOMMAND_HG
#include "cCommand.h"
#include <vector>

enum FollowCurveType
{
	SPEED1POINT = 1,
	TIME1POINT = 2,
	SPEED2POINT = 3,
	TIME2POINT = 4
};

class cFollowCurveCommand : public cCommand
{
public:
	cFollowCurveCommand();
	cFollowCurveCommand(cGameObject * theObject, float speed, glm::vec3 destination, glm::vec3 offPoint1);
	cFollowCurveCommand(cGameObject * theObject, float speed, glm::vec3 destination, glm::vec3 offPoint1, glm::vec3 offPoint2);

	cFollowCurveCommand(cGameObject * theObject, float speed, glm::vec3 destination, glm::vec3 offPoint1, int easeInPoints, int easeOutPoints);
	cFollowCurveCommand(cGameObject * theObject, float speed, glm::vec3 destination, glm::vec3 offPoint1, int easeInPoints, int easeOutPoints, float easePercentage);
	cFollowCurveCommand(cGameObject * theObject, float speed, glm::vec3 destination, glm::vec3 offPoint1, glm::vec3 offPoint2, int easeInPoints, int easeOutPoints);
	cFollowCurveCommand(cGameObject * theObject, float speed, glm::vec3 destination, glm::vec3 offPoint1, glm::vec3 offPoint2, int easeInPoints, int easeOutPoints, float easePercentage);

	cFollowCurveCommand(cGameObject * theObject, glm::vec3 destination, glm::vec3 offPoint1, float time);
	cFollowCurveCommand(cGameObject * theObject, glm::vec3 destination, glm::vec3 offPoint1, glm::vec3 offPoint2, float time);
	cFollowCurveCommand(float delay, cGameObject * theObject, float speed, glm::vec3 destination, glm::vec3 offPoint1);
	cFollowCurveCommand(float delay, cGameObject * theObject, float speed, glm::vec3 destination, glm::vec3 offPoint1, glm::vec3 offPoint2);
	cFollowCurveCommand(float delay, cGameObject * theObject, glm::vec3 destination, glm::vec3 offPoint1, float time);
	cFollowCurveCommand(float delay, cGameObject * theObject, glm::vec3 destination, glm::vec3 offPoint1, glm::vec3 offPoint2, float time);

	cFollowCurveCommand(float delay, cGameObject * theObject, float speed, glm::vec3 destination, glm::vec3 offPoint1, int easeInPoints, int easeOutPoints);
	cFollowCurveCommand(float delay, cGameObject * theObject, float speed, glm::vec3 destination, glm::vec3 offPoint1, int easeInPoints, int easeOutPoints, float easePercentage);
	cFollowCurveCommand(float delay, cGameObject * theObject, float speed, glm::vec3 destination, glm::vec3 offPoint1, glm::vec3 offPoint2, int easeInPoints, int easeOutPoints);
	cFollowCurveCommand(float delay, cGameObject * theObject, float speed, glm::vec3 destination, glm::vec3 offPoint1, glm::vec3 offPoint2, int easeInPoints, int easeOutPoints, float easePercentage);

	~cFollowCurveCommand();
	virtual void Update();
	virtual void Update(float deltaTime);
	FollowCurveType type;

	float speed;
	float allottedTime;
	float consumedTime;
	glm::vec3 offPoint1;
	glm::vec3 offPoint2;
	glm::vec3 destination;
	glm::vec3 startingPoint;
	virtual cCommand* genCopy();
	virtual void setAffectedObject(cGameObject * gameObject);
	std::vector<glm::vec3> speedPoints;

	int pointNum = 1;

	//defaults 
	int easeInPoints = 20;
	int easeOutPoints = 10;
	float percentageEaseStep = 0.01;
	float percentageRegularstep = 0.02;
	
	



};

#endif // !CFOLLOWCURVECOMMAND_HG

