#include <iostream>
#include "cGameObject.h"
#include "Utilities.h"
#include "cCollisionManager.h"
#include "cCollision.h"
#include "Physics.h"
#include "cVAOMeshManager.h"
#include "cModelData.h"
#include <glm/glm.hpp>

//ctor
cGameObject::cGameObject()
{
	//set default values
	this->modelData.scale = 1.0f;
	this->modelData.position = glm::vec3(0.0f);
	//this->modelData.orientation = glm::vec3(0.0f);
	this->modelData.orientation2 = glm::vec3(0.0f);
	this->modelData.qOrientation = glm::quat(glm::vec3(0.0f, 0.0f, 0.0f));

	this->modelData.diffuseColour = glm::vec4( 0.0f, 0.0f, 0.0f, 1.0f );
	this->modelData.origColour = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
	this->modelData.worldMatrix = glm::mat4x4(1.0f);

	this->modelData.bIsWireFrame = false;

	//attributes needed for the lua scripts
	this->moveTarget = glm::vec3(0.0f);
	this->rotationTarget = glm::vec3(0.0f);
	moving = false;
	rotating = false;
	cGameObject* followTarget = nullptr;
	following = false;
	moveTime = 0.0f;
	rotationSpeed = 0.0f;

	return;
}

cGameObject::~cGameObject()
{
	return;
}

void cGameObject::Update(std::vector< iGameObject* >*  g_vecGameObjects, double deltaTime)
{

}

void cGameObject::MoveTo(glm::vec3 target, float time)
{
	moveTime = time;
	moveTarget = target;
	moving = true;
}

void cGameObject::RotateTo(glm::vec3 rotationT, float time)
{
	rotationTarget = glm::vec3(glm::radians(rotationT.x), glm::radians(rotationT.y), glm::radians(rotationT.z));
	rotating = true;
	rotationSpeed = time;
}

void cGameObject::Follow(cGameObject * target, float distance, float followT)
{
	followTarget = target;
	following = true;
	followingDistance = distance;
	followSpeed = followT;
}

void cGameObject::overwriteQOrientationFormEuler(glm::vec3 eulerAxisOrientation)
{
	// Calcualte the quaternion represnetaiton of this Euler axis
	// NOTE: We are OVERWRITING this..
	this->modelData.qOrientation = glm::quat(eulerAxisOrientation);

	return;
}

void cGameObject::adjustQOrientationFormDeltaEuler(glm::vec3 eulerAxisOrientChange)
{
	// How do we combine two matrices?
	// That's also how we combine quaternions...

	// So we want to "add" this change in oriention
	glm::quat qRotationChange = glm::quat(eulerAxisOrientChange);

	// Mulitply it by the current orientation;
	this->modelData.qOrientation = this->modelData.qOrientation * qRotationChange;

	return;
}

cModelData * cGameObject::GetModelData()
{
	return &modelData;
}

std::string cGameObject::GetType()
{
	return this->typeOfObject;
}

void cGameObject::SetType(std::string value)
{
	this->typeOfObject = value;
}
