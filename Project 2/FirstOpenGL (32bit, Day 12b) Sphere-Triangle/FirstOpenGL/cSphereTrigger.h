#ifndef CSPHERETRIGGER_HG
#define CSPHERETRIGGER_HG
#include "cLocationTrigger.h"


class cSphereTrigger : public cLocationTrigger
{
public:
	glm::vec3 centerPosition;
	float radius;
	cSphereTrigger(cCommandGroup commandGroup, glm::vec3 centerPos, float radius);
	cSphereTrigger(glm::vec3 centerPos, float radius);
	virtual bool InLocation(glm::vec3 position);
};


#endif // !CSPHERETRIGGER_HG