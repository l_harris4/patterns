#include "cSphereTrigger.h"
#include <glm\geometric.hpp>

cSphereTrigger::cSphereTrigger(cCommandGroup commandGroup, glm::vec3 centerPos, float radius)
{
	this->commandGroup = commandGroup;
	this->centerPosition = centerPos;
	this->radius = radius;
	this->type = "sphere";
}

cSphereTrigger::cSphereTrigger(glm::vec3 centerPos, float radius)
{
	this->centerPosition = centerPos;
	this->radius = radius;
	this->type = "sphere";
}

bool cSphereTrigger::InLocation(glm::vec3 position)
{
	if (glm::distance(position, centerPosition) < radius)
	{
		return true;
	}
	else
	{
		return false;
	}
}
