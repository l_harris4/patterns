#ifndef CBOXTRIGGER_HG
#define CBOXTRIGGER_HG
#include "cLocationTrigger.h"

class cBoxTrigger : public cLocationTrigger
{
public:
	glm::vec3 center;
	float halfSize;
	cBoxTrigger(cCommandGroup commandGroup, glm::vec3 center, float halfSize);
	virtual bool InLocation(glm::vec3 position);
};
#endif // !CBOXTRIGGER_HG
