#ifndef CLOCATIONTRIGGER
#define CLOCATIONTRIGGER
#include <glm\vec3.hpp>
#include "cCommandGroup.h"

class cLocationTrigger {
public:
	cLocationTrigger();
	virtual ~cLocationTrigger();
	virtual bool InLocation(glm::vec3 position);
	cCommandGroup commandGroup;
	std::string type;

};
#endif // !CLOCATIONTRIGGER

