#include "cCollisionManager.h"
#include "cCollision.h"
#include "cTriangle.h"
#include "cCollisionManagerImp.h"

cCollisionManager::cCollisionManager()
{
	// Create the actual implementation
	this->pImp = new cCollisionManagerImp();
}

cCollisionManager::~cCollisionManager()
{
	// Delete the actual implementation
	delete this->pImp;
}

void cCollisionManager::CalculateCollisions()
{
	this->pImp->CalculateCollisions();
}

void cCollisionManager::AddCollision(cCollision collisionData)
{
	this->pImp->AddCollision(collisionData);
}

void cCollisionManager::ClearCollisions()
{
	this->pImp->ClearCollisions();
}
