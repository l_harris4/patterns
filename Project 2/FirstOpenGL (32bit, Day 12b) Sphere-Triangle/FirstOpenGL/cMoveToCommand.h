#ifndef CMOVETOCOMMAND_HG
#define CMOVETOCOMMAND_HG

#include "cCommand.h"
#include "cLocationTrigger.h"
#include <vector>

enum MoveToCommandType {
	SPEEDTYPE = 1,
	TIMETYPE = 2,
	INSTANT = 3
};

class cMoveToCommand : public cCommand
{
public:
	cMoveToCommand();
	cMoveToCommand(cGameObject * theObject, glm::vec3 destination,float maxSpeed);
	cMoveToCommand(cGameObject * theObject, float inTime, glm::vec3 destination);
	cMoveToCommand(cGameObject * theObject, glm::vec3 destination);
	cMoveToCommand(cGameObject * theObject, glm::vec3 destination, float maxSpeed, int easeInSteps, int easeOutSteps);
	cMoveToCommand(cGameObject * theObject, glm::vec3 destination, float maxSpeed, int easeInSteps, int easeOutSteps,
	float easeStepsPercentage);

	cMoveToCommand(float delay, cGameObject * theObject, glm::vec3 destination, float maxSpeed);
	cMoveToCommand(float delay, cGameObject * theObject, float inTime, glm::vec3 destination);
	cMoveToCommand(float delay, cGameObject * theObject, glm::vec3 destination);

	cMoveToCommand(float delay, cGameObject * theObject, glm::vec3 destination, float maxSpeed, int easeInSteps, int easeOutSteps);
	cMoveToCommand(float delay, cGameObject * theObject, glm::vec3 destination, float maxSpeed, int easeInSteps, int easeOutSteps,
		float easeStepsPercentage);

	void fillOutSpeedPoints();

	float totalTime;
	float consumedTime;
	float speed;
	float speedIncrease;
	float maxSpeed;
	float startingDistance;
	float timeToFull;
	float distancePortion;
	virtual cCommand* genCopy();
	virtual void setAffectedObject(cGameObject * gameObject);

	glm::vec3 destination;
	glm::vec3 startingPos;
	virtual void Update();
	virtual void Update(float deltaTime);
	MoveToCommandType type;

	//defaults
	int easeInPoints = 40;
	int easeOutPoints = 10;
	float percentageEaseStep = 0.01;
	float percentageRegularstep = 0.02;
	std::vector<glm::vec3> speedPoints;
	int pointNum = 1;

};
#endif // !CMOVETOCOMMAND_HG
