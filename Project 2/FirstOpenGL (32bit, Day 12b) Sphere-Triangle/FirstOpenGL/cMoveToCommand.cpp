#include "cMoveToCommand.h"
#include <glm/geometric.hpp>
#include <glm/glm.hpp>
#include "cGameObject.h"
#include <stdio.h>      /* printf */
#include <math.h> 
#include <iostream>
#include "cGameObjectMediator.h"
#include <algorithm>


cMoveToCommand::cMoveToCommand()
{
}

void cMoveToCommand::fillOutSpeedPoints()
{
	speedPoints.clear();
	float tempTime;
	for (float i = 0; i < easeInPoints* percentageEaseStep;)
	{
		tempTime = i;

		float distance = startingDistance - (startingDistance* tempTime);

		this->speedPoints.push_back(destination + (glm::normalize(startingPos - destination) * distance));

		i += percentageEaseStep;
	}

	float percent = easeInPoints* percentageEaseStep;
	tempTime = percent;// *totalTime;
	float distance = startingDistance - (startingDistance* tempTime);
	this->speedPoints.push_back(destination + (glm::normalize(startingPos - destination) * distance));

	percent = 1 - (easeOutPoints * percentageEaseStep);
	tempTime = percent;// *totalTime;
	distance = startingDistance - (startingDistance* tempTime);
	this->speedPoints.push_back(destination + (glm::normalize(startingPos - destination) * distance));

	for (float i = 1 - (easeOutPoints * percentageEaseStep); i < 1; )
	{
		tempTime = i;

		float distance = startingDistance - (startingDistance* tempTime);

		this->speedPoints.push_back(destination + (glm::normalize(startingPos - destination) * distance));

		i += percentageEaseStep;
	}
}
cMoveToCommand::cMoveToCommand(cGameObject * theObject, glm::vec3 destination, float maxSpeed)
{
	this->objectBeingAffected = theObject;
	this->destination = destination;
	this->speed = 0;
	this->maxSpeed = maxSpeed;
	//Calculate what the speed increase would be for the ease in
	this->startingPos = objectBeingAffected->modelData.position;
	// ease out stuff
	startingDistance = glm::distance(destination, objectBeingAffected->modelData.position);
	//find 10 percent of that distance
	distancePortion = startingDistance * 0.1;

	//this->speedIncrease =
	this->type = MoveToCommandType::SPEEDTYPE;

	//timeToFull = (startingDistance / maxSpeed) * 0.01;
	this->totalTime = startingDistance / maxSpeed;
	
	easeInPoints = 40;
	easeOutPoints = 40;

	fillOutSpeedPoints();

}

cMoveToCommand::cMoveToCommand(cGameObject * theObject, float inTime, glm::vec3 destination)
{
	this->objectBeingAffected = theObject;
	this->destination = destination;
	this->totalTime = inTime;
	this->consumedTime = 0;


	this->startingPos = objectBeingAffected->modelData.position;
	this->startingDistance = glm::distance(destination, objectBeingAffected->modelData.position);
	this->type = MoveToCommandType::TIMETYPE;
}

cMoveToCommand::cMoveToCommand(cGameObject * theObject, glm::vec3 destination)
{
	this->objectBeingAffected = theObject;
	this->destination = destination;
	this->type = MoveToCommandType::INSTANT;
}

cMoveToCommand::cMoveToCommand(cGameObject * theObject, glm::vec3 destination, float maxSpeed, int easeInSteps, int easeOutSteps)
{
	this->objectBeingAffected = theObject;
	this->destination = destination;
	this->speed = 0;
	this->maxSpeed = maxSpeed;
	//Calculate what the speed increase would be for the ease in
	this->startingPos = objectBeingAffected->modelData.position;
	// ease out stuff
	startingDistance = glm::distance(destination, objectBeingAffected->modelData.position);
	//find 10 percent of that distance
	distancePortion = startingDistance * 0.1;

	//this->speedIncrease =
	this->type = MoveToCommandType::SPEEDTYPE;

	//timeToFull = (startingDistance / maxSpeed) * 0.01;
	this->totalTime = startingDistance / maxSpeed;

	easeInPoints = easeInSteps;
	easeOutPoints = easeOutSteps;

	fillOutSpeedPoints();
}

cMoveToCommand::cMoveToCommand(cGameObject * theObject, glm::vec3 destination, float maxSpeed, int easeInSteps, int easeOutSteps, float easeStepsPercentage)
{
	this->objectBeingAffected = theObject;
	this->destination = destination;
	this->speed = 0;
	this->maxSpeed = maxSpeed;
	//Calculate what the speed increase would be for the ease in
	this->startingPos = objectBeingAffected->modelData.position;
	// ease out stuff
	startingDistance = glm::distance(destination, objectBeingAffected->modelData.position);
	//find 10 percent of that distance
	distancePortion = startingDistance * 0.1;

	//this->speedIncrease =
	this->type = MoveToCommandType::SPEEDTYPE;

	//timeToFull = (startingDistance / maxSpeed) * 0.01;
	this->totalTime = startingDistance / maxSpeed;

	easeInPoints = easeInSteps;
	easeOutPoints = easeOutSteps;
	this->percentageEaseStep = easeStepsPercentage;

	fillOutSpeedPoints();
}

cMoveToCommand::cMoveToCommand(float delay, cGameObject * theObject, glm::vec3 destination, float maxSpeed)
{
	this->delayed = true;
	this->startDelay = delay;
	this->objectBeingAffected = theObject;
	this->destination = destination;
	this->speed = 0;
	this->maxSpeed = maxSpeed;
	//Calculate what the speed increase would be for the ease in
	this->startingPos = objectBeingAffected->modelData.position;
	// ease out stuff
	startingDistance = glm::distance(destination, objectBeingAffected->modelData.position);
	//find 10 percent of that distance
	distancePortion = startingDistance * 0.1;

	//this->speedIncrease =
	this->type = MoveToCommandType::SPEEDTYPE;

	easeInPoints = 40;
	easeOutPoints = 40;

	fillOutSpeedPoints();
}

cMoveToCommand::cMoveToCommand(float delay, cGameObject * theObject, float inTime, glm::vec3 destination)
{
	this->delayed = true;
	this->startDelay = delay;
	this->objectBeingAffected = theObject;
	this->destination = destination;
	this->totalTime = inTime;
	this->consumedTime = 0;
	this->startingPos = objectBeingAffected->modelData.position;
	this->startingDistance = glm::distance(destination, objectBeingAffected->modelData.position);
	this->type = MoveToCommandType::TIMETYPE;
}

cMoveToCommand::cMoveToCommand(float delay, cGameObject * theObject, glm::vec3 destination)
{
	this->delayed = true;
	this->startDelay = delay;
	this->objectBeingAffected = theObject;
	this->destination = destination;
	this->type = MoveToCommandType::INSTANT;
}

cMoveToCommand::cMoveToCommand(float delay, cGameObject * theObject, glm::vec3 destination, float maxSpeed, int easeInSteps, int easeOutSteps)
{
	this->delayed = true;
	this->startDelay = delay;

	this->objectBeingAffected = theObject;
	this->destination = destination;
	this->speed = 0;
	this->maxSpeed = maxSpeed;
	//Calculate what the speed increase would be for the ease in
	this->startingPos = objectBeingAffected->modelData.position;
	// ease out stuff
	startingDistance = glm::distance(destination, objectBeingAffected->modelData.position);
	//find 10 percent of that distance
	distancePortion = startingDistance * 0.1;

	//this->speedIncrease =
	this->type = MoveToCommandType::SPEEDTYPE;

	//timeToFull = (startingDistance / maxSpeed) * 0.01;
	this->totalTime = startingDistance / maxSpeed;

	easeInPoints = easeInSteps;
	easeOutPoints = easeOutSteps;

	fillOutSpeedPoints();
}

cMoveToCommand::cMoveToCommand(float delay, cGameObject * theObject, glm::vec3 destination, float maxSpeed, int easeInSteps, int easeOutSteps, float easeStepsPercentage)
{
	this->delayed = true;
	this->startDelay = delay;

	this->objectBeingAffected = theObject;
	this->destination = destination;
	this->speed = 0;
	this->maxSpeed = maxSpeed;
	//Calculate what the speed increase would be for the ease in
	this->startingPos = objectBeingAffected->modelData.position;
	// ease out stuff
	startingDistance = glm::distance(destination, objectBeingAffected->modelData.position);
	//find 10 percent of that distance
	distancePortion = startingDistance * 0.1;

	//this->speedIncrease =
	this->type = MoveToCommandType::SPEEDTYPE;

	//timeToFull = (startingDistance / maxSpeed) * 0.01;
	this->totalTime = startingDistance / maxSpeed;

	easeInPoints = 40;
	easeOutPoints = 40;
	this->percentageEaseStep = easeStepsPercentage;

	fillOutSpeedPoints();
}

cCommand * cMoveToCommand::genCopy()
{
	cMoveToCommand* newCommand = new cMoveToCommand();

	newCommand->destination = this->destination;
	newCommand->speed = 0;
	newCommand->maxSpeed = this->maxSpeed;
	newCommand->type = this->type;
	newCommand->totalTime = this->totalTime;
	newCommand->consumedTime = 0;
	newCommand->startDelay = this->startDelay;
	newCommand->easeInPoints = this->easeInPoints;
	newCommand->easeOutPoints = this->easeOutPoints;
	newCommand->percentageEaseStep = this->percentageEaseStep;
	newCommand->percentageRegularstep = this->percentageRegularstep;
	newCommand->fillOutSpeedPoints();



	return newCommand;
}

void cMoveToCommand::setAffectedObject(cGameObject * gameObject)
{
	//need to be calculated on the set affected
	this->objectBeingAffected = gameObject;
}

void cMoveToCommand::Update()
{
}

void cMoveToCommand::Update(float deltaTime)
{
	if (Ready(deltaTime)) {
		if (firstUpdate)
		{
			checkIfInLocationTriggerAtStart();
			firstUpdate = false;
			this->startingPos = objectBeingAffected->modelData.position;
			this->startingDistance = glm::distance(destination, objectBeingAffected->modelData.position);
		}

		switch (type)
		{
		case MoveToCommandType::SPEEDTYPE:
		{

			if (pointNum == speedPoints.size())
				complete = true;
			else
			{
				float tempSpeed = maxSpeed;
				//change the speed based on ease in ease out
				//ease in
				if (pointNum <= easeInPoints)
				{
					tempSpeed *= ((float)pointNum / (float)easeInPoints);
				}
				//ease out
				else if (pointNum >= (speedPoints.size() - easeOutPoints))
				{
					tempSpeed *= 1.0f - ((float)(pointNum - (speedPoints.size() - easeOutPoints)) / (float)(easeOutPoints));
				}
				else
				{
					tempSpeed = maxSpeed;
				}


				objectBeingAffected->modelData.position += glm::normalize(speedPoints[pointNum] - objectBeingAffected->modelData.position) * tempSpeed;
				if (glm::distance(objectBeingAffected->modelData.position, speedPoints[pointNum]) < tempSpeed)
				{
					pointNum++;
				}
			}
			//std::cout << "moved: " << speed << std::endl;
			//objectBeingAffected->modelData.position += glm::normalize(destination - objectBeingAffected->modelData.position) * speed;
		}
		break;
		case MoveToCommandType::TIMETYPE:

			if (consumedTime > totalTime)
				complete = true;
			else {
				consumedTime += deltaTime;

				float time = smootherstep(0,
					totalTime,
					consumedTime);

				float distance = startingDistance - (startingDistance* time);

				objectBeingAffected->modelData.position = destination + (glm::normalize(startingPos - destination) * distance);
			}
			break;
		case MoveToCommandType::INSTANT:
			objectBeingAffected->modelData.position = destination;
			complete = true;
			break;
		}

		checkIfInLocationTrigger();
	}

}
