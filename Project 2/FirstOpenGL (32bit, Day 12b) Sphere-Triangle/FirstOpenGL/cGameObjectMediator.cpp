#include "cGameObjectMediator.h"
#include "cSphere.h"
#include "cVAOMeshManager.h"
#include "cCollisionManager.h"
#include <glm\vec4.hpp>
#include "cCollision.h"
#include "Physics.h"
#include <iostream>
#include "cGameObject.h"
#include "cCommand.h"
#include "cCommandGroup.h"
#include "cFollowCommand.h"
#include "cFollowCurveCommand.h"
#include "cMoveToCommand.h"
#include "cOrientToCommand.h"
#include "cCameraTargetCommand.h"
#include "cSphereTrigger.h"
#include "cBoxTrigger.h"
#include "cCamera.h"

extern cVAOMeshManager* g_pVAOManager;
extern cCollisionManager*  g_pCollisionManager;

cGameObjectMediator* cGameObjectMediator::instance = nullptr;

//ctor
cGameObjectMediator::cGameObjectMediator()
{
	state = luaL_newstate();
	luaL_openlibs(state);
}

//dtor
cGameObjectMediator::~cGameObjectMediator()
{
	//delete all the game objects
	for (int i = 0; i < vecGameObjects.size(); i++)
	{
		delete vecGameObjects[i];
	}

	//delete all the commands if any are left
	//delete all the location based triggers
	lua_close(state);
}

cGameObjectMediator * cGameObjectMediator::GetInstance()
{
	if (instance == nullptr)
		instance = new cGameObjectMediator();
	return instance;
}

void cGameObjectMediator::AddToCommands(cCommand *command)
{
	if (cGameObjectMediator::GetInstance()->addingIntoLocationTrigger)
	{
		cGameObjectMediator::GetInstance()->vecLocationTriggers.back()->commandGroup.commands.push_back(command);
		cGameObjectMediator::GetInstance()->addingIntoLocationTrigger = false;
	}
	else
	{
		cGameObjectMediator::GetInstance()->addingCommandGroup->commands.push_back(command);
	}
	
}

//std::vector<iGameObject*>* cGameObjectMediator::GetVecGameObjects()
//{
//	return &vecGameObjects;
//}


int cGameObjectMediator::doubleIt(lua_State* L) {
	int d = lua_tonumber(L, 1);
	//double d1 = lua_tonumber(L, 2);
	//lua_pushnumber(L, d * 2);
	return 1;
}

int cGameObjectMediator::endGroup(lua_State * L)
{
	cGameObjectMediator::GetInstance()->tempCommandLevel -= 1;
	if (cGameObjectMediator::GetInstance()->tempCommandLevel <= 0)
	{
		cGameObjectMediator::GetInstance()->tempCommandLevel = 0;
		//go through the temp vector and add all the commands into the actual commands
		for (int i = 0; i < cGameObjectMediator::GetInstance()->tempVecCommands.size(); ++i)
		{
			cGameObjectMediator::GetInstance()->vecCommands.push_back(
				cGameObjectMediator::GetInstance()->tempVecCommands[i]);
		}
		cGameObjectMediator::GetInstance()->tempVecCommands.clear();
		//dont have to touch cGameObjectMediator::GetInstance()->addingCommandGroup
	}
	else
	{
		//have to touch cGameObjectMediator::GetInstance()->addingCommandGroup
		cGameObjectMediator::GetInstance()->addingCommandGroup =
			cGameObjectMediator::GetInstance()->addingCommandGroup->parentGroup;
	}
	return 1;
}

int cGameObjectMediator::startGroup(lua_State * L)
{
	bool serial = lua_toboolean(L, 1);
	cGameObjectMediator::GetInstance()->tempCommandLevel += 1;

	int commandLevel = cGameObjectMediator::GetInstance()->tempCommandLevel;
	
	cCommandGroup* commandGroup = new cCommandGroup();
	commandGroup->serial = serial;
	//if top level
	if (cGameObjectMediator::GetInstance()->tempCommandLevel <= 1 ) {
		cGameObjectMediator::GetInstance()->tempVecCommands.push_back(commandGroup);
		cGameObjectMediator::GetInstance()->addingCommandGroup =
			cGameObjectMediator::GetInstance()->tempVecCommands.back();
	}
	else
	{
		commandGroup->parentGroup = cGameObjectMediator::GetInstance()->addingCommandGroup;
		cGameObjectMediator::GetInstance()->addingCommandGroup->commands.push_back(commandGroup);		
		cGameObjectMediator::GetInstance()->addingCommandGroup = 
			(cCommandGroup*)cGameObjectMediator::GetInstance()->addingCommandGroup->commands.back();
	}

	
	return 1;
}


int cGameObjectMediator::followDistanceCommand(lua_State * L)
{
	int id = lua_tonumber(L, 1);
	int id2 = lua_tonumber(L, 2);
	float distance = lua_tonumber(L, 3);
	float x = lua_tonumber(L, 4);
	float y = lua_tonumber(L, 5);
	float z = lua_tonumber(L, 6);
	float followTime = lua_tonumber(L, 7);
	cFollowCommand* follow = new cFollowCommand((cGameObject*)cGameObjectMediator::GetInstance()->vecGameObjects[id], distance, (cGameObject*)cGameObjectMediator::GetInstance()->vecGameObjects[id2]
		, glm::vec3(x, y, z), followTime);
	cGameObjectMediator::GetInstance()->AddToCommands(follow);

	return 1;
}

int cGameObjectMediator::followSpeedCommand(lua_State * L)
{
	int id = lua_tonumber(L, 1);
	int id2 = lua_tonumber(L, 2);
	float maxSpeed = lua_tonumber(L, 3);
	float closeDistance = lua_tonumber(L, 4);
	float farDistance = lua_tonumber(L, 5);
	float x = lua_tonumber(L, 6);
	float y = lua_tonumber(L, 7);
	float z = lua_tonumber(L, 8);
	float followTime = lua_tonumber(L, 9);
	cFollowCommand* follow = new cFollowCommand(maxSpeed, (cGameObject*)cGameObjectMediator::GetInstance()->vecGameObjects[id], (cGameObject*)cGameObjectMediator::GetInstance()->vecGameObjects[id2]
		, closeDistance, farDistance, glm::vec3(x, y, z), followTime);
	cGameObjectMediator::GetInstance()->AddToCommands(follow);
	return 1;
}

int cGameObjectMediator::curveSpeedSinglePointCommand(lua_State * L)
{
	int id = lua_tonumber(L, 1);
	float maxSpeed = lua_tonumber(L, 2);
	float xDest = lua_tonumber(L, 3);
	float yDest = lua_tonumber(L, 4);
	float zDest = lua_tonumber(L, 5);
	float xOff = lua_tonumber(L, 6);
	float yOff = lua_tonumber(L, 7);
	float zOff = lua_tonumber(L, 8);

	cFollowCurveCommand* followC = new cFollowCurveCommand((cGameObject*)cGameObjectMediator::GetInstance()->vecGameObjects[id], maxSpeed, glm::vec3(xDest, yDest, zDest), glm::vec3(xOff, yOff, zOff));
	cGameObjectMediator::GetInstance()->AddToCommands(followC);
	return 1;
}

int cGameObjectMediator::curveSpeedDoublePointCommand(lua_State * L)
{
	int id = lua_tonumber(L, 1);
	float maxSpeed = lua_tonumber(L, 2);
	float xDest = lua_tonumber(L, 3);
	float yDest = lua_tonumber(L, 4);
	float zDest = lua_tonumber(L, 5);
	float xOff = lua_tonumber(L, 6);
	float yOff = lua_tonumber(L, 7);
	float zOff = lua_tonumber(L, 8);
	float xOff2 = lua_tonumber(L, 9);
	float yOff2 = lua_tonumber(L, 10);
	float zOff2 = lua_tonumber(L, 11);
	cFollowCurveCommand* followC = new cFollowCurveCommand((cGameObject*)cGameObjectMediator::GetInstance()->vecGameObjects[id], maxSpeed,
		glm::vec3(xDest, yDest, zDest), glm::vec3(xOff, yOff, zOff), glm::vec3(xOff2, yOff2, zOff2));
	cGameObjectMediator::GetInstance()->AddToCommands(followC);
	return 1;
}

int cGameObjectMediator::curveSpeedEaseInEaseOutSinglePointCommand(lua_State * L)
{
	int id = lua_tonumber(L, 1);
	float maxSpeed = lua_tonumber(L, 2);
	float xDest = lua_tonumber(L, 3);
	float yDest = lua_tonumber(L, 4);
	float zDest = lua_tonumber(L, 5);
	float xOff = lua_tonumber(L, 6);
	float yOff = lua_tonumber(L, 7);
	float zOff = lua_tonumber(L, 8);
	int easeInPoints = lua_tonumber(L, 9);
	int easeOutPoints = lua_tonumber(L, 10);

	cFollowCurveCommand* followC = new cFollowCurveCommand((cGameObject*)cGameObjectMediator::GetInstance()->vecGameObjects[id], maxSpeed, glm::vec3(xDest, yDest, zDest), glm::vec3(xOff, yOff, zOff), easeInPoints,easeOutPoints);
	cGameObjectMediator::GetInstance()->AddToCommands(followC);
	return 1;
}

int cGameObjectMediator::curveSpeedEaseInEaseOutDoublePointCommand(lua_State * L)
{
	int id = lua_tonumber(L, 1);
	float maxSpeed = lua_tonumber(L, 2);
	float xDest = lua_tonumber(L, 3);
	float yDest = lua_tonumber(L, 4);
	float zDest = lua_tonumber(L, 5);
	float xOff = lua_tonumber(L, 6);
	float yOff = lua_tonumber(L, 7);
	float zOff = lua_tonumber(L, 8);
	float xOff2 = lua_tonumber(L, 9);
	float yOff2 = lua_tonumber(L, 10);
	float zOff2 = lua_tonumber(L, 11);
	int easeInPoints = lua_tonumber(L, 11);
	int easeOutPoints = lua_tonumber(L, 12);
	cFollowCurveCommand* followC = new cFollowCurveCommand((cGameObject*)cGameObjectMediator::GetInstance()->vecGameObjects[id], maxSpeed,
		glm::vec3(xDest, yDest, zDest), glm::vec3(xOff, yOff, zOff), glm::vec3(xOff2, yOff2, zOff2), easeInPoints, easeOutPoints);
	cGameObjectMediator::GetInstance()->AddToCommands(followC);
	return 1;
}

int cGameObjectMediator::curveSpeedEaseInEaseOutEasePercentageSinglePointCommand(lua_State * L)
{
	int id = lua_tonumber(L, 1);
	float maxSpeed = lua_tonumber(L, 2);
	float xDest = lua_tonumber(L, 3);
	float yDest = lua_tonumber(L, 4);
	float zDest = lua_tonumber(L, 5);
	float xOff = lua_tonumber(L, 6);
	float yOff = lua_tonumber(L, 7);
	float zOff = lua_tonumber(L, 8);
	int easeInPoints = lua_tonumber(L, 9);
	int easeOutPoints = lua_tonumber(L, 10);
	float easePercentage = lua_tonumber(L, 11);

	cFollowCurveCommand* followC = new cFollowCurveCommand((cGameObject*)cGameObjectMediator::GetInstance()->vecGameObjects[id], maxSpeed, glm::vec3(xDest, yDest, zDest), glm::vec3(xOff, yOff, zOff), easeInPoints, easeOutPoints, easePercentage);
	cGameObjectMediator::GetInstance()->AddToCommands(followC);
	return 1;
}

int cGameObjectMediator::curveSpeedEaseInEaseOutEasePercentageDoublePointCommand(lua_State * L)
{
	int id = lua_tonumber(L, 1);
	float maxSpeed = lua_tonumber(L, 2);
	float xDest = lua_tonumber(L, 3);
	float yDest = lua_tonumber(L, 4);
	float zDest = lua_tonumber(L, 5);
	float xOff = lua_tonumber(L, 6);
	float yOff = lua_tonumber(L, 7);
	float zOff = lua_tonumber(L, 8);
	float xOff2 = lua_tonumber(L, 9);
	float yOff2 = lua_tonumber(L, 10);
	float zOff2 = lua_tonumber(L, 11);
	int easeInPoints = lua_tonumber(L, 11);
	int easeOutPoints = lua_tonumber(L, 12);
	float easePercentage = lua_tonumber(L, 13);
	cFollowCurveCommand* followC = new cFollowCurveCommand((cGameObject*)cGameObjectMediator::GetInstance()->vecGameObjects[id], maxSpeed,
		glm::vec3(xDest, yDest, zDest), glm::vec3(xOff, yOff, zOff), glm::vec3(xOff2, yOff2, zOff2), easeInPoints, easeOutPoints, easePercentage);
	cGameObjectMediator::GetInstance()->AddToCommands(followC);
	return 1;
}

int cGameObjectMediator::curveSpeedEaseInEaseOutSinglePointCommandDelay(lua_State * L)
{
	int id = lua_tonumber(L, 1);
	float maxSpeed = lua_tonumber(L, 2);
	float xDest = lua_tonumber(L, 3);
	float yDest = lua_tonumber(L, 4);
	float zDest = lua_tonumber(L, 5);
	float xOff = lua_tonumber(L, 6);
	float yOff = lua_tonumber(L, 7);
	float zOff = lua_tonumber(L, 8);
	int easeInPoints = lua_tonumber(L, 9);
	int easeOutPoints = lua_tonumber(L, 10);
	float delay = lua_tonumber(L, 11);

	cFollowCurveCommand* followC = new cFollowCurveCommand(delay,(cGameObject*)cGameObjectMediator::GetInstance()->vecGameObjects[id], maxSpeed, glm::vec3(xDest, yDest, zDest), glm::vec3(xOff, yOff, zOff), easeInPoints, easeOutPoints);
	cGameObjectMediator::GetInstance()->AddToCommands(followC);
	return 1;
}

int cGameObjectMediator::curveSpeedEaseInEaseOutDoublePointCommandDelay(lua_State * L)
{
	int id = lua_tonumber(L, 1);
	float maxSpeed = lua_tonumber(L, 2);
	float xDest = lua_tonumber(L, 3);
	float yDest = lua_tonumber(L, 4);
	float zDest = lua_tonumber(L, 5);
	float xOff = lua_tonumber(L, 6);
	float yOff = lua_tonumber(L, 7);
	float zOff = lua_tonumber(L, 8);
	float xOff2 = lua_tonumber(L, 9);
	float yOff2 = lua_tonumber(L, 10);
	float zOff2 = lua_tonumber(L, 11);
	int easeInPoints = lua_tonumber(L, 11);
	int easeOutPoints = lua_tonumber(L, 12);
	float delay = lua_tonumber(L, 13);
	cFollowCurveCommand* followC = new cFollowCurveCommand(delay,(cGameObject*)cGameObjectMediator::GetInstance()->vecGameObjects[id], maxSpeed,
		glm::vec3(xDest, yDest, zDest), glm::vec3(xOff, yOff, zOff), glm::vec3(xOff2, yOff2, zOff2), easeInPoints, easeOutPoints);
	cGameObjectMediator::GetInstance()->AddToCommands(followC);
	return 1;
}

int cGameObjectMediator::curveSpeedEaseInEaseOutEasePercentageSinglePointCommandDelay(lua_State * L)
{
	int id = lua_tonumber(L, 1);
	float maxSpeed = lua_tonumber(L, 2);
	float xDest = lua_tonumber(L, 3);
	float yDest = lua_tonumber(L, 4);
	float zDest = lua_tonumber(L, 5);
	float xOff = lua_tonumber(L, 6);
	float yOff = lua_tonumber(L, 7);
	float zOff = lua_tonumber(L, 8);
	int easeInPoints = lua_tonumber(L, 9);
	int easeOutPoints = lua_tonumber(L, 10);
	float easePercentage = lua_tonumber(L, 11);
	float delay = lua_tonumber(L, 12);

	cFollowCurveCommand* followC = new cFollowCurveCommand(delay,(cGameObject*)cGameObjectMediator::GetInstance()->vecGameObjects[id], maxSpeed, glm::vec3(xDest, yDest, zDest), glm::vec3(xOff, yOff, zOff), easeInPoints, easeOutPoints, easePercentage);
	cGameObjectMediator::GetInstance()->AddToCommands(followC);
	return 1;
}

int cGameObjectMediator::curveSpeedEaseInEaseOutEasePercentageDoublePointCommandDelay(lua_State * L)
{
	int id = lua_tonumber(L, 1);
	float maxSpeed = lua_tonumber(L, 2);
	float xDest = lua_tonumber(L, 3);
	float yDest = lua_tonumber(L, 4);
	float zDest = lua_tonumber(L, 5);
	float xOff = lua_tonumber(L, 6);
	float yOff = lua_tonumber(L, 7);
	float zOff = lua_tonumber(L, 8);
	float xOff2 = lua_tonumber(L, 9);
	float yOff2 = lua_tonumber(L, 10);
	float zOff2 = lua_tonumber(L, 11);
	int easeInPoints = lua_tonumber(L, 11);
	int easeOutPoints = lua_tonumber(L, 12);
	float easePercentage = lua_tonumber(L, 13);
	cFollowCurveCommand* followC = new cFollowCurveCommand((cGameObject*)cGameObjectMediator::GetInstance()->vecGameObjects[id], maxSpeed,
		glm::vec3(xDest, yDest, zDest), glm::vec3(xOff, yOff, zOff), glm::vec3(xOff2, yOff2, zOff2), easeInPoints, easeOutPoints, easePercentage);
	cGameObjectMediator::GetInstance()->AddToCommands(followC);
	return 1;
}

int cGameObjectMediator::curveTimeSinglePointCommand(lua_State * L)
{
	int id = lua_tonumber(L, 1);
	float time = lua_tonumber(L, 2);
	float xDest = lua_tonumber(L, 3);
	float yDest = lua_tonumber(L, 4);
	float zDest = lua_tonumber(L, 5);
	float xOff = lua_tonumber(L, 6);
	float yOff = lua_tonumber(L, 7);
	float zOff = lua_tonumber(L, 8);

	cFollowCurveCommand* followC = new cFollowCurveCommand((cGameObject*)cGameObjectMediator::GetInstance()->vecGameObjects[id],
		glm::vec3(xDest, yDest, zDest), glm::vec3(xOff, yOff, zOff), time);
	cGameObjectMediator::GetInstance()->AddToCommands(followC);
	return 1;
}

int cGameObjectMediator::curveTimeDoublePointCommand(lua_State * L)
{
	int id = lua_tonumber(L, 1);
	float time = lua_tonumber(L, 2);
	float xDest = lua_tonumber(L, 3);
	float yDest = lua_tonumber(L, 4);
	float zDest = lua_tonumber(L, 5);
	float xOff = lua_tonumber(L, 6);
	float yOff = lua_tonumber(L, 7);
	float zOff = lua_tonumber(L, 8);
	float xOff2 = lua_tonumber(L, 9);
	float yOff2 = lua_tonumber(L, 10);
	float zOff2 = lua_tonumber(L, 11);

	cFollowCurveCommand* followC = new cFollowCurveCommand((cGameObject*)cGameObjectMediator::GetInstance()->vecGameObjects[id],
		glm::vec3(xDest, yDest, zDest), glm::vec3(xOff, yOff, zOff), glm::vec3(xOff2, yOff2, zOff2), time);
	cGameObjectMediator::GetInstance()->AddToCommands(followC);
	return 1;
}

int cGameObjectMediator::moveSpeedCommand(lua_State * L)
{
	/*cMoveToCommand(cGameObject * theObject, glm::vec3 destination,
		float maxSpeed);
	cMoveToCommand(cGameObject * theObject, float inTime, glm::vec3 destination);*/
	int id = lua_tonumber(L, 1);
	float maxSpeed = lua_tonumber(L, 2);
	float x = lua_tonumber(L, 3);
	float y = lua_tonumber(L, 4);
	float z = lua_tonumber(L, 5);

	cMoveToCommand* moveTo = new cMoveToCommand((cGameObject*)cGameObjectMediator::GetInstance()->vecGameObjects[id], glm::vec3(x, y, z), maxSpeed);
	cGameObjectMediator::GetInstance()->AddToCommands(moveTo);
	return 1;

}

int cGameObjectMediator::moveSpeedEaseInEaseOutCommand(lua_State * L)
{
	int id = lua_tonumber(L, 1);
	float maxSpeed = lua_tonumber(L, 2);
	float x = lua_tonumber(L, 3);
	float y = lua_tonumber(L, 4);
	float z = lua_tonumber(L, 5);
	int easeInPoints = lua_tonumber(L, 6);
	int easeOutPoints = lua_tonumber(L, 7);

	cMoveToCommand* moveTo = new cMoveToCommand((cGameObject*)cGameObjectMediator::GetInstance()->vecGameObjects[id], glm::vec3(x, y, z), maxSpeed, easeInPoints,easeOutPoints);
	cGameObjectMediator::GetInstance()->AddToCommands(moveTo);
	return 1;
}

int cGameObjectMediator::moveSpeedEaseInEaseOutEasePercentageCommand(lua_State * L)
{
	int id = lua_tonumber(L, 1);
	float maxSpeed = lua_tonumber(L, 2);
	float x = lua_tonumber(L, 3);
	float y = lua_tonumber(L, 4);
	float z = lua_tonumber(L, 5);
	int easeInPoints = lua_tonumber(L, 6);
	int easeOutPoints = lua_tonumber(L, 7);
	float easePercentage = lua_tonumber(L, 8);

	cMoveToCommand* moveTo = new cMoveToCommand((cGameObject*)cGameObjectMediator::GetInstance()->vecGameObjects[id], glm::vec3(x, y, z), maxSpeed, easeInPoints, easeOutPoints, easePercentage);
	cGameObjectMediator::GetInstance()->AddToCommands(moveTo);
	return 1;
}

int cGameObjectMediator::moveSpeedEaseInEaseOutCommandDelay(lua_State * L)
{
	int id = lua_tonumber(L, 1);
	float maxSpeed = lua_tonumber(L, 2);
	float x = lua_tonumber(L, 3);
	float y = lua_tonumber(L, 4);
	float z = lua_tonumber(L, 5);
	int easeInPoints = lua_tonumber(L, 6);
	int easeOutPoints = lua_tonumber(L, 7);
	float delay = lua_tonumber(L, 8);

	cMoveToCommand* moveTo = new cMoveToCommand(delay,(cGameObject*)cGameObjectMediator::GetInstance()->vecGameObjects[id], glm::vec3(x, y, z), maxSpeed, easeInPoints, easeOutPoints);
	cGameObjectMediator::GetInstance()->AddToCommands(moveTo);
	return 1;
}

int cGameObjectMediator::moveSpeedEaseInEaseOutEasePercentageCommandDelay(lua_State * L)
{
	int id = lua_tonumber(L, 1);
	float maxSpeed = lua_tonumber(L, 2);
	float x = lua_tonumber(L, 3);
	float y = lua_tonumber(L, 4);
	float z = lua_tonumber(L, 5);
	int easeInPoints = lua_tonumber(L, 6);
	int easeOutPoints = lua_tonumber(L, 7);
	float easePercentage = lua_tonumber(L, 8);
	float delay = lua_tonumber(L, 9);

	cMoveToCommand* moveTo = new cMoveToCommand(delay,(cGameObject*)cGameObjectMediator::GetInstance()->vecGameObjects[id], glm::vec3(x, y, z), maxSpeed, easeInPoints, easeOutPoints, easePercentage);
	cGameObjectMediator::GetInstance()->AddToCommands(moveTo);
	return 1;
}

int cGameObjectMediator::moveTimeCommand(lua_State * L)
{
	int id = lua_tonumber(L, 1);
	float inTime = lua_tonumber(L, 2);
	float x = lua_tonumber(L, 3);
	float y = lua_tonumber(L, 4);
	float z = lua_tonumber(L, 5);

	
	int size = cGameObjectMediator::GetInstance()->tempVecCommands.size();

	cMoveToCommand* moveTo = new cMoveToCommand((cGameObject*)cGameObjectMediator::GetInstance()->vecGameObjects[id], inTime, glm::vec3(x, y, z));
	cGameObjectMediator::GetInstance()->AddToCommands(moveTo);
	return 1;
}

int cGameObjectMediator::moveInstantCommand(lua_State * L)
{

	int id = lua_tonumber(L, 1);
	float x = lua_tonumber(L, 2);
	float y = lua_tonumber(L, 3);
	float z = lua_tonumber(L, 4);


	int size = cGameObjectMediator::GetInstance()->tempVecCommands.size();

	cMoveToCommand* moveTo = new cMoveToCommand((cGameObject*)cGameObjectMediator::GetInstance()->vecGameObjects[id], glm::vec3(x, y, z));
	cGameObjectMediator::GetInstance()->AddToCommands(moveTo);
	return 1;
}

int cGameObjectMediator::moveInstantCommandDelay(lua_State * L)
{

	int id = lua_tonumber(L, 1);
	float x = lua_tonumber(L, 2);
	float y = lua_tonumber(L, 3);
	float z = lua_tonumber(L, 4);
	float delay = lua_tonumber(L, 5);


	int size = cGameObjectMediator::GetInstance()->tempVecCommands.size();

	cMoveToCommand* moveTo = new cMoveToCommand(delay,(cGameObject*)cGameObjectMediator::GetInstance()->vecGameObjects[id], glm::vec3(x, y, z));
	cGameObjectMediator::GetInstance()->AddToCommands(moveTo);
	return 1;
}


int cGameObjectMediator::orientInstantCommand(lua_State * L)
{

	int id = lua_tonumber(L, 1);
	float x = lua_tonumber(L, 2);
	float y = lua_tonumber(L, 3);
	float z = lua_tonumber(L, 4);


	int size = cGameObjectMediator::GetInstance()->tempVecCommands.size();

	cOrientToCommand* orientTo = new cOrientToCommand((cGameObject*)cGameObjectMediator::GetInstance()->vecGameObjects[id], glm::vec3(x, y, z));
	cGameObjectMediator::GetInstance()->AddToCommands(orientTo);
	return 1;
}

int cGameObjectMediator::orientInstantCommandDelay(lua_State * L)
{

	int id = lua_tonumber(L, 1);
	float x = lua_tonumber(L, 2);
	float y = lua_tonumber(L, 3);
	float z = lua_tonumber(L, 4);
	float delay = lua_tonumber(L, 5);


	int size = cGameObjectMediator::GetInstance()->tempVecCommands.size();

	cOrientToCommand* orientTo = new cOrientToCommand(delay, (cGameObject*)cGameObjectMediator::GetInstance()->vecGameObjects[id], glm::vec3(x, y, z));
	cGameObjectMediator::GetInstance()->AddToCommands(orientTo);
	return 1;
}

int cGameObjectMediator::orientSpeedCommand(lua_State * L)
{
	int id = lua_tonumber(L, 1);
	float maxSpeed = lua_tonumber(L, 2);
	float x = lua_tonumber(L, 3);
	float y = lua_tonumber(L, 4);
	float z = lua_tonumber(L, 5);

	cOrientToCommand* orientTo = new cOrientToCommand((cGameObject*)cGameObjectMediator::GetInstance()->vecGameObjects[id], glm::vec3(x, y, z), maxSpeed);
	cGameObjectMediator::GetInstance()->AddToCommands(orientTo);
	return 1;
}

int cGameObjectMediator::orientSpeedEaseInEaseOutCommand(lua_State * L)
{
	int id = lua_tonumber(L, 1);
	float maxSpeed = lua_tonumber(L, 2);
	float x = lua_tonumber(L, 3);
	float y = lua_tonumber(L, 4);
	float z = lua_tonumber(L, 5);
	int easeInPoints = lua_tonumber(L, 6);
	int easeOutPoints = lua_tonumber(L, 7);

	cOrientToCommand* orientTo = new cOrientToCommand((cGameObject*)cGameObjectMediator::GetInstance()->vecGameObjects[id], glm::vec3(x, y, z), maxSpeed, easeInPoints,easeOutPoints);
	cGameObjectMediator::GetInstance()->AddToCommands(orientTo);
	return 1;
}


int cGameObjectMediator::orientSpeedEaseInEaseOutCommandDelay(lua_State * L)
{
	int id = lua_tonumber(L, 1);
	float maxSpeed = lua_tonumber(L, 2);
	float x = lua_tonumber(L, 3);
	float y = lua_tonumber(L, 4);
	float z = lua_tonumber(L, 5);
	int easeInPoints = lua_tonumber(L, 6);
	int easeOutPoints = lua_tonumber(L, 7);
	float delay = lua_tonumber(L, 8);

	cOrientToCommand* orientTo = new cOrientToCommand(delay,(cGameObject*)cGameObjectMediator::GetInstance()->vecGameObjects[id], glm::vec3(x, y, z), maxSpeed, easeInPoints, easeOutPoints);
	cGameObjectMediator::GetInstance()->AddToCommands(orientTo);
	return 1;
}



int cGameObjectMediator::orientTimeCommand(lua_State * L)
{
	int id = lua_tonumber(L, 1);
	float inTime = lua_tonumber(L, 2);
	float x = lua_tonumber(L, 3);
	float y = lua_tonumber(L, 4);
	float z = lua_tonumber(L, 5);

	cOrientToCommand* orientTo = new cOrientToCommand((cGameObject*)cGameObjectMediator::GetInstance()->vecGameObjects[id], inTime, glm::vec3(x, y, z));
	cGameObjectMediator::GetInstance()->AddToCommands(orientTo);
	return 1;
}

int cGameObjectMediator::cameraTargetCommand(lua_State * L)
{
	int id = lua_tonumber(L, 1);


	cCameraTargetCommand* followTarget = new cCameraTargetCommand((cGameObject*)cGameObjectMediator::GetInstance()->vecGameObjects[cGameObjectMediator::GetInstance()->vecGameObjects.size() - 1],
		(cGameObject*)cGameObjectMediator::GetInstance()->vecGameObjects[id]);

	cGameObjectMediator::GetInstance()->AddToCommands(followTarget);
	return 1;
}

int cGameObjectMediator::followDistanceCommandDelay(lua_State * L)
{
	int id = lua_tonumber(L, 1);
	int id2 = lua_tonumber(L, 2);
	float distance = lua_tonumber(L, 3);
	float x = lua_tonumber(L, 4);
	float y = lua_tonumber(L, 5);
	float z = lua_tonumber(L, 6);
	float followTime = lua_tonumber(L, 7);
	float delay = lua_tonumber(L, 8);
	cFollowCommand* follow = new cFollowCommand(delay,(cGameObject*)cGameObjectMediator::GetInstance()->vecGameObjects[id], distance, (cGameObject*)cGameObjectMediator::GetInstance()->vecGameObjects[id2]
		, glm::vec3(x, y, z), followTime);
	cGameObjectMediator::GetInstance()->AddToCommands(follow);

	return 1;
}

int cGameObjectMediator::followSpeedCommandDelay(lua_State * L)
{
	int id = lua_tonumber(L, 1);
	int id2 = lua_tonumber(L, 2);
	float maxSpeed = lua_tonumber(L, 3);
	float closeDistance = lua_tonumber(L, 4);
	float farDistance = lua_tonumber(L, 5);
	float x = lua_tonumber(L, 6);
	float y = lua_tonumber(L, 7);
	float z = lua_tonumber(L, 8);
	float followTime = lua_tonumber(L, 9);
	float delay = lua_tonumber(L, 10);
	cFollowCommand* follow = new cFollowCommand(delay, maxSpeed, (cGameObject*)cGameObjectMediator::GetInstance()->vecGameObjects[id], (cGameObject*)cGameObjectMediator::GetInstance()->vecGameObjects[id2]
		, closeDistance, farDistance, glm::vec3(x, y, z), followTime);
	cGameObjectMediator::GetInstance()->AddToCommands(follow);
	return 1;
}

int cGameObjectMediator::curveSpeedSinglePointCommandDelay(lua_State * L)
{

	int id = lua_tonumber(L, 1);
	float maxSpeed = lua_tonumber(L, 2);
	float xDest = lua_tonumber(L, 3);
	float yDest = lua_tonumber(L, 4);
	float zDest = lua_tonumber(L, 5);
	float xOff = lua_tonumber(L, 6);
	float yOff = lua_tonumber(L, 7);
	float zOff = lua_tonumber(L, 8);
	float delay = lua_tonumber(L, 9);

	cFollowCurveCommand* followC = new cFollowCurveCommand(delay,(cGameObject*)cGameObjectMediator::GetInstance()->vecGameObjects[id], maxSpeed, glm::vec3(xDest, yDest, zDest), glm::vec3(xOff, yOff, zOff));
	cGameObjectMediator::GetInstance()->AddToCommands(followC);
	return 1;
}

int cGameObjectMediator::curveSpeedDoublePointCommandDelay(lua_State * L)
{
	int id = lua_tonumber(L, 1);
	float maxSpeed = lua_tonumber(L, 2);
	float xDest = lua_tonumber(L, 3);
	float yDest = lua_tonumber(L, 4);
	float zDest = lua_tonumber(L, 5);
	float xOff = lua_tonumber(L, 6);
	float yOff = lua_tonumber(L, 7);
	float zOff = lua_tonumber(L, 8);
	float xOff2 = lua_tonumber(L, 9);
	float yOff2 = lua_tonumber(L, 10);
	float zOff2 = lua_tonumber(L, 11);
	float delay = lua_tonumber(L, 12);

	cFollowCurveCommand* followC = new cFollowCurveCommand(delay,(cGameObject*)cGameObjectMediator::GetInstance()->vecGameObjects[id], maxSpeed,
		glm::vec3(xDest, yDest, zDest), glm::vec3(xOff, yOff, zOff), glm::vec3(xOff2, yOff2, zOff2));
	cGameObjectMediator::GetInstance()->AddToCommands(followC);
	return 1;
}

int cGameObjectMediator::curveTimeSinglePointCommandDelay(lua_State * L)
{
	int id = lua_tonumber(L, 1);
	float time = lua_tonumber(L, 2);
	float xDest = lua_tonumber(L, 3);
	float yDest = lua_tonumber(L, 4);
	float zDest = lua_tonumber(L, 5);
	float xOff = lua_tonumber(L, 6);
	float yOff = lua_tonumber(L, 7);
	float zOff = lua_tonumber(L, 8);
	float delay = lua_tonumber(L, 9);

	cFollowCurveCommand* followC = new cFollowCurveCommand(delay,(cGameObject*)cGameObjectMediator::GetInstance()->vecGameObjects[id],
		glm::vec3(xDest, yDest, zDest), glm::vec3(xOff, yOff, zOff), time);
	cGameObjectMediator::GetInstance()->AddToCommands(followC);
	return 1;
}

int cGameObjectMediator::curveTimeDoublePointCommandDelay(lua_State * L)
{
	int id = lua_tonumber(L, 1);
	float time = lua_tonumber(L, 2);
	float xDest = lua_tonumber(L, 3);
	float yDest = lua_tonumber(L, 4);
	float zDest = lua_tonumber(L, 5);
	float xOff = lua_tonumber(L, 6);
	float yOff = lua_tonumber(L, 7);
	float zOff = lua_tonumber(L, 8);
	float xOff2 = lua_tonumber(L, 9);
	float yOff2 = lua_tonumber(L, 10);
	float zOff2 = lua_tonumber(L, 11);
	float delay = lua_tonumber(L, 12);

	cFollowCurveCommand* followC = new cFollowCurveCommand(delay,(cGameObject*)cGameObjectMediator::GetInstance()->vecGameObjects[id],
		glm::vec3(xDest, yDest, zDest), glm::vec3(xOff, yOff, zOff), glm::vec3(xOff2, yOff2, zOff2), time);
	cGameObjectMediator::GetInstance()->AddToCommands(followC);
	return 1;
}

int cGameObjectMediator::moveSpeedCommandDelay(lua_State * L)
{
	int id = lua_tonumber(L, 1);
	float maxSpeed = lua_tonumber(L, 2);
	float x = lua_tonumber(L, 3);
	float y = lua_tonumber(L, 4);
	float z = lua_tonumber(L, 5);
	float delay = lua_tonumber(L, 6);

	cMoveToCommand* moveTo = new cMoveToCommand(delay,(cGameObject*)cGameObjectMediator::GetInstance()->vecGameObjects[id], glm::vec3(x, y, z), maxSpeed);
	cGameObjectMediator::GetInstance()->AddToCommands(moveTo);
	return 1;
}

int cGameObjectMediator::moveTimeCommandDelay(lua_State * L)
{
	int id = lua_tonumber(L, 1);
	float inTime = lua_tonumber(L, 2);
	float x = lua_tonumber(L, 3);
	float y = lua_tonumber(L, 4);
	float z = lua_tonumber(L, 5);
	float delay = lua_tonumber(L, 6);

	int size = cGameObjectMediator::GetInstance()->tempVecCommands.size();

	cMoveToCommand* moveTo = new cMoveToCommand(delay,(cGameObject*)cGameObjectMediator::GetInstance()->vecGameObjects[id], inTime, glm::vec3(x, y, z));
	cGameObjectMediator::GetInstance()->AddToCommands(moveTo);
	return 1;
}

int cGameObjectMediator::orientSpeedCommandDelay(lua_State * L)
{
	int id = lua_tonumber(L, 1);
	float maxSpeed = lua_tonumber(L, 2);
	float x = lua_tonumber(L, 3);
	float y = lua_tonumber(L, 4);
	float z = lua_tonumber(L, 5);
	float delay = lua_tonumber(L, 6);

	cOrientToCommand* orientTo = new cOrientToCommand(delay,(cGameObject*)cGameObjectMediator::GetInstance()->vecGameObjects[id], glm::vec3(x, y, z), maxSpeed);
	cGameObjectMediator::GetInstance()->AddToCommands(orientTo);
	return 1;
}

int cGameObjectMediator::orientTimeCommandDelay(lua_State * L)
{
	int id = lua_tonumber(L, 1);
	float inTime = lua_tonumber(L, 2);
	float x = lua_tonumber(L, 3);
	float y = lua_tonumber(L, 4);
	float z = lua_tonumber(L, 5);
	float delay = lua_tonumber(L, 6);

	cOrientToCommand* orientTo = new cOrientToCommand(delay,(cGameObject*)cGameObjectMediator::GetInstance()->vecGameObjects[id], inTime, glm::vec3(x, y, z));
	cGameObjectMediator::GetInstance()->AddToCommands(orientTo);
	return 1;
}

int cGameObjectMediator::cameraTargetCommandDelay(lua_State * L)
{
	int id = lua_tonumber(L, 1);
	float delay = lua_tonumber(L, 2);

	cCameraTargetCommand* followTarget = new cCameraTargetCommand(delay,(cGameObject*)cGameObjectMediator::GetInstance()->vecGameObjects[cGameObjectMediator::GetInstance()->vecGameObjects.size() - 1],
		(cGameObject*)cGameObjectMediator::GetInstance()->vecGameObjects[id]);

	cGameObjectMediator::GetInstance()->AddToCommands(followTarget);
	return 1;
}

int cGameObjectMediator::sphereTrigger(lua_State * L)
{
	float centerX = lua_tonumber(L, 1);
	float centerY = lua_tonumber(L, 2);
	float centerZ = lua_tonumber(L, 3);
	float radius = lua_tonumber(L, 4);
	//TODO, see if using a blank command group is ok
	cCommandGroup tempGroup;
	//cSphereTrigger::cSphereTrigger(cCommandGroup commandGroup, glm::vec3 centerPos, float radius)
	cSphereTrigger* sphereTrigger = new cSphereTrigger(tempGroup, glm::vec3(centerX, centerY, centerZ), radius);
	cGameObjectMediator::GetInstance()->vecLocationTriggers.push_back(sphereTrigger);
	cGameObjectMediator::GetInstance()->addingIntoLocationTrigger = true;
	return 1;
}

int cGameObjectMediator::boxTrigger(lua_State * L)
{
	float centerX = lua_tonumber(L, 1);
	float centerY = lua_tonumber(L, 2);
	float centerZ = lua_tonumber(L, 3);
	float halfSize = lua_tonumber(L, 4);
	cCommandGroup tempGroup;
	cBoxTrigger* boxTrigger = new cBoxTrigger(tempGroup, glm::vec3(centerX, centerY, centerZ), halfSize);
	cGameObjectMediator::GetInstance()->vecLocationTriggers.push_back(boxTrigger);
	cGameObjectMediator::GetInstance()->addingIntoLocationTrigger = true;
	return 1;
}

void cGameObjectMediator::ActivateLua()
{
	int result = luaL_dofile(state, "double.lua");

	lua_register(state, "sphereTrigger", sphereTrigger);
	lua_register(state, "endGroup", endGroup);
	lua_register(state, "doubleIt", doubleIt);
	lua_register(state, "endGroup", endGroup);
	lua_register(state, "startGroup", startGroup);
	lua_register(state, "moveSpeedCommand", moveSpeedCommand);

	lua_register(state, "moveSpeedEaseInEaseOutCommand", moveSpeedEaseInEaseOutCommand);
	lua_register(state, "moveSpeedEaseInEaseOutCommandDelay", moveSpeedEaseInEaseOutCommandDelay);
	lua_register(state, "moveSpeedEaseInEaseOutEasePercentageCommand", moveSpeedEaseInEaseOutEasePercentageCommand);
	lua_register(state, "moveSpeedEaseInEaseOutEasePercentageCommandDelay", moveSpeedEaseInEaseOutEasePercentageCommandDelay);

	lua_register(state, "followSpeedCommand", followSpeedCommand);
	lua_register(state, "followDistanceCommand", followDistanceCommand);
	lua_register(state, "curveSpeedSinglePointCommand", curveSpeedSinglePointCommand);
	lua_register(state, "curveSpeedDoublePointCommand", curveSpeedDoublePointCommand);
	lua_register(state, "curveTimeSinglePointCommand", curveTimeSinglePointCommand);
	lua_register(state, "curveTimeDoublePointCommand", curveTimeDoublePointCommand);
	lua_register(state, "moveTimeCommand", moveTimeCommand);
	lua_register(state, "orientSpeedCommand", orientSpeedCommand);

	lua_register(state, "orientSpeedEaseInEaseOutCommand", orientSpeedEaseInEaseOutCommand);
	lua_register(state, "orientSpeedEaseInEaseOutCommandDelay", orientSpeedEaseInEaseOutCommandDelay);

	lua_register(state, "orientTimeCommand", orientTimeCommand);
	lua_register(state, "cameraTargetCommand", cameraTargetCommand);
	lua_register(state, "curveSpeedEaseInEaseOutSinglePointCommand", curveSpeedEaseInEaseOutSinglePointCommand);
	lua_register(state, "curveSpeedEaseInEaseOutDoublePointCommand", curveSpeedEaseInEaseOutDoublePointCommand);
	lua_register(state, "curveSpeedEaseInEaseOutEasePercentageSinglePointCommand", curveSpeedEaseInEaseOutEasePercentageSinglePointCommand);
	lua_register(state, "curveSpeedEaseInEaseOutEasePercentageDoublePointCommand", curveSpeedEaseInEaseOutEasePercentageDoublePointCommand);
	lua_register(state, "curveSpeedEaseInEaseOutSinglePointCommandDelay", curveSpeedEaseInEaseOutSinglePointCommandDelay);
	lua_register(state, "curveSpeedEaseInEaseOutDoublePointCommandDelay", curveSpeedEaseInEaseOutDoublePointCommandDelay);
	lua_register(state, "curveSpeedEaseInEaseOutEasePercentageSinglePointCommandDelay", curveSpeedEaseInEaseOutEasePercentageSinglePointCommandDelay);
	lua_register(state, "curveSpeedEaseInEaseOutEasePercentageDoublePointCommandDelay", curveSpeedEaseInEaseOutEasePercentageDoublePointCommandDelay);

	lua_register(state, "moveSpeedCommandDelay", moveSpeedCommandDelay);
	lua_register(state, "followSpeedCommandDelay", followSpeedCommandDelay);
	lua_register(state, "followDistanceCommandDelay", followDistanceCommandDelay);
	lua_register(state, "curveSpeedSinglePointCommandDelay", curveSpeedSinglePointCommandDelay);
	lua_register(state, "curveSpeedDoublePointCommandDelay", curveSpeedDoublePointCommandDelay);
	lua_register(state, "curveTimeSinglePointCommandDelay", curveTimeSinglePointCommandDelay);
	lua_register(state, "curveTimeDoublePointCommandDelay", curveTimeDoublePointCommandDelay);
	lua_register(state, "moveTimeCommandDelay", moveTimeCommandDelay);
	lua_register(state, "orientSpeedCommandDelay", orientSpeedCommandDelay);
	lua_register(state, "orientTimeCommandDelay", orientTimeCommandDelay);
	lua_register(state, "cameraTargetCommandDelay", cameraTargetCommandDelay);

	lua_register(state, "moveInstantCommandDelay", moveInstantCommandDelay);
	lua_register(state, "moveInstantCommand", moveInstantCommand);
	lua_register(state, "orientInstantCommandDelay", orientInstantCommandDelay);
	lua_register(state, "orientInstantCommand", orientInstantCommand);
	
	lua_getglobal(state, "runLua");

	lua_pcall(state, 0, 0, 0); //Call the lua method
}

//move the main sphere
void cGameObjectMediator::MoveMainSphere(glm::vec3 force)
{
	((cSphere*)vecGameObjects[3])->velocity += force;
}

//update all the game objects
void cGameObjectMediator::UpdateGameObjects(float deltaTime)
{
	//update the commands as well
	int totalCommands = vecCommands.size();

	if (totalCommands >= 1)
	{
		vecCommands[0]->Update(deltaTime);
	}

	/*for (int index = 0; index < totalCommands; ++index)
	{
		vecCommands[index]->Update(deltaTime);
	}*/


	for (int i = 0; i < vecCommands.size(); ++i)
	{
		if (vecCommands[i]->complete)
		{
			vecCommands.erase(vecCommands.begin() + i);
			i--;

		}
	}

	//int totalGameObjects = vecGameObjects.size();
	//for (int index = 0; index != totalGameObjects; index++)
	//{
	//	iGameObject* pCurGO = vecGameObjects[index];

	//	pCurGO->Update(&vecGameObjects, deltaTime);

	//}//for ( int index...
}

//find all colliding triangles for a specific sphere
bool cGameObjectMediator::GetCollidingTriangles(iGameObject* piSphere, iGameObject* piTriangleObject, std::vector<cPhysTriangle>& closestTriangles)
{
	cGameObject* pTriangleObject = (cGameObject*)piTriangleObject;
	cSphere* pSphere = (cSphere*)piSphere;

	sVAOInfo VAODrawInfo;
	::g_pVAOManager->lookupVAOFromName(pTriangleObject->modelData.meshName, VAODrawInfo);
	int numberOfTriangles = VAODrawInfo.vecPhysTris.size();

	int activeIndex = -1;
	for (int triIndex = 0; triIndex != numberOfTriangles; triIndex++)
	{
		cPhysTriangle curTriangle = VAODrawInfo.vecPhysTris[triIndex];
		//////////////////////////////////
		//Below are the steps needed if the game object can be moved, not just a static model
		//at position 0
		if (pTriangleObject->editable) {
			glm::vec4 tempVec1(curTriangle.vertex[0].x, curTriangle.vertex[0].y, curTriangle.vertex[0].z, 1);
			tempVec1 = pTriangleObject->modelData.worldMatrix * tempVec1;

			glm::vec4 tempVec2(curTriangle.vertex[1].x, curTriangle.vertex[1].y, curTriangle.vertex[1].z, 1);
			tempVec2 = pTriangleObject->modelData.worldMatrix * tempVec2;

			glm::vec4 tempVec3(curTriangle.vertex[2].x, curTriangle.vertex[2].y, curTriangle.vertex[2].z, 1);
			tempVec3 = pTriangleObject->modelData.worldMatrix * tempVec3;

			curTriangle.vertex[0] = glm::vec3(tempVec1.x, tempVec1.y, tempVec1.z);
			curTriangle.vertex[1] = glm::vec3(tempVec2.x, tempVec2.y, tempVec2.z);
			curTriangle.vertex[2] = glm::vec3(tempVec3.x, tempVec3.y, tempVec3.z);
		}
		////////////////////////////////////////////////////////////////////////

		//get the closest point on a triangle
		glm::vec3 theClosestPoint = curTriangle.ClosestPtPointTriangle(pSphere->GetModelData()->position);
		//get the distance to that point
		float thisDistance = glm::distance(pSphere->GetModelData()->position, theClosestPoint);
		//if the distance is less than the sphere radius, then a collision happened
		if (thisDistance < pSphere->radius)
		{
			closestTriangles.push_back(curTriangle);
		}

	}

	if (closestTriangles.size() > 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

//compute all the interactions for a sphere
void cGameObjectMediator::ComputeInteractions(cSphere * sphere)
{
	//reset some attributes
	sphere->collidedThisFrame = false;
	sphere->hittingGround = false;

	//create a new collision
	cCollision collision;
	collision.mainObject = sphere;

	//rotate the sphere
	sphere->modelData.orientation2.x -= 0.03;


	int size = vecGameObjects.size();

	//go through all the other game objects
	for (int indexEO = 0; indexEO != size; indexEO++)
	{
		// Don't test for myself
		if (sphere == vecGameObjects[indexEO])
			continue;

		iGameObject* pOtherObject = vecGameObjects[indexEO];

		//if the other object is sphere
		if ((pOtherObject->GetType() == "sphere" || pOtherObject->GetType() == "grower") && !sphere->hittingSphere) {
			//if the spheres are penetrating 
			if (PenetrationTestSphereSphere(sphere, pOtherObject))
			{
				//push the other object into the collision object
				collision.otherGameObjects.push_back(pOtherObject);
				sphere->collidedThisFrame = true;
				cSphere* pSphereOther = (cSphere*)pOtherObject;

				pSphereOther->TakeDamage();
				if (pSphereOther->GetType() == "grower")
				{
					sphere->ChangeScale(sphere->modelData.scale * 1.7);
					sphere->ChangeRadius(sphere->radius * 1.7);
					sphere->Respawn();
				}
			}
		}
		else if (pOtherObject->GetType() == "cube" || pOtherObject->GetType() == "plane") {

			float distance = 99999; //value way too big for our simulation

			//will have to find if multiple triangles were in range
			std::vector<cPhysTriangle> closestTriangles;
			if (GetCollidingTriangles(sphere, pOtherObject, closestTriangles))
			{
				int trianglesSize = closestTriangles.size();
				//if there was a multiple triangle collision say so
				if (trianglesSize > 1)
				{
					std::cout << "Multi-Triangle Collision" << std::endl;
				}
				//push the triangle into the collision
				for (int i = 0; i < trianglesSize; i++)
				{
					std::pair<cPhysTriangle, iGameObject*> thePair;
					thePair = std::make_pair(closestTriangles[i], pOtherObject);
					collision.triangles.push_back(thePair);
				}
				sphere->hittingGround = true;

			}
		}

	}


	sphere->hittingSphere = sphere->collidedThisFrame;

	::g_pCollisionManager->AddCollision(collision);

}

void cGameObjectMediator::MoveObjectTo(int ID, glm::vec3 target, float time)
{
	((cGameObject*)vecGameObjects[ID])->MoveTo(target, time);
}

void cGameObjectMediator::RotateObjectTo(int ID, glm::vec3 rotationMatrix, float time)
{
	((cGameObject*)vecGameObjects[ID])->RotateTo(rotationMatrix, time);
}

void cGameObjectMediator::FollowObjectTo(int ID, int ID2, float distance, float followT)
{
	((cGameObject*)vecGameObjects[ID])->Follow(((cGameObject*)vecGameObjects[ID2]), distance, followT);
}

glm::mat4 cGameObjectMediator::getViewMatrix()
{
	if (cameraId == -1)
	{
		//find the first id of a camera object
		int size = vecGameObjects.size();
		for (int i = 0; i < size; ++i)
		{
			if (vecGameObjects[i]->GetType() == "camera")
			{
				cameraId = i;
				break;
			}
		}
	}

	if (cameraId != -1)
	{
		cCamera* camera = (cCamera*)vecGameObjects[cameraId];
		return camera->getViewMatrix();
	}
	return glm::mat4(1.0f);

}