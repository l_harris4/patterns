#ifndef CFOLLOWCOMMAND_HG
#define CFOLLOWCOMMAND_HG
#include "cCommand.h"

enum FollowType
{
	DISTANCE = 1,
	SPEED = 2
};

class cFollowCommand : public cCommand
{
public:
	cFollowCommand();
	cFollowCommand(cGameObject * theObject, float distance, cGameObject * followObject, glm::vec3 relativeToTarget, float followTime);
	cFollowCommand(float speed, cGameObject * theObject, cGameObject * followObject, float closeDistance, float farDistance, glm::vec3 relativeToTarget, float followTime);

	cFollowCommand(float delay, cGameObject * theObject, float distance, cGameObject * followObject, glm::vec3 relativeToTarget, float followTime);
	cFollowCommand(float delay, float speed, cGameObject * theObject, cGameObject * followObject, float closeDistance, float farDistance, glm::vec3 relativeToTarget, float followTime);
	~cFollowCommand();
	virtual void Update();
	virtual void Update(float deltaTime);
	float awayDistance;
	float farDistance;
	float followSpeed;
	float followTime;
	bool timed = true;
	glm::vec3 relativeToTarget;
	FollowType type;
	cGameObject* followObject;
	virtual cCommand* genCopy();
	virtual void setAffectedObject(cGameObject * gameObject);


};

#endif // !CFOLLOWCOMMAND_HG

