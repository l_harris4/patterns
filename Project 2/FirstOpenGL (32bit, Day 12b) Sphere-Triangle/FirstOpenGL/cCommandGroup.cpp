#include "cCommandGroup.h"
#include "cGameObject.h"
//#include "cOrientToCommand.h"

cCommandGroup::cCommandGroup()
{
}

cCommandGroup::cCommandGroup(float delay)
{
	this->startDelay = delay;
	this->delayed = true;
}

cCommandGroup::~cCommandGroup()
{
	int size = commands.size();
	for (int i = 0; i < size; i++)
	{
		delete commands[i];
	}
}

void cCommandGroup::Update()
{
}

void cCommandGroup::Update(float deltaTime)
{
	if (Ready(deltaTime))
	{
		int size = commands.size();
		bool allComplete = true;
		for (int i = 0; i < size; i++)
		{
			commands[i]->Update(deltaTime);

			if (!commands[i]->complete)
				allComplete = false;

			//if the command we are working on is not complete and this a serial command group,
			//break so the next command isn't updated
			if (!commands[i]->complete && serial)
				break;

		}
		complete = allComplete;
	}
}

cCommand * cCommandGroup::genCopy()
{
	cCommandGroup* newCommand = new cCommandGroup();
	int size = commands.size();
	newCommand->startDelay = this->startDelay;
	for (int i = 0; i < size; ++i)
	{
		newCommand->commands.push_back(commands[i]->genCopy());
	}
	return newCommand;
}

void cCommandGroup::setAffectedObject(cGameObject * gameObject)
{
	int size = commands.size();
	this->objectBeingAffected = gameObject;
	for (int i = 0; i < size; ++i)
	{
		commands[i]->setAffectedObject(gameObject);
	}
}
