#include "cFollowCurveCommand.h"
#include "cGameObject.h"

cFollowCurveCommand::cFollowCurveCommand()
{
}

cFollowCurveCommand::cFollowCurveCommand(cGameObject * theObject, float speed, glm::vec3 destination, glm::vec3 offPoint1)
{
	this->objectBeingAffected = theObject;
	this->startingPoint = objectBeingAffected->modelData.position;
	this->speed = speed;
	this->destination = destination;
	this->type = FollowCurveType::SPEED1POINT;
	this->offPoint1 = offPoint1;

	this->allottedTime = (glm::distance(startingPoint, destination) / speed) * 2;
	float tempTime;
	
	//speedPoints.push_back(startingPoint);
	//float tempTime = consumedTime / allottedTime;
	for (float i = 0; i < easeInPoints* percentageEaseStep;)
	{
		tempTime = i;
		this->speedPoints.push_back((1 - tempTime)*((1 - tempTime)*startingPoint + (tempTime* offPoint1)) +
			(tempTime*((1 - tempTime)*offPoint1 + tempTime*destination)));
		i += percentageEaseStep;
	}
	
	for (float percentage = easeInPoints * percentageEaseStep; percentage <= 1- (easeOutPoints * percentageEaseStep); )
	{
		tempTime = percentage;
		this->speedPoints.push_back((1 - tempTime)*((1 - tempTime)*startingPoint + (tempTime* offPoint1)) +
			(tempTime*((1 - tempTime)*offPoint1 + tempTime*destination)));
		percentage += percentageRegularstep;
	}

	for (float i = 1 - (easeOutPoints * percentageEaseStep); i < 1; )
	{
		tempTime = i;
		this->speedPoints.push_back((1 - tempTime)*((1 - tempTime)*startingPoint + (tempTime* offPoint1)) +
			(tempTime*((1 - tempTime)*offPoint1 + tempTime*destination)));
		i += percentageEaseStep;
	}

	
}

cFollowCurveCommand::cFollowCurveCommand(cGameObject * theObject, float speed, glm::vec3 destination, glm::vec3 offPoint1, glm::vec3 offPoint2)
{
	this->objectBeingAffected = theObject;
	this->startingPoint = objectBeingAffected->modelData.position;
	this->speed = speed;
	this->destination = destination;
	this->type = FollowCurveType::SPEED2POINT;
	this->offPoint1 = offPoint1;
	this->offPoint2 = offPoint2;

	float tempTime;
	for (float i = 0; i < easeInPoints* percentageEaseStep;)
	{
		tempTime = i;
			
		this->speedPoints.push_back(glm::pow((1 - tempTime), 3)*startingPoint +
			3 * (glm::pow((1 - tempTime), 2))*tempTime*offPoint1 +
			3 * (1 - tempTime)*glm::pow(tempTime, 2)*offPoint2 +
			glm::pow(tempTime, 3)*destination);
		i += percentageEaseStep;
	}

	for (float percentage = easeInPoints * percentageEaseStep; percentage <= 1 - (easeOutPoints * percentageEaseStep); )
	{
		tempTime = percentage;
		this->speedPoints.push_back(glm::pow((1 - tempTime), 3)*startingPoint +
			3 * (glm::pow((1 - tempTime), 2))*tempTime*offPoint1 +
			3 * (1 - tempTime)*glm::pow(tempTime, 2)*offPoint2 +
			glm::pow(tempTime, 3)*destination);
		percentage += percentageRegularstep;
	}

	for (float i = 1 - (easeOutPoints * percentageEaseStep); i < 1; )
	{
		tempTime = i;
		this->speedPoints.push_back(glm::pow((1 - tempTime), 3)*startingPoint +
			3 * (glm::pow((1 - tempTime), 2))*tempTime*offPoint1 +
			3 * (1 - tempTime)*glm::pow(tempTime, 2)*offPoint2 +
			glm::pow(tempTime, 3)*destination);
		i += percentageEaseStep;
	}

}

cFollowCurveCommand::cFollowCurveCommand(cGameObject * theObject, float speed, glm::vec3 destination, glm::vec3 offPoint1, int easeInPoints, int easeOutPoints)
{
	this->objectBeingAffected = theObject;
	this->startingPoint = objectBeingAffected->modelData.position;
	this->speed = speed;
	this->destination = destination;
	this->type = FollowCurveType::SPEED2POINT;
	this->offPoint1 = offPoint1;
	this->easeInPoints = easeInPoints;
	this->easeOutPoints = easeOutPoints;

	float tempTime;
	for (float i = 0; i < easeInPoints* percentageEaseStep;)
	{
		tempTime = i;
		this->speedPoints.push_back((1 - tempTime)*((1 - tempTime)*startingPoint + (tempTime* offPoint1)) +
			(tempTime*((1 - tempTime)*offPoint1 + tempTime*destination)));
		i += percentageEaseStep;
	}

	for (float percentage = easeInPoints * percentageEaseStep; percentage <= 1 - (easeOutPoints * percentageEaseStep); )
	{
		tempTime = percentage;
		this->speedPoints.push_back((1 - tempTime)*((1 - tempTime)*startingPoint + (tempTime* offPoint1)) +
			(tempTime*((1 - tempTime)*offPoint1 + tempTime*destination)));
		percentage += percentageRegularstep;
	}

	for (float i = 1 - (easeOutPoints * percentageEaseStep); i < 1; )
	{
		tempTime = i;
		this->speedPoints.push_back((1 - tempTime)*((1 - tempTime)*startingPoint + (tempTime* offPoint1)) +
			(tempTime*((1 - tempTime)*offPoint1 + tempTime*destination)));
		i += percentageEaseStep;
	}
}

cFollowCurveCommand::cFollowCurveCommand(cGameObject * theObject, float speed, glm::vec3 destination, glm::vec3 offPoint1, int easeInPoints, int easeOutPoints, float easePercentage)
{
	this->objectBeingAffected = theObject;
	this->startingPoint = objectBeingAffected->modelData.position;
	this->speed = speed;
	this->destination = destination;
	this->type = FollowCurveType::SPEED2POINT;
	this->offPoint1 = offPoint1;
	this->offPoint2 = offPoint2;
	this->easeInPoints = easeInPoints;
	this->easeOutPoints = easeOutPoints;
	this->percentageEaseStep = easePercentage;

	float tempTime;
	for (float i = 0; i < easeInPoints* percentageEaseStep;)
	{
		tempTime = i;

		this->speedPoints.push_back(glm::pow((1 - tempTime), 3)*startingPoint +
			3 * (glm::pow((1 - tempTime), 2))*tempTime*offPoint1 +
			3 * (1 - tempTime)*glm::pow(tempTime, 2)*offPoint2 +
			glm::pow(tempTime, 3)*destination);
		i += percentageEaseStep;
	}

	for (float percentage = easeInPoints * percentageEaseStep; percentage <= 1 - (easeOutPoints * percentageEaseStep); )
	{
		tempTime = percentage;
		this->speedPoints.push_back(glm::pow((1 - tempTime), 3)*startingPoint +
			3 * (glm::pow((1 - tempTime), 2))*tempTime*offPoint1 +
			3 * (1 - tempTime)*glm::pow(tempTime, 2)*offPoint2 +
			glm::pow(tempTime, 3)*destination);
		percentage += percentageRegularstep;
	}

	for (float i = 1 - (easeOutPoints * percentageEaseStep); i < 1; )
	{
		tempTime = i;
		this->speedPoints.push_back(glm::pow((1 - tempTime), 3)*startingPoint +
			3 * (glm::pow((1 - tempTime), 2))*tempTime*offPoint1 +
			3 * (1 - tempTime)*glm::pow(tempTime, 2)*offPoint2 +
			glm::pow(tempTime, 3)*destination);
		i += percentageEaseStep;
	}
}

cFollowCurveCommand::cFollowCurveCommand(cGameObject * theObject, float speed, glm::vec3 destination, glm::vec3 offPoint1, glm::vec3 offPoint2, int easeInPoints, int easeOutPoints)
{
	this->objectBeingAffected = theObject;
	this->startingPoint = objectBeingAffected->modelData.position;
	this->speed = speed;
	this->destination = destination;
	this->type = FollowCurveType::SPEED2POINT;
	this->offPoint1 = offPoint1;
	this->offPoint2 = offPoint2;
	this->easeInPoints = easeInPoints;
	this->easeOutPoints = easeOutPoints;

	float tempTime;
	for (float i = 0; i < easeInPoints* percentageEaseStep;)
	{
		tempTime = i;

		this->speedPoints.push_back(glm::pow((1 - tempTime), 3)*startingPoint +
			3 * (glm::pow((1 - tempTime), 2))*tempTime*offPoint1 +
			3 * (1 - tempTime)*glm::pow(tempTime, 2)*offPoint2 +
			glm::pow(tempTime, 3)*destination);
		i += percentageEaseStep;
	}

	for (float percentage = easeInPoints * percentageEaseStep; percentage <= 1 - (easeOutPoints * percentageEaseStep); )
	{
		tempTime = percentage;
		this->speedPoints.push_back(glm::pow((1 - tempTime), 3)*startingPoint +
			3 * (glm::pow((1 - tempTime), 2))*tempTime*offPoint1 +
			3 * (1 - tempTime)*glm::pow(tempTime, 2)*offPoint2 +
			glm::pow(tempTime, 3)*destination);
		percentage += percentageRegularstep;
	}

	for (float i = 1 - (easeOutPoints * percentageEaseStep); i < 1; )
	{
		tempTime = i;
		this->speedPoints.push_back(glm::pow((1 - tempTime), 3)*startingPoint +
			3 * (glm::pow((1 - tempTime), 2))*tempTime*offPoint1 +
			3 * (1 - tempTime)*glm::pow(tempTime, 2)*offPoint2 +
			glm::pow(tempTime, 3)*destination);
		i += percentageEaseStep;
	}
}

cFollowCurveCommand::cFollowCurveCommand(cGameObject * theObject, float speed, glm::vec3 destination, glm::vec3 offPoint1, glm::vec3 offPoint2, int easeInPoints, int easeOutPoints, float easePercentage)
{
	this->objectBeingAffected = theObject;
	this->startingPoint = objectBeingAffected->modelData.position;
	this->speed = speed;
	this->destination = destination;
	this->type = FollowCurveType::SPEED2POINT;
	this->offPoint1 = offPoint1;
	this->offPoint2 = offPoint2;
	this->easeInPoints = easeInPoints;
	this->easeOutPoints = easeOutPoints;
	this->percentageEaseStep = easePercentage;

	float tempTime;
	for (float i = 0; i < easeInPoints* percentageEaseStep;)
	{
		tempTime = i;

		this->speedPoints.push_back(glm::pow((1 - tempTime), 3)*startingPoint +
			3 * (glm::pow((1 - tempTime), 2))*tempTime*offPoint1 +
			3 * (1 - tempTime)*glm::pow(tempTime, 2)*offPoint2 +
			glm::pow(tempTime, 3)*destination);
		i += percentageEaseStep;
	}

	for (float percentage = easeInPoints * percentageEaseStep; percentage <= 1 - (easeOutPoints * percentageEaseStep); )
	{
		tempTime = percentage;
		this->speedPoints.push_back(glm::pow((1 - tempTime), 3)*startingPoint +
			3 * (glm::pow((1 - tempTime), 2))*tempTime*offPoint1 +
			3 * (1 - tempTime)*glm::pow(tempTime, 2)*offPoint2 +
			glm::pow(tempTime, 3)*destination);
		percentage += percentageRegularstep;
	}

	for (float i = 1 - (easeOutPoints * percentageEaseStep); i < 1; )
	{
		tempTime = i;
		this->speedPoints.push_back(glm::pow((1 - tempTime), 3)*startingPoint +
			3 * (glm::pow((1 - tempTime), 2))*tempTime*offPoint1 +
			3 * (1 - tempTime)*glm::pow(tempTime, 2)*offPoint2 +
			glm::pow(tempTime, 3)*destination);
		i += percentageEaseStep;
	}
}

cFollowCurveCommand::cFollowCurveCommand(cGameObject * theObject, glm::vec3 destination, glm::vec3 offPoint1, float time)
{
	this->objectBeingAffected = theObject;
	this->startingPoint = objectBeingAffected->modelData.position;
	this->allottedTime = time;
	this->consumedTime = 0;
	this->destination = destination;
	this->type = FollowCurveType::TIME1POINT;
	this->offPoint1 = offPoint1;

}

cFollowCurveCommand::cFollowCurveCommand(cGameObject * theObject, glm::vec3 destination, glm::vec3 offPoint1, glm::vec3 offPoint2, float time)
{
	this->objectBeingAffected = theObject;
	this->startingPoint = objectBeingAffected->modelData.position;
	this->allottedTime = time;
	this->consumedTime = 0;
	this->destination = destination;
	this->type = FollowCurveType::TIME2POINT;
	this->offPoint1 = offPoint1;
	this->offPoint2 = offPoint2;
}

cFollowCurveCommand::cFollowCurveCommand(float delay, cGameObject * theObject, float speed, glm::vec3 destination, glm::vec3 offPoint1)
{
	this->delayed = true;
	this->startDelay = delay;
	this->objectBeingAffected = theObject;
	this->startingPoint = objectBeingAffected->modelData.position;
	this->speed = speed;
	this->destination = destination;
	this->type = FollowCurveType::SPEED1POINT;
	this->offPoint1 = offPoint1;
}

cFollowCurveCommand::cFollowCurveCommand(float delay, cGameObject * theObject, float speed, glm::vec3 destination, glm::vec3 offPoint1, glm::vec3 offPoint2)
{
	this->delayed = true;
	this->startDelay = delay;
	this->objectBeingAffected = theObject;
	this->startingPoint = objectBeingAffected->modelData.position;
	this->speed = speed;
	this->destination = destination;
	this->type = FollowCurveType::SPEED2POINT;
	this->offPoint1 = offPoint1;
	this->offPoint2 = offPoint2;
}

cFollowCurveCommand::cFollowCurveCommand(float delay, cGameObject * theObject, glm::vec3 destination, glm::vec3 offPoint1, float time)
{
	this->delayed = true;
	this->startDelay = delay;
	this->objectBeingAffected = theObject;
	this->startingPoint = objectBeingAffected->modelData.position;
	this->allottedTime = time;
	this->consumedTime = 0;
	this->destination = destination;
	this->type = FollowCurveType::TIME1POINT;
	this->offPoint1 = offPoint1;

}

cFollowCurveCommand::cFollowCurveCommand(float delay, cGameObject * theObject, glm::vec3 destination, glm::vec3 offPoint1, glm::vec3 offPoint2, float time)
{
	this->delayed = true;
	this->startDelay = delay;
	this->objectBeingAffected = theObject;
	this->startingPoint = objectBeingAffected->modelData.position;
	this->allottedTime = time;
	this->consumedTime = 0;
	this->destination = destination;
	this->type = FollowCurveType::TIME2POINT;
	this->offPoint1 = offPoint1;
	this->offPoint2 = offPoint2;
}

cFollowCurveCommand::cFollowCurveCommand(float delay, cGameObject * theObject, float speed, glm::vec3 destination, glm::vec3 offPoint1, int easeInPoints, int easeOutPoints)
{
	this->delayed = true;
	this->startDelay = delay;

	this->objectBeingAffected = theObject;
	this->startingPoint = objectBeingAffected->modelData.position;
	this->speed = speed;
	this->destination = destination;
	this->type = FollowCurveType::SPEED2POINT;
	this->offPoint1 = offPoint1;
	this->offPoint2 = offPoint2;
	this->easeInPoints = easeInPoints;
	this->easeOutPoints = easeOutPoints;

	float tempTime;
	for (float i = 0; i < easeInPoints* percentageEaseStep;)
	{
		tempTime = i;

		this->speedPoints.push_back(glm::pow((1 - tempTime), 3)*startingPoint +
			3 * (glm::pow((1 - tempTime), 2))*tempTime*offPoint1 +
			3 * (1 - tempTime)*glm::pow(tempTime, 2)*offPoint2 +
			glm::pow(tempTime, 3)*destination);
		i += percentageEaseStep;
	}

	for (float percentage = easeInPoints * percentageEaseStep; percentage <= 1 - (easeOutPoints * percentageEaseStep); )
	{
		tempTime = percentage;
		this->speedPoints.push_back(glm::pow((1 - tempTime), 3)*startingPoint +
			3 * (glm::pow((1 - tempTime), 2))*tempTime*offPoint1 +
			3 * (1 - tempTime)*glm::pow(tempTime, 2)*offPoint2 +
			glm::pow(tempTime, 3)*destination);
		percentage += percentageRegularstep;
	}

	for (float i = 1 - (easeOutPoints * percentageEaseStep); i < 1; )
	{
		tempTime = i;
		this->speedPoints.push_back(glm::pow((1 - tempTime), 3)*startingPoint +
			3 * (glm::pow((1 - tempTime), 2))*tempTime*offPoint1 +
			3 * (1 - tempTime)*glm::pow(tempTime, 2)*offPoint2 +
			glm::pow(tempTime, 3)*destination);
		i += percentageEaseStep;
	}
}

cFollowCurveCommand::cFollowCurveCommand(float delay, cGameObject * theObject, float speed, glm::vec3 destination, glm::vec3 offPoint1, int easeInPoints, int easeOutPoints, float easePercentage)
{
	this->delayed = true;
	this->startDelay = delay;

	this->objectBeingAffected = theObject;
	this->startingPoint = objectBeingAffected->modelData.position;
	this->speed = speed;
	this->destination = destination;
	this->type = FollowCurveType::SPEED2POINT;
	this->offPoint1 = offPoint1;
	this->offPoint2 = offPoint2;
	this->easeInPoints = easeInPoints;
	this->easeOutPoints = easeOutPoints;
	this->percentageEaseStep = easePercentage;

	float tempTime;
	for (float i = 0; i < easeInPoints* percentageEaseStep;)
	{
		tempTime = i;

		this->speedPoints.push_back(glm::pow((1 - tempTime), 3)*startingPoint +
			3 * (glm::pow((1 - tempTime), 2))*tempTime*offPoint1 +
			3 * (1 - tempTime)*glm::pow(tempTime, 2)*offPoint2 +
			glm::pow(tempTime, 3)*destination);
		i += percentageEaseStep;
	}

	for (float percentage = easeInPoints * percentageEaseStep; percentage <= 1 - (easeOutPoints * percentageEaseStep); )
	{
		tempTime = percentage;
		this->speedPoints.push_back(glm::pow((1 - tempTime), 3)*startingPoint +
			3 * (glm::pow((1 - tempTime), 2))*tempTime*offPoint1 +
			3 * (1 - tempTime)*glm::pow(tempTime, 2)*offPoint2 +
			glm::pow(tempTime, 3)*destination);
		percentage += percentageRegularstep;
	}

	for (float i = 1 - (easeOutPoints * percentageEaseStep); i < 1; )
	{
		tempTime = i;
		this->speedPoints.push_back(glm::pow((1 - tempTime), 3)*startingPoint +
			3 * (glm::pow((1 - tempTime), 2))*tempTime*offPoint1 +
			3 * (1 - tempTime)*glm::pow(tempTime, 2)*offPoint2 +
			glm::pow(tempTime, 3)*destination);
		i += percentageEaseStep;
	}
}

cFollowCurveCommand::cFollowCurveCommand(float delay, cGameObject * theObject, float speed, glm::vec3 destination, glm::vec3 offPoint1, glm::vec3 offPoint2, int easeInPoints, int easeOutPoints)
{
	this->delayed = true;
	this->startDelay = delay;
	this->objectBeingAffected = theObject;
	this->startingPoint = objectBeingAffected->modelData.position;
	this->speed = speed;
	this->destination = destination;
	this->type = FollowCurveType::SPEED2POINT;
	this->offPoint1 = offPoint1;
	this->offPoint2 = offPoint2;
	this->easeInPoints = easeInPoints;
	this->easeOutPoints = easeOutPoints;

	float tempTime;
	for (float i = 0; i < easeInPoints* percentageEaseStep;)
	{
		tempTime = i;

		this->speedPoints.push_back(glm::pow((1 - tempTime), 3)*startingPoint +
			3 * (glm::pow((1 - tempTime), 2))*tempTime*offPoint1 +
			3 * (1 - tempTime)*glm::pow(tempTime, 2)*offPoint2 +
			glm::pow(tempTime, 3)*destination);
		i += percentageEaseStep;
	}

	for (float percentage = easeInPoints * percentageEaseStep; percentage <= 1 - (easeOutPoints * percentageEaseStep); )
	{
		tempTime = percentage;
		this->speedPoints.push_back(glm::pow((1 - tempTime), 3)*startingPoint +
			3 * (glm::pow((1 - tempTime), 2))*tempTime*offPoint1 +
			3 * (1 - tempTime)*glm::pow(tempTime, 2)*offPoint2 +
			glm::pow(tempTime, 3)*destination);
		percentage += percentageRegularstep;
	}

	for (float i = 1 - (easeOutPoints * percentageEaseStep); i < 1; )
	{
		tempTime = i;
		this->speedPoints.push_back(glm::pow((1 - tempTime), 3)*startingPoint +
			3 * (glm::pow((1 - tempTime), 2))*tempTime*offPoint1 +
			3 * (1 - tempTime)*glm::pow(tempTime, 2)*offPoint2 +
			glm::pow(tempTime, 3)*destination);
		i += percentageEaseStep;
	}
}

cFollowCurveCommand::cFollowCurveCommand(float delay, cGameObject * theObject, float speed, glm::vec3 destination, glm::vec3 offPoint1, glm::vec3 offPoint2, int easeInPoints, int easeOutPoints, float easePercentage)
{
	this->delayed = true;
	this->startDelay = delay;

	this->objectBeingAffected = theObject;
	this->startingPoint = objectBeingAffected->modelData.position;
	this->speed = speed;
	this->destination = destination;
	this->type = FollowCurveType::SPEED2POINT;
	this->offPoint1 = offPoint1;
	this->offPoint2 = offPoint2;
	this->easeInPoints = easeInPoints;
	this->easeOutPoints = easeOutPoints;
	this->percentageEaseStep = easePercentage;

	float tempTime;
	for (float i = 0; i < easeInPoints* percentageEaseStep;)
	{
		tempTime = i;

		this->speedPoints.push_back(glm::pow((1 - tempTime), 3)*startingPoint +
			3 * (glm::pow((1 - tempTime), 2))*tempTime*offPoint1 +
			3 * (1 - tempTime)*glm::pow(tempTime, 2)*offPoint2 +
			glm::pow(tempTime, 3)*destination);
		i += percentageEaseStep;
	}

	for (float percentage = easeInPoints * percentageEaseStep; percentage <= 1 - (easeOutPoints * percentageEaseStep); )
	{
		tempTime = percentage;
		this->speedPoints.push_back(glm::pow((1 - tempTime), 3)*startingPoint +
			3 * (glm::pow((1 - tempTime), 2))*tempTime*offPoint1 +
			3 * (1 - tempTime)*glm::pow(tempTime, 2)*offPoint2 +
			glm::pow(tempTime, 3)*destination);
		percentage += percentageRegularstep;
	}

	for (float i = 1 - (easeOutPoints * percentageEaseStep); i < 1; )
	{
		tempTime = i;
		this->speedPoints.push_back(glm::pow((1 - tempTime), 3)*startingPoint +
			3 * (glm::pow((1 - tempTime), 2))*tempTime*offPoint1 +
			3 * (1 - tempTime)*glm::pow(tempTime, 2)*offPoint2 +
			glm::pow(tempTime, 3)*destination);
		i += percentageEaseStep;
	}
}

cFollowCurveCommand::~cFollowCurveCommand()
{
}

void cFollowCurveCommand::Update()
{
}

void cFollowCurveCommand::Update(float deltaTime)
{
	if (Ready(deltaTime)) {
		if (firstUpdate)
		{
			checkIfInLocationTriggerAtStart();
			firstUpdate = false;
		}

		switch (type) {
		case FollowCurveType::SPEED1POINT:case FollowCurveType::SPEED2POINT:
			if (pointNum == speedPoints.size())
				complete = true;
			else
			{
				float tempSpeed = speed;
				//change the speed based on ease in ease out
				//ease in
				if (pointNum <= easeInPoints)
				{
					tempSpeed *= ((float)pointNum / (float)easeInPoints);
				}
				//ease out
				else if(pointNum >= (speedPoints.size() - easeOutPoints))
				{
					tempSpeed *= 1.0f- ((float)(pointNum - (speedPoints.size()-easeOutPoints)) / (float)(easeOutPoints));
				}


				objectBeingAffected->modelData.position += glm::normalize(speedPoints[pointNum] - objectBeingAffected->modelData.position) * tempSpeed;
				if (glm::distance(objectBeingAffected->modelData.position, speedPoints[pointNum]) < tempSpeed)
				{
					pointNum++;
				}
			}

			break;
		case FollowCurveType::TIME1POINT:
			consumedTime += deltaTime;

			if (consumedTime > allottedTime)
				complete = true;
			else
			{
				//calulate the new position using a bezier curve
				float tempTime = consumedTime / allottedTime;
				objectBeingAffected->modelData.position =
					(1 - tempTime)*((1 - tempTime)*startingPoint + (tempTime* offPoint1)) +
					(tempTime*((1 - tempTime)*offPoint1 + tempTime*destination));
			}
			break;
		case FollowCurveType::TIME2POINT:
			consumedTime += deltaTime;
			if (consumedTime > allottedTime)
				complete = true;
			else
			{
				//calulate the new position using a bezier curve
				float tempTime = consumedTime / allottedTime;
				objectBeingAffected->modelData.position =
					glm::pow((1 - tempTime), 3)*startingPoint +
					3 * (glm::pow((1 - tempTime), 2))*tempTime*offPoint1 +
					3 * (1 - tempTime)*glm::pow(tempTime, 2)*offPoint2 +
					glm::pow(tempTime, 3)*destination;
			}
			break;
		}

		checkIfInLocationTrigger();
	}
}

cCommand * cFollowCurveCommand::genCopy()
{
	cFollowCurveCommand* newCommand = new cFollowCurveCommand();
	//newCommand->objectBeingAffected = this->objectBeingAffected;
	//newCommand->startingPoint = this->startingPoint;
	newCommand->speed = this->speed;
	newCommand->destination = this->destination;
	newCommand->type = this->type;
	newCommand->offPoint1 = this->offPoint1;
	newCommand->offPoint2 = this->offPoint2;
	newCommand->allottedTime = this->allottedTime;
	newCommand->consumedTime = 0;
	newCommand->startDelay = this->startDelay;
	return newCommand;
}

void cFollowCurveCommand::setAffectedObject(cGameObject * gameObject)
{
	this->objectBeingAffected = gameObject;
	this->startingPoint = gameObject->modelData.position;
}
