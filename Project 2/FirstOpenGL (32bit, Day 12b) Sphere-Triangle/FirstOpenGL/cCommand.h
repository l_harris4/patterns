#ifndef CCOMMAND_HG
#define CCOMMAND_HG
#include <glm\vec3.hpp>
#include <vector>


class cGameObject;
class cLocationTrigger;
class cCommandGroup;

class cCommand
{
public:
	cCommand();
	virtual ~cCommand();
	bool complete = false;
	virtual void Update();
	virtual void Update(float deltaTime);
	virtual void setAffectedObject(cGameObject * gameObject);
	virtual cCommand* genCopy();
	cGameObject* objectBeingAffected;
	float startDelay;
	bool delayed = false;
	bool Ready(float deltaTime);
	std::string ID;
	
	void checkIfInLocationTrigger();
	void checkIfInLocationTriggerAtStart();
	bool firstUpdate = true;
	std::vector<cLocationTrigger*> locationTriggersTripped;

	float smoothstep(float edge0, float edge1, float x);

	float smootherstep(float edge0, float edge1, float x);

	float clamp(float x, float lowerlimit, float upperlimit);
};


#endif // !CCOMMAND_HG

