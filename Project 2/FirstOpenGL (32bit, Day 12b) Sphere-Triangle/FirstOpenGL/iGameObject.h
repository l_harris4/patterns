#ifndef _IGAMEOBJECT_HG
#define _IGAMEOBJECT_HG

#include <glm\vec3.hpp>
#include <glm\vec4.hpp>
#include <glm\geometric.hpp>
#include <string>
#include <vector>
//forward declaration
class cModelData;

//interface for game objects
class iGameObject
{
public:
	virtual ~iGameObject() {};		// Make this virtual, too

	//used to update game objects based on time
	virtual void Update(std::vector< iGameObject* >*  g_vecGameObjects, double deltaTime) = 0;

	virtual cModelData* GetModelData() = 0;
	virtual std::string GetType() = 0;
	virtual void SetType(std::string value) = 0;
};



#endif // !_IGAMEOBJECT_HG

