#ifndef _cMesh_HG_
#define _cMesh_HG_

#include "cVertex_xyz_rgb_n.h"
#include "cTriangle.h" 

#include <string>
#include <glm/vec3.hpp>
#include <vector>

class cMesh
{
public:
	//ctor and dtor
	cMesh();
	~cMesh();

	std::string name;	// the friendly name of the mesh

	cVertex_xyz_rgb_n* pVertices;
	int numberOfVertices;

	cTriangle* pTriangles;
	int numberOfTriangles;

	// Used for the physics calculations
	void GeneratePhysicsTriangles(void);

	// Takes an indexed model and makes just a vertex array model
	// WARNING: Will OVERWRITE the vertex array 
	void FlattenIndexedModel(void);

	// First 2 are the "bounding box" edges
	glm::vec3 minXYZ;
	glm::vec3 maxXYZ;
	// This is Max - Min for each x,y,z
	glm::vec3 maxExtentXYZ;	
	float maxExtent;		// Biggest of above
	// Scale can be 1.0 div maxExtent;
	float scaleForUnitBBox;

	void CalculateExtents(void);

	void CalculateNormals(void);
};


#endif
