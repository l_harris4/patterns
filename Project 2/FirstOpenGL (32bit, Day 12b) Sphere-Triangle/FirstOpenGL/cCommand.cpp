#include "cCommand.h"
#include "cGameObjectMediator.h"
#include "cGameObject.h"
#include <time.h> 

std::string RandomID(const int len) {
	std::string returnString = "";
	static const char alphanum[] =
		"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	int size = (sizeof(alphanum) - 1);

	srand((unsigned)time(NULL));

	for (int i = 0; i < len; ++i) {
		int random = rand() % size;
		char add = alphanum[random];
		returnString += add;
	}

	return returnString;
}

cCommand::cCommand()
{
	ID = RandomID(5);
}

cCommand::~cCommand()
{
}

void cCommand::Update()
{

}

void cCommand::Update(float deltaTime)
{
}

void cCommand::setAffectedObject(cGameObject * gameObject)
{
	objectBeingAffected = gameObject;
}

cCommand * cCommand::genCopy()
{
	//this should never be called, gen copy should always polymorph down to an actual command
	return nullptr;
}

bool cCommand::Ready(float deltaTime)
{
	if (delayed)
	{
		this->startDelay -= deltaTime;
		if ((startDelay -= deltaTime) < 0)
			delayed = false;
	}
	return !delayed && !complete;
}

void cCommand::checkIfInLocationTrigger()
{
	//now check if it is in a new location based trigger 
	//go through all the location based triggers
	int size = cGameObjectMediator::GetInstance()->vecLocationTriggers.size();
	for (int i = 0; i < size; i++)
	{
		//see if the object being affected is inside one
		if (cGameObjectMediator::GetInstance()->vecLocationTriggers[i]->InLocation(objectBeingAffected->modelData.position))
		{
			//check if that location has already been tripped
			if (std::find(locationTriggersTripped.begin(), locationTriggersTripped.end(), cGameObjectMediator::GetInstance()->vecLocationTriggers[i]) != locationTriggersTripped.end())
			{
				//do nothing
			}
			//if not set the create a new command 
			else
			{
				locationTriggersTripped.push_back(cGameObjectMediator::GetInstance()->vecLocationTriggers[i]);
				//set the object being affected to this
				cCommandGroup* copyiedCommandGroup = (cCommandGroup*)cGameObjectMediator::GetInstance()->vecLocationTriggers[i]->commandGroup.genCopy();
				copyiedCommandGroup->setAffectedObject(objectBeingAffected);

				
				if (cGameObjectMediator::GetInstance()->vecCommands.size() >= 1)
				{
					//add it to being currently run
					cGameObjectMediator::GetInstance()->vecCommands[i]->commands.push_back(copyiedCommandGroup);
				}
				else
				{
					//add to end
					cGameObjectMediator::GetInstance()->vecCommands.push_back(copyiedCommandGroup);
				}

				//add it onto the end
				//cGameObjectMediator::GetInstance()->vecCommands.push_back(copyiedCommandGroup);
			}


		}
	}
}

void cCommand::checkIfInLocationTriggerAtStart()
{
	//now check if it is in a new location based trigger 
	//go through all the location based triggers
	int size = cGameObjectMediator::GetInstance()->vecLocationTriggers.size();
	for (int i = 0; i < size; i++)
	{
		//see if the object being affected is inside one
		if (cGameObjectMediator::GetInstance()->vecLocationTriggers[i]->InLocation(objectBeingAffected->modelData.position))
		{
			//check if that location has already been tripped
			if (std::find(locationTriggersTripped.begin(), locationTriggersTripped.end(), cGameObjectMediator::GetInstance()->vecLocationTriggers[i]) != locationTriggersTripped.end())
			{
				//do nothing
			}
			//if not set the create a new command 
			else
			{
				locationTriggersTripped.push_back(cGameObjectMediator::GetInstance()->vecLocationTriggers[i]);
			}


		}
	}
}


float cCommand::smoothstep(float edge0, float edge1, float x) {
	// Scale, bias and saturate x to 0..1 range
	x = clamp((x - edge0) / (edge1 - edge0), 0.0, 1.0);
	// Evaluate polynomial
	return x * x * (3 - 2 * x);
}

float cCommand::smootherstep(float edge0, float edge1, float x) {
	// Scale, and clamp x to 0..1 range
	x = clamp((x - edge0) / (edge1 - edge0), 0.0, 1.0);
	// Evaluate polynomial
	return x * x * x * (x * (x * 6 - 15) + 10);
}

float cCommand::clamp(float x, float lowerlimit, float upperlimit) {
	if (x < lowerlimit)
		x = lowerlimit;
	if (x > upperlimit)
		x = upperlimit;
	return x;
}
