#include "cCameraTargetCommand.h"
#include "cCamera.h"

cCameraTargetCommand::cCameraTargetCommand(cGameObject * camera, cGameObject * followObject)
{
	this->objectBeingAffected = camera;
	this->followObject = followObject;
	type = TargetType::FOLLOW;
}

cCameraTargetCommand::cCameraTargetCommand(float delay, cGameObject * camera, cGameObject * followObject)
{
	this->startDelay = delay;
	this->delayed = true;
	this->objectBeingAffected = camera;
	this->followObject = followObject;
	type = TargetType::FOLLOW;
}

cCameraTargetCommand::cCameraTargetCommand(cGameObject * camera)
{
	type = TargetType::FLY;
	this->objectBeingAffected = camera;
}

cCameraTargetCommand::cCameraTargetCommand(float delay, cGameObject * camera)
{
	this->startDelay = delay;
	this->delayed = true;
	type = TargetType::FLY;
	this->objectBeingAffected = camera;
}

cCameraTargetCommand::~cCameraTargetCommand()
{
}

void cCameraTargetCommand::Update()
{
}

void cCameraTargetCommand::Update(float deltaTime)
{
	
	if (Ready(deltaTime))
	{
		switch (type)
		{
		case TargetType::FLY:
			((cCamera*)objectBeingAffected)->type = eMode::FLY_CAMERA;
			break;
		case TargetType::FOLLOW:
			((cCamera*)objectBeingAffected)->type = eMode::FOLLOW_CAMERA;
			((cCamera*)objectBeingAffected)->followTarget = followObject;
			break;
		}

		complete = true;
	}
}
