#ifndef CCAMERATARGETCOMMAND_HG
#define CCAMERATARGETCOMMAND_HG
#include "cCommand.h"

enum TargetType
{
	FLY = 1,
	FOLLOW = 2
};

class cCameraTargetCommand : public cCommand
{
public:
	cCameraTargetCommand(cGameObject * camera, cGameObject * followObject);
	cCameraTargetCommand(float delay, cGameObject * camera, cGameObject * followObject);
	cCameraTargetCommand(cGameObject * camera);
	cCameraTargetCommand(float delay, cGameObject * camera);
	~cCameraTargetCommand();
	virtual void Update();
	virtual void Update(float deltaTime);
	TargetType type;
	cGameObject* followObject;
	/*virtual cCommand* genCopy();
	virtual void setAffectedObject(cGameObject * gameObject);*/

	//((cCamera*)vecGameObjects[cameraId])->type = eMode::FOLLOW_CAMERA;
	//((cCamera*)vecGameObjects[cameraId])->followTarget = (cGameObject*)vecGameObjects[4];


};

#endif // !CCAMERATARGETCOMMAND_HG
