#ifndef CCAMERA_HG
#define CCAMERA_HG
#include "cGameObject.h"
#include <glm/mat4x4.hpp>

enum eMode
{
	FOLLOW_CAMERA,	// Follows a target (lookat)
	FLY_CAMERA		// Movement based on direction of gaze
};
class cCamera : public cGameObject
{
public:
	cCamera();
	eMode type;
	glm::mat4 getViewMatrix();
	//cGameObject* followTarget;
};

#endif // !CCAMERA_HG

