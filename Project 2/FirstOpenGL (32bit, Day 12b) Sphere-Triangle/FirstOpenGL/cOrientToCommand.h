#ifndef CORIENTTOCOMMAND_HG
#define CORIENTTOCOMMAND_HG
#include "cCommand.h"
#include <glm\vec3.hpp>

enum OrientCommandType {
	SPEEDOTYPE = 1,
	TIMEOTYPE = 2,
	INSTANTO = 3
};

class cOrientToCommand : public cCommand
{
public:
	cOrientToCommand();
	cOrientToCommand(cGameObject * theObject,glm::vec3 targetOrientation, float speed);
	cOrientToCommand(cGameObject * theObject,float time, glm::vec3 targetOrientation);
	cOrientToCommand(cGameObject * theObject, glm::vec3 targetOrientation);

	cOrientToCommand(cGameObject * theObject, glm::vec3 targetOrientation, float speed, int easeInSteps, int easeOutSteps);

	cOrientToCommand(float delay, cGameObject * theObject, glm::vec3 targetOrientation, float speed);
	cOrientToCommand(float delay, cGameObject * theObject, float time, glm::vec3 targetOrientation);
	cOrientToCommand(float delay, cGameObject * theObject, glm::vec3 targetOrientatio);

	cOrientToCommand(float delay, cGameObject * theObject, glm::vec3 targetOrientation, float speed, int easeInSteps, int easeOutSteps);

	~cOrientToCommand();
	void fillOutSpeedPoints();
	virtual void Update();
	virtual void Update(float deltaTime);
	glm::vec3 targetOrientation;
	glm::vec3 startingOrientation;
	float timeConsumed;
	float timeAlotted;
	float turnSpeed;
	float maxTurnSpeed;
	float startingTurnDistance;
	float distanceTurnPortion;
	float timeToFull;
	virtual cCommand* genCopy();
	virtual void setAffectedObject(cGameObject * gameObject);
	//bool completed = false;
	OrientCommandType type;

	//defaults
	int easeInPoints = 40;
	int easeOutPoints = 10;
	std::vector<glm::vec3> speedPoints;
	int pointNum = 1;
};
#endif // !CORIENTTOCOMMAND_HG
