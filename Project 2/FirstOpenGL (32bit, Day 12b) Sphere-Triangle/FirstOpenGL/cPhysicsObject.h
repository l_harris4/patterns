#ifndef _CPHYSICSOBJECT_HG_
#define _CPHYSICSOBJECT_HG_

#include <glm\vec3.hpp>

class cPhysicsObject
{
public:
	//ctor and dtor
	cPhysicsObject();
	~cPhysicsObject();
	//data members needed
	glm::vec3 velocity;
	glm::vec3 holderVelocity;
	glm::vec3 acceleration;
	bool collidedThisFrame;
	////used to update the sphere based on time
	//virtual void Update(std::vector< iGameObject* >*  g_vecGameObjects, double deltaTime);
	////methods
	//void MoveTo(glm::vec3);
};

#endif // !_CPHYSICSOBJECT_HG_

