#include "cFollowCommand.h"
#include "cGameObject.h"

cFollowCommand::cFollowCommand()
{
}

cFollowCommand::cFollowCommand(cGameObject * theObject, float distance, cGameObject * followObject, glm::vec3 relativeToTarget, float followTime)
{
	this->type = FollowType::DISTANCE;
	this->awayDistance = distance;   //Distance not being used currently
	this->objectBeingAffected = theObject;
	this->followObject = followObject;
	this->relativeToTarget = relativeToTarget;
	this->followTime = followTime;
}

cFollowCommand::cFollowCommand(float speed, cGameObject * theObject, cGameObject * followObject, float closeDistance, float farDistance, glm::vec3 relativeToTarget, float followTime)
{
	this->relativeToTarget = relativeToTarget;
	this->type = FollowType::SPEED;
	this->followSpeed = speed;
	this->objectBeingAffected = theObject;
	this->followObject = followObject;
	this->awayDistance = closeDistance;
	this->farDistance = farDistance;
	this->followTime = followTime;
}

cFollowCommand::cFollowCommand(float delay, cGameObject * theObject, float distance, cGameObject * followObject, glm::vec3 relativeToTarget, float followTime)
{
	this->type = FollowType::DISTANCE;
	this->awayDistance = distance;   //Distance not being used currently
	this->objectBeingAffected = theObject;
	this->followObject = followObject;
	this->relativeToTarget = relativeToTarget;
	this->followTime = followTime;
	this->startDelay = delay;
	this->delayed = true;
}

cFollowCommand::cFollowCommand(float delay, float speed, cGameObject * theObject, cGameObject * followObject, float closeDistance, float farDistance, glm::vec3 relativeToTarget, float followTime)
{
	this->relativeToTarget = relativeToTarget;
	this->type = FollowType::SPEED;
	this->followSpeed = speed;
	this->objectBeingAffected = theObject;
	this->followObject = followObject;
	this->awayDistance = closeDistance;
	this->farDistance = farDistance;
	this->followTime = followTime;
	this->startDelay = delay;
	this->delayed = true;
}

cFollowCommand::~cFollowCommand()
{
}

void cFollowCommand::Update()
{
}

void cFollowCommand::Update(float deltaTime)
{
	if (Ready(deltaTime)) {
		if (firstUpdate)
		{
			checkIfInLocationTriggerAtStart();
			firstUpdate = false;
		}

		if (timed) {
			if (followTime > 0)
				followTime -= deltaTime;
			else
				complete = true;
		}

		switch (type)
		{
		case DISTANCE:
			//if it is distance just calculate the the new position for the followerObject
			//if (glm::distance(followObject->modelData.position, objectBeingAffected->modelData.position) > awayDistance) {
			objectBeingAffected->modelData.position = followObject->modelData.position + relativeToTarget;
			//}
			break;
		case SPEED:

			// Calculate ideal location in world
			glm::vec3 camIdealPositionWorld
				= this->followObject->modelData.position + this->relativeToTarget;

			// How far away from the object are we?
			glm::vec3 vectorToTarget = camIdealPositionWorld - objectBeingAffected->modelData.position;
			glm::vec3 vectorToTargetNormal = glm::normalize(vectorToTarget);

			// Pro tip: don't use .length();
			float currentDistanceToTarget = glm::length(vectorToTarget);


			// Pass all that into the smooth (or smoother) step 
			// 1st is distance to Zero Speed "circle"
			// 2nd is distance to Max Speed "circle"
			float speedRatio
				= smootherstep(this->awayDistance,
					this->farDistance,
					currentDistanceToTarget);

			float scaledMaxSpeed = speedRatio * this->followSpeed;// this->farDistance;

			// Ideal max speed
			glm::vec3 vecMaxSpeed = glm::vec3(scaledMaxSpeed,
				scaledMaxSpeed,
				scaledMaxSpeed);

			glm::vec3 velocity = vecMaxSpeed * vectorToTargetNormal;

			objectBeingAffected->modelData.position += velocity * deltaTime;
			break;
		}

		checkIfInLocationTrigger();
	}
}

cCommand * cFollowCommand::genCopy()
{
	cFollowCommand* newCommand = new cFollowCommand();
	newCommand->type = this->type;
	newCommand->awayDistance = this->awayDistance;
	//newCommand->objectBeingAffected = this->objectBeingAffected;
	newCommand->followObject = this->followObject;
	newCommand->relativeToTarget = this->relativeToTarget;
	newCommand->followSpeed = this->followSpeed;
	newCommand->followObject = this->followObject;
	newCommand->awayDistance = this->awayDistance;
	newCommand->farDistance = this->farDistance;
	newCommand->startDelay = this->startDelay;
	return newCommand;
}

void cFollowCommand::setAffectedObject(cGameObject * gameObject)
{
	this->objectBeingAffected = gameObject;
}
