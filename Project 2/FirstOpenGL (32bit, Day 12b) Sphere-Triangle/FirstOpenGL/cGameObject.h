#ifndef _cGameObject_HG_
#define _cGameObject_HG_

#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glm/mat4x4.hpp>

#include <string>

#include "iGameObject.h"
#include "cModelData.h"
class cModelData;

class cGameObject : public iGameObject
{
public:
	cGameObject();		// constructor
	virtual ~cGameObject();		// destructor

	//Keep track of model data, ie: position, scale...
	cModelData modelData;

	//needed getters and setters
	virtual cModelData* GetModelData();
	virtual std::string GetType();
	virtual void SetType(std::string value);

	//method to update the game object based on time
	virtual void Update(std::vector< iGameObject* >*  g_vecGameObjects, double deltaTime);

	//methods to be used by the lua scripts
	virtual void MoveTo(glm::vec3 target, float time);
	virtual void RotateTo(glm::vec3 rotation, float time);
	virtual void Follow(cGameObject* target, float distance, float followT);

	void overwriteQOrientationFormEuler(glm::vec3 eulerAxisOrientation);

	void adjustQOrientationFormDeltaEuler(glm::vec3 eulerAxisOrientChange);

	//attributes needed for the lua scripts
	glm::vec3 moveTarget;
	glm::vec3 rotationTarget;
	cGameObject* followTarget;
	bool moving;
	bool rotating;
	bool following;
	float moveTime;
	float rotationSpeed;
	float followingDistance;
	float followSpeed;

	// string used to determine the type of the object
	std::string typeOfObject;
	// used to keep track of if the object is guaranteed to be unchanged
	// used when computing colliding triangles
	bool editable;
};

#endif
