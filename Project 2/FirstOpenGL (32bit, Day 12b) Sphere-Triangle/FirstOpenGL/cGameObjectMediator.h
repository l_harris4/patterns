#ifndef _CGAMEOBJECTMEDIATOR_HG_
#define _CGAMEOBJECTMEDIATOR_HG_
#include <vector>
#include <glm\vec3.hpp>
#include <glm\mat4x4.hpp>
#include "cLocationTrigger.h"
extern "C" {
#include <Lua\lua.hpp>
#include <Lua\lauxlib.h>
#include <Lua\lua.h>
#include <Lua\luaconf.h>
#include <Lua\lualib.h>
}

//extern "C" {
//#include <Lua5.3.3\lua.h>
//#include <Lua5.3.3\lua.h>
//#include <Lua5.3.3\lauxlib.h>
//#include <Lua5.3.3/lualib.h>
//}


//forward declarations of classes
class cSphere;
class cPhysTriangle;
class iGameObject;
class cGameObject;
class cCommand;

class cGameObjectMediator
{
public:
	//the vector of all game objects
	std::vector< iGameObject* >  vecGameObjects;
	//the vector of commands
	std::vector<cCommandGroup*> vecCommands;

	std::vector<cCommandGroup*> tempVecCommands;

	cCommandGroup* addingCommandGroup;
	bool addingIntoLocationTrigger = false;


	std::vector<std::vector<cCommandGroup*>> tempVecCommandGroup;
	//the vector of location based triggers
	std::vector<cLocationTrigger*> vecLocationTriggers;
	//ctor and dtor
	
	~cGameObjectMediator();
	lua_State* state;

	static cGameObjectMediator* GetInstance();
	//std::vector< iGameObject*>* GetVecGameObjects();

	void AddToCommands(cCommand* command);


	//used for moving one sphere
	void MoveMainSphere(glm::vec3 force);
	//used for updating all the game objects
	void UpdateGameObjects(float deltaTime);
	//used for getting all the colliding triangles for a specific object
	bool GetCollidingTriangles(iGameObject* piSphere, iGameObject* piTriangleObject, std::vector<cPhysTriangle>& closestTriangles);
	//computing all the interactions for a sphere
	void ComputeInteractions(cSphere* sphere);

	//methods to be used by the lua scripts
	void MoveObjectTo(int ID, glm::vec3 target, float time);
	void RotateObjectTo(int ID, glm::vec3 rotationMatrix, float time);
	void FollowObjectTo(int ID, int ID2, float distance, float followT);
	glm::mat4 getViewMatrix();
	int cameraId = -1;
	int tempCommandLevel = 0;
	static int doubleIt(lua_State* L);
	static int endGroup(lua_State* L);
	static int startGroup(lua_State* L);

	static int followDistanceCommand(lua_State* L);
	static int followSpeedCommand(lua_State* L);
	static int curveSpeedSinglePointCommand(lua_State* L);
	static int curveSpeedDoublePointCommand(lua_State* L);

	static int curveSpeedEaseInEaseOutSinglePointCommand(lua_State* L);
	static int curveSpeedEaseInEaseOutDoublePointCommand(lua_State* L);
	static int curveSpeedEaseInEaseOutEasePercentageSinglePointCommand(lua_State* L);
	static int curveSpeedEaseInEaseOutEasePercentageDoublePointCommand(lua_State* L);
	static int curveSpeedEaseInEaseOutSinglePointCommandDelay(lua_State* L);
	static int curveSpeedEaseInEaseOutDoublePointCommandDelay(lua_State* L);
	static int curveSpeedEaseInEaseOutEasePercentageSinglePointCommandDelay(lua_State* L);
	static int curveSpeedEaseInEaseOutEasePercentageDoublePointCommandDelay(lua_State* L);

	static int curveTimeSinglePointCommand(lua_State* L);
	static int curveTimeDoublePointCommand(lua_State* L);
	static int moveSpeedEaseInEaseOutCommand(lua_State* L);
	static int moveSpeedEaseInEaseOutEasePercentageCommand(lua_State* L);
	static int moveSpeedEaseInEaseOutCommandDelay(lua_State* L);
	static int moveSpeedEaseInEaseOutEasePercentageCommandDelay(lua_State* L);
	static int moveSpeedCommand(lua_State* L);
	static int moveTimeCommand(lua_State* L);
	static int moveInstantCommand(lua_State* L);
	static int orientInstantCommand(lua_State* L);
	static int orientSpeedCommand(lua_State* L);

	static int orientSpeedEaseInEaseOutCommand(lua_State* L);
	static int orientSpeedEaseInEaseOutCommandDelay(lua_State* L);

	static int orientTimeCommand(lua_State* L);
	static int cameraTargetCommand(lua_State* L);

	static int moveInstantCommandDelay(lua_State * L);
	static int followDistanceCommandDelay(lua_State* L);
	static int followSpeedCommandDelay(lua_State* L);
	static int curveSpeedSinglePointCommandDelay(lua_State* L);
	static int curveSpeedDoublePointCommandDelay(lua_State* L);
	static int curveTimeSinglePointCommandDelay(lua_State* L);
	static int curveTimeDoublePointCommandDelay(lua_State* L);
	static int moveSpeedCommandDelay(lua_State* L);
	static int moveTimeCommandDelay(lua_State* L);
	static int orientInstantCommandDelay(lua_State* L);
	static int orientSpeedCommandDelay(lua_State* L);
	static int orientTimeCommandDelay(lua_State* L);
	static int cameraTargetCommandDelay(lua_State* L);
	
	static int sphereTrigger(lua_State* L);
	static int boxTrigger(lua_State* L);


	void ActivateLua();

private :
	static cGameObjectMediator* instance;
	cGameObjectMediator();
};


#endif // !_CGAMEOBJECTMEDIATOR_HG_

