#include "cBoxTrigger.h"

cBoxTrigger::cBoxTrigger(cCommandGroup commandGroup, glm::vec3 center, float halfSize)
{
	this->commandGroup = commandGroup;
	this->halfSize = halfSize;
	this->center = center;
	this->type = "sphere";
}

bool cBoxTrigger::InLocation(glm::vec3 position)
{
	bool inBox = false;
	if (position.x >= (center.x - halfSize) && position.x <= (center.x + halfSize))
		if (position.y >= (center.y - halfSize) && position.y <= (center.y + halfSize))
			if (position.z >= (center.z - halfSize) && position.z <= (center.z + halfSize))
				inBox = true;
	return inBox;
}
