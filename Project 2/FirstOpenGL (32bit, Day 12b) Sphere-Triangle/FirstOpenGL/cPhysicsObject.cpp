#include "cPhysicsObject.h"
#include <vector>

#include "iGameObject.h"

cPhysicsObject::cPhysicsObject()
{
	this->velocity = glm::vec3(0.0f);
	this->acceleration = glm::vec3(0.0f);
}

cPhysicsObject::~cPhysicsObject()
{
}

////update the sphere based on time
//void cPhysicsObject::Update(std::vector<iGameObject*>* g_vecGameObjects, double deltaTime)
//{
//	// New position is based on velocity over time
//	glm::vec3 deltaPosition = (float)deltaTime * velocity;
//	modelData.position += deltaPosition;
//	modelData.oldPosition = modelData.position;
//
//	// New velocity is based on acceleration over time
//	glm::vec3 deltaVelocity = ((float)deltaTime * acceleration)
//		+ ((float)deltaTime * glm::vec3(0.0f, -9.0f, 0.0f));
//
//	velocity += deltaVelocity;
//}
