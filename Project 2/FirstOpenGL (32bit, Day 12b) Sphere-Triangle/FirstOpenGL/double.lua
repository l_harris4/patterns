function runLua()
	--sphereTrigger(5,0,-25,15)
	--orientTimeCommand(5,1,0,180,0)
	startGroup(false)
	startGroup(true)
	
	startGroup(false)
	cameraTargetCommand(3)
	moveSpeedCommand(3,0.2,5,0,-25)
	--moveSpeedEaseInEaseOutCommand(3,0.2,5,0,-25,40,40)
	--moveSpeedCommandDelay(3,0.2,5,0,-25,3)
	--moveInstantCommand(3,5,0,-25)
	--moveInstantCommandDelay(3,5,0,-25,3)

	moveTimeCommand(4,6,5,0,-15)
	moveTimeCommand(5,6,5,0,-5)

	followDistanceCommand(6,5,1,5,1,3,4)
	--followSpeedCommand(6,5,5,1,3,0,0,5,4)
	followSpeedCommand(7,4,5,1,3,-2,0,0,4)
	endGroup()
	
	startGroup(false)
	moveTimeCommand(1,4.5,15,20,0)--id, time, locx,locy,locz
	endGroup()
	
	startGroup(false)
	cameraTargetCommand(1)
	endGroup()
	
	startGroup(false)
	--orientSpeedCommand(4,0.05,0,180,0) --id, time, ending rotation
	orientSpeedEaseInEaseOutCommand(4,0.05,0,180,0,10,10)
	--orientSpeedCommand(4,0.05,0,180,0,3)
	--orientInstantCommand(4,0,180,0)
	--orientInstantCommandDelay(4,0,180,0,3)

	moveTimeCommand(4,5,-60,0,-35)
	
	moveTimeCommand(5,5,-60,0,-17)
	orientTimeCommand(5,1,0,180,0)
	
	moveTimeCommand(6,5,-60,0,-5)
	orientTimeCommand(6,1,0,180,0)

	moveTimeCommand(7,5,-60,0,0)
	orientTimeCommand(7,1,0,180,0)
	
	moveTimeCommand(3,5,-60,0,25)
	orientTimeCommand(3,1,0,180,0)

	orientTimeCommand(8,1,0,0,0) --id, time, ending rotation
	moveTimeCommand(8,5,60,0,-35)
	
	moveTimeCommand(9,5,60,0,-17)
	orientTimeCommand(9,1,0,0,0)
	
	moveTimeCommand(10,5,60,0,-5)
	orientTimeCommand(10,1,0,0,0)

	moveTimeCommand(11,5,60,0,0)
	orientTimeCommand(11,1,0,0,0)
	endGroup()
	
	startGroup(false)
	moveTimeCommand(1,9,15,5,0)--id, time, locx,locy,locz
	orientTimeCommand(1,9,0,180,0)
	endGroup()
	
	
	endGroup()
	startGroup(false)
	--curveSpeedSinglePointCommandDelay(12,2,0,50,-120,220,50,0,4)
	--curveSpeedSinglePointCommand(12,2,0,50,-120,220,50,0)
	--curveSpeedEaseInEaseOutSinglePointCommand(12,2,0,50,-120,220,50,0,30,30)

	--curveSpeedDoublePointCommand(12,2,0,50,-120,220,50,0,-220,60,0)
	--curveSpeedEaseInEaseOutDoublePointCommand(12,1,0,50,-120,220,50,0,-220,60,0,30,30)


	curveTimeSinglePointCommand(12,10,0,50,-120,220,50,0)
	--curveTimeSinglePointCommandDelay(12,10,0,50,-120,220,50,0,4)
	--curveTimeDoublePointCommand(12,10,0,50,-120,220,50,0,220,50,0)
	--curveTimeDoublePointCommandDelay(12,10,0,50,-120,220,50,0,220,50,4)
	endGroup()
	endGroup()
end