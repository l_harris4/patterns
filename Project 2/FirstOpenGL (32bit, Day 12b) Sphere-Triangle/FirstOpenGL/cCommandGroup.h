#ifndef CCOMMANDGROUP_HG
#define CCOMMANDGROUP_HG
#include "cCommand.h"
#include <vector>


class cCommandGroup : public cCommand
{
public:
	cCommandGroup();
	cCommandGroup(float delay);
	~cCommandGroup();
	virtual void Update();
	virtual void Update(float deltaTime);
	std::vector<cCommand*> commands;
	cCommandGroup* parentGroup;
	bool serial = false;

	virtual cCommand* genCopy();
	virtual void setAffectedObject(cGameObject* gameObject);


};

#endif // !CCOMMANDGROUP_HG

#pragma once
