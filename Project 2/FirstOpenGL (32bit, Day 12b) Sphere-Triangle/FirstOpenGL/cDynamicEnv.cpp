#include "cDynamicEnv.h"

cDynamicEnv::cDynamicEnv()
{
	this->editable = true;
}

cDynamicEnv::~cDynamicEnv()
{
}

void cDynamicEnv::Update(std::vector<iGameObject*>* g_vecGameObjects, double deltaTime)
{
	//Stuff needed for lua scripts and moving
	//moving for moveto
	if (moving)
	{
		moveTime -= deltaTime;
		//if the object is fully moved
		if (moveTime <= 0 || glm::distance(moveTarget,this->modelData.position) < 1)
		{
			moving = false;
			velocity = glm::vec3(0.0f);
		}
	}

	//rotating for rotateto
	if (rotating)
	{
		//if the object is fully rotated
		if (glm::distance(rotationTarget,this->modelData.orientation2) <= rotationSpeed)
		{
			rotating = false;
		}
		else
		{
			//continue rotating the object
			//modelData.orientation2.x += 0.1;
			if (modelData.orientation2.x != rotationTarget.x)
			{
				if (modelData.orientation2.x < rotationTarget.x)
					modelData.orientation2.x += rotationSpeed;
				else
					modelData.orientation2.x -= rotationSpeed;
			}
			if (modelData.orientation2.y != rotationTarget.y)
			{
				if (modelData.orientation2.y < rotationTarget.y)
					modelData.orientation2.y += rotationSpeed;
				else
					modelData.orientation2.y -= rotationSpeed;
			}
			if (modelData.orientation2.z != rotationTarget.z)
			{
				if (modelData.orientation2.z < rotationTarget.z)
					modelData.orientation2.z += rotationSpeed;
				else
					modelData.orientation2.z -= rotationSpeed;
			}
			/*modelData.orientation2.x += (rotationTarget.x - modelData.orientation2.x) * rotationTime;
			modelData.orientation2.y += (rotationTarget.y - modelData.orientation2.y)  * rotationTime;
			modelData.orientation2.z += (rotationTarget.z - modelData.orientation2.z) * rotationTime;*/
		}
	}

	//following
	if (following)
	{
		if (glm::distance(modelData.position, followTarget->modelData.position) > followingDistance)
		{
			velocity = glm::normalize(followTarget->modelData.position - this->modelData.position) *
				(glm::distance(followTarget->modelData.position, this->modelData.position) / (followSpeed));
		}
		else
		{
			velocity = glm::vec3(0.0f);
		}

		/*if (glm::distance(modelData.position, followTarget->modelData.position) > followingDistance)
		{
			MoveTo((glm::normalize(followTarget->modelData.position - modelData.position)*followingDistance),followSpeed);
		}*/
	}


	//updating the position based on velocity
	glm::vec3 deltaPosition = (float)deltaTime * velocity;
	modelData.position += deltaPosition;
	modelData.oldPosition = modelData.position;

	////movement behaviour
	//if (this->modelData.position.x > 0)
	//{
	//	velocity.x -= 0.1;
	//}
	//else if (this->modelData.position.x < -20)
	//{
	//	velocity.x += 0.1;
	//}
}

void cDynamicEnv::MoveTo(glm::vec3 target, float time)
{
	cGameObject::MoveTo(target, time);
	velocity = glm::normalize(target - this->modelData.position) *
		(glm::distance(target, this->modelData.position) / (time));
}
