#include "cOrientToCommand.h"
#include "cGameObject.h"
#include <glm\glm.hpp>


cOrientToCommand::cOrientToCommand()
{
}

void cOrientToCommand::fillOutSpeedPoints()
{
	speedPoints.clear();

	glm::vec3 tempOrientation = objectBeingAffected->modelData.orientation2;
	//int tempEaseIn = easeInPoints;
	//int tempEaseOut = easeOutPoints;
	int tempPointNum = 1;
	while (tempOrientation != targetOrientation)
	{
		float tempSpeed = maxTurnSpeed;

		if (tempOrientation.x != targetOrientation.x)
		{
			if (glm::abs(tempOrientation.x - targetOrientation.x) < tempSpeed)
			{
				tempOrientation.x = targetOrientation.x;
			}
			else
			{
				tempOrientation.x += tempSpeed;
			}
		}

		if (tempOrientation.y != targetOrientation.y)
		{
			if (glm::abs(tempOrientation.y - targetOrientation.y) < tempSpeed)
			{
				tempOrientation.y = targetOrientation.y;
			}
			else
			{
				tempOrientation.y += tempSpeed;
			}
		}

		if (tempOrientation.z != targetOrientation.z)
		{
			if (glm::abs(tempOrientation.z - targetOrientation.z) < tempSpeed)
			{
				tempOrientation.z = targetOrientation.z;
			}
			else
			{
				tempOrientation.z += tempSpeed;
			}
		}
		tempPointNum++;
		speedPoints.push_back(tempOrientation);
	}
}

cOrientToCommand::cOrientToCommand(cGameObject * theObject, glm::vec3 targetOrientation, float speed)
{
	this->objectBeingAffected = theObject;
	this->targetOrientation = glm::vec3(glm::radians(targetOrientation.x),
		glm::radians(targetOrientation.y),
		glm::radians(targetOrientation.z));
	this->turnSpeed = speed;
	this->maxTurnSpeed = speed;
	this->type = OrientCommandType::SPEEDOTYPE;
	//this->startingOrientation = objectBeingAffected->modelData.orientation2;
	startingTurnDistance = glm::distance(this->targetOrientation, objectBeingAffected->modelData.orientation2);

	easeInPoints = 10;
	easeOutPoints = 10;

	fillOutSpeedPoints();

}

cOrientToCommand::cOrientToCommand(cGameObject * theObject, float time, glm::vec3 targetOrientation)
{
	this->objectBeingAffected = theObject;
	this->targetOrientation = glm::vec3(glm::radians(targetOrientation.x),
		glm::radians(targetOrientation.y),
		glm::radians(targetOrientation.z));
	this->startingOrientation = theObject->modelData.orientation2;
	this->timeAlotted = time;
	this->timeConsumed = 0.0f;
	this->startingTurnDistance = glm::distance(this->targetOrientation, objectBeingAffected->modelData.orientation2);
	this->type = OrientCommandType::TIMEOTYPE;
	
}

cOrientToCommand::cOrientToCommand(cGameObject * theObject, glm::vec3 targetOrientation)
{
	this->objectBeingAffected = theObject;
	this->targetOrientation = glm::vec3(glm::radians(targetOrientation.x),
		glm::radians(targetOrientation.y),
		glm::radians(targetOrientation.z));
	this->type = OrientCommandType::INSTANTO;
}

cOrientToCommand::cOrientToCommand(cGameObject * theObject, glm::vec3 targetOrientation, float speed, int easeInSteps, int easeOutSteps)
{

	this->objectBeingAffected = theObject;
	this->targetOrientation = glm::vec3(glm::radians(targetOrientation.x),
		glm::radians(targetOrientation.y),
		glm::radians(targetOrientation.z));
	this->turnSpeed = speed;
	this->maxTurnSpeed = speed;
	this->type = OrientCommandType::SPEEDOTYPE;
	startingTurnDistance = glm::distance(this->targetOrientation, objectBeingAffected->modelData.orientation2);

	this->easeInPoints = easeInSteps;
	this->easeOutPoints = easeOutSteps;

	fillOutSpeedPoints();
}


cOrientToCommand::cOrientToCommand(float delay, cGameObject * theObject, glm::vec3 targetOrientation, float speed)
{
	this->startDelay = delay;
	this->delayed = true;
	this->objectBeingAffected = theObject;
	this->targetOrientation = targetOrientation;
	this->turnSpeed = speed;
	this->maxTurnSpeed = speed;
	this->type = OrientCommandType::SPEEDOTYPE;
	startingTurnDistance = glm::distance(this->targetOrientation, objectBeingAffected->modelData.orientation2);
	easeInPoints = 40;
	easeOutPoints = 40;

	fillOutSpeedPoints();
}

cOrientToCommand::cOrientToCommand(float delay, cGameObject * theObject, float time, glm::vec3 targetOrientation)
{
	this->startDelay = delay;
	this->delayed = true;
	this->objectBeingAffected = theObject;
	this->targetOrientation = glm::vec3(glm::radians(targetOrientation.x),
		glm::radians(targetOrientation.y),
		glm::radians(targetOrientation.z));
	this->startingOrientation = theObject->modelData.orientation2;
	this->timeAlotted = time;
	this->timeConsumed = 0.0f;
	this->type = OrientCommandType::TIMEOTYPE;
	this->startingTurnDistance = glm::distance(this->targetOrientation, objectBeingAffected->modelData.orientation2);
}

cOrientToCommand::cOrientToCommand(float delay, cGameObject * theObject, glm::vec3 targetOrientatio)
{
	this->startDelay = delay;
	this->delayed = true;
	this->objectBeingAffected = theObject;
	this->targetOrientation = glm::vec3(glm::radians(targetOrientation.x),
		glm::radians(targetOrientation.y),
		glm::radians(targetOrientation.z));
	this->type = OrientCommandType::INSTANTO;
}

cOrientToCommand::cOrientToCommand(float delay, cGameObject * theObject, glm::vec3 targetOrientation, float speed, int easeInSteps, int easeOutSteps)
{
	this->startDelay = delay;
	this->delayed = true;
	this->objectBeingAffected = theObject;
	this->targetOrientation = glm::vec3(glm::radians(targetOrientation.x),
		glm::radians(targetOrientation.y),
		glm::radians(targetOrientation.z));
	this->turnSpeed = speed;
	this->maxTurnSpeed = speed;
	this->type = OrientCommandType::SPEEDOTYPE;
	startingTurnDistance = glm::distance(this->targetOrientation, objectBeingAffected->modelData.orientation2);

	this->easeInPoints = easeInSteps;
	this->easeOutPoints = easeOutSteps;

	fillOutSpeedPoints();
}

cOrientToCommand::~cOrientToCommand()
{
}

void cOrientToCommand::Update()
{
}

void cOrientToCommand::Update(float deltaTime)
{
	if (Ready(deltaTime)) {
		if (firstUpdate)
		{
			checkIfInLocationTriggerAtStart();
			firstUpdate = false;
			this->startingTurnDistance = glm::distance(this->targetOrientation, objectBeingAffected->modelData.orientation2);
			this->distanceTurnPortion = startingTurnDistance * 0.1;
			this->startingOrientation = objectBeingAffected->modelData.orientation2;
		}

		switch (type)
		{
		case OrientCommandType::SPEEDOTYPE:
		{
			if (!complete)
			{
				if (pointNum == speedPoints.size())
					complete = true;
				else
				{
					float tempSpeed = maxTurnSpeed;// *deltaTime;
					//change the speed based on ease in ease out
					//ease in
					if (pointNum <= easeInPoints)
					{
						tempSpeed *= ((float)pointNum / (float)easeInPoints);
					}
					//ease out
					else if (pointNum >= (speedPoints.size() - easeOutPoints))
					{
						tempSpeed *= 1.0f - ((float)(pointNum - (speedPoints.size() - easeOutPoints)) / (float)(easeOutPoints));
					}
					else
					{
						tempSpeed = maxTurnSpeed;
					}

					objectBeingAffected->modelData.orientation2 += glm::normalize(speedPoints[pointNum] - objectBeingAffected->modelData.orientation2) * tempSpeed;
					objectBeingAffected->overwriteQOrientationFormEuler(objectBeingAffected->modelData.orientation2);
					//objectBeingAffected->modelData.position += glm::normalize(speedPoints[pointNum] - objectBeingAffected->modelData.position) * tempSpeed;
					if (glm::distance(objectBeingAffected->modelData.orientation2, speedPoints[pointNum]) < tempSpeed)
					{
						pointNum++;
					}
				}
				
			}
		}
		break;
		case OrientCommandType::TIMEOTYPE:
		{
			if (timeConsumed > timeAlotted)
				complete = true;
			else {
				timeConsumed += deltaTime;

				float time = smootherstep(0,
					timeAlotted,
					timeConsumed);

				float distance = startingTurnDistance - (startingTurnDistance* time);

				objectBeingAffected->modelData.orientation2 = targetOrientation + (glm::normalize(startingOrientation - targetOrientation) * distance);
				objectBeingAffected->overwriteQOrientationFormEuler(objectBeingAffected->modelData.orientation2);
			}
		}
		break;
		case OrientCommandType::INSTANTO:
		{
			objectBeingAffected->modelData.orientation2 = targetOrientation;
			complete = true;
		}
		break;
		}
	}
}

cCommand * cOrientToCommand::genCopy()
{
	cOrientToCommand* newCommand = new cOrientToCommand();

	newCommand->targetOrientation = this->targetOrientation;
	newCommand->turnSpeed = this->turnSpeed;
	newCommand->maxTurnSpeed = this->maxTurnSpeed;
	newCommand->type = this->type;
	newCommand->targetOrientation = this->targetOrientation;
	newCommand->timeAlotted = this->timeAlotted;
	newCommand->timeConsumed = 0.0f;
	newCommand->startDelay = this->startDelay;
	newCommand->easeInPoints = this->easeInPoints;
	newCommand->easeOutPoints = this->easeOutPoints;
	newCommand->fillOutSpeedPoints();

	return newCommand;
}

void cOrientToCommand::setAffectedObject(cGameObject * gameObject)
{
	this->objectBeingAffected = gameObject;
}
