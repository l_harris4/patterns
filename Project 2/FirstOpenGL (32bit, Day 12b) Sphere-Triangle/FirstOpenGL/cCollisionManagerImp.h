#ifndef _CCOLLISIONMANAGERIMP_HG_
#define _CCOLLISIONMANAGERIMP_HG_
#include <vector>

class cCollision;

class cCollisionManagerImp
{
public:
	//ctor and dtor
	cCollisionManagerImp();
	~cCollisionManagerImp();
	//add a collision to the vector of collisions
	void AddCollision(cCollision collisionData);
	//clear the vector of collisions
	void ClearCollisions();
	//compute all the collisions
	void CalculateCollisions();
	//vector to keep track of all the collisions
	std::vector<cCollision> collisions;

	//add an extra method that does nothing for demonstration purposes
	void DoStuff();

};
#endif // !_CCOLLISIONMANAGERIMP_HG_
