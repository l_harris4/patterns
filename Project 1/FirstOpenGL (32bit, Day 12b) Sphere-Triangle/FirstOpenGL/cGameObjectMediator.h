#ifndef _CGAMEOBJECTMEDIATOR_HG_
#define _CGAMEOBJECTMEDIATOR_HG_
#include <vector>
#include <glm\vec3.hpp>

//forward declarations of classes
class cSphere;
class cPhysTriangle;
class iGameObject;

class cGameObjectMediator
{
public:
	//the vector of all game objects
	std::vector< iGameObject* >  vecGameObjects;
	//ctor and dtor
	cGameObjectMediator();
	~cGameObjectMediator();

	//used for moving one sphere
	void MoveMainSphere(glm::vec3 force);
	//used for updating all the game objects
	void UpdateGameObjects(float deltaTime);
	//used for getting all the colliding triangles for a specific object
	bool GetCollidingTriangles(iGameObject* piSphere, iGameObject* piTriangleObject, std::vector<cPhysTriangle>& closestTriangles);
	//computing all the interactions for a sphere
	void ComputeInteractions(cSphere* sphere);
};


#endif // !_CGAMEOBJECTMEDIATOR_HG_

