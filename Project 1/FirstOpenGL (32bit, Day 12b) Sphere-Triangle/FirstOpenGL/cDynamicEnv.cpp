#include "cDynamicEnv.h"

cDynamicEnv::cDynamicEnv()
{
	this->editable = true;
}

cDynamicEnv::~cDynamicEnv()
{
}

void cDynamicEnv::Update(std::vector<iGameObject*>* g_vecGameObjects, double deltaTime)
{
	//updating the position based on velocity
	glm::vec3 deltaPosition = (float)deltaTime * velocity;
	modelData.position += deltaPosition;
	modelData.oldPosition = modelData.position;

	//movement behaviour
	if (this->modelData.position.x > 0)
	{
		velocity.x -= 0.1;
	}
	else if (this->modelData.position.x < -20)
	{
		velocity.x += 0.1;
	}
}
