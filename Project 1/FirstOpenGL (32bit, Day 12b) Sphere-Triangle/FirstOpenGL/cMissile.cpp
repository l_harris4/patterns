#include "cMissile.h"
#include <glm\vec3.hpp>

cMissile::cMissile()
{
}

cMissile::~cMissile()
{
}

void cMissile::Fire()
{
	fired = true;
	this->velocity = glm::vec3(0.0f, 7.0f, 0.0f);
}

void cMissile::Update(std::vector<iGameObject*>* g_vecGameObjects, double deltaTime)
{
	if (fired)
	{
		glm::vec3 deltaPosition = (float)deltaTime * velocity;
		modelData.position += deltaPosition;
		modelData.oldPosition = modelData.position;
	}
}
