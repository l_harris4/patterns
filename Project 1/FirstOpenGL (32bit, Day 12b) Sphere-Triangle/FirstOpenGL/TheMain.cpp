#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>			// C++ cin, cout, etc.
#include <glm/vec3.hpp> // glm::vec3
#include <glm/vec4.hpp> // glm::vec4
#include <glm/mat4x4.hpp> // glm::mat4
#include <glm/gtc/matrix_transform.hpp> // glm::translate, glm::rotate, glm::scale, glm::perspective
#include <glm/gtc/type_ptr.hpp> // glm::value_ptr
#include <stdlib.h>
#include <stdio.h>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include "Utilities.h"
#include "ModelUtilities.h"
#include "cMesh.h"
#include "cShaderManager.h" 
#include "cVAOMeshManager.h"
#include "cCollisionManager.h"
#include "cCollision.h"
#include "cGameObjectFactory.h"
#include "cModelData.h"
#include "cGameObjectMediator.h"
#include "Physics.h"
#include "cLightManager.h"

// Forward declaration of functions
void DrawObject(iGameObject* pTheGO);
void PhysicsStep(double deltaTime);
bool LoadModelsLightsFromFile();
bool Load3DModelsIntoMeshManager(int shaderID, cVAOMeshManager* pVAOManager, std::string &error);


glm::vec3 g_cameraXYZOrig = glm::vec3(0.0f, 51.0f, 122.0f);	// 5 units "down" z
glm::vec3 g_cameraTarget_XYZ = glm::vec3(0.0f, 0.0f, 0.0f);
bool g_cameraOnBall = true;

cVAOMeshManager* g_pVAOManager = 0;	
cShaderManager*		g_pShaderManager;
cLightManager*		g_pLightManager;
cCollisionManager*  g_pCollisionManager;
cGameObjectMediator* g_pGameObjectMediator;

bool g_bDrawDebugLightSpheres = false;

// Other uniforms:
GLint uniLoc_materialDiffuse = -1;
GLint uniLoc_materialAmbient = -1;
GLint uniLoc_ambientToDiffuseRatio = -1; 	// Maybe	// 0.2 or 0.3
GLint uniLoc_materialSpecular = -1;  // rgb = colour of HIGHLIGHT only
							// w = shininess of the 
GLint uniLoc_bIsDebugWireFrameObject = -1;

GLint uniLoc_eyePosition = -1;	// Camera position
GLint uniLoc_mModel = -1;
GLint uniLoc_mView = -1;
GLint uniLoc_mProjection = -1;


static void error_callback(int error, const char* description)
{
	fprintf(stderr, "Error: %s\n", description);
}

bool bIsShiftPressed(int mods);
bool bIsCtrlPressed(int mods);
bool bIsAltPressed(int mods);
bool bIsShiftPressedAlone(int mods);
bool bIsCtrlPressedAlone(int mods);
bool bIsAltPressedAlone(int mods);

static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GLFW_TRUE);

	if (key == GLFW_KEY_SPACE)
	{
	}

	const float CAMERASPEED = 0.25f;
	switch (key)
	{
	case GLFW_KEY_A:		// Left
		if (action == GLFW_REPEAT || action == GLFW_PRESS)
			::g_pGameObjectMediator->MoveMainSphere(glm::vec3(-1.0, 0, 0));
		break;
	case GLFW_KEY_D:		// Right
		if (action == GLFW_REPEAT || action == GLFW_PRESS)
			::g_pGameObjectMediator->MoveMainSphere(glm::vec3(+1.0, 0, 0));
		break;
	case GLFW_KEY_W:		// Forward
		if (action == GLFW_REPEAT || action == GLFW_PRESS)
			::g_pGameObjectMediator->MoveMainSphere(glm::vec3(0, 0, -1.0));
		break;
	case GLFW_KEY_S:		// Backwards (along z)
		if (action == GLFW_REPEAT || action == GLFW_PRESS)
			::g_pGameObjectMediator->MoveMainSphere(glm::vec3(0, 0, +1.0));
		break;
	case GLFW_KEY_SPACE:
		if (action == GLFW_PRESS) {
			::g_pGameObjectMediator->MoveMainSphere(glm::vec3(0, 6, 0));
		}
		break;

	}// switch ( key )

	return;
}


int main(void)
{

	GLFWwindow* window;
	GLint mvp_location;	// , vpos_location, vcol_location;
	glfwSetErrorCallback(error_callback);

	if (!glfwInit())
		exit(EXIT_FAILURE);

	int height = 480;	/* default */
	int width = 640;	// default
	std::string title = "OpenGL Rocks";

	std::ifstream infoFile("config.txt");
	if (!infoFile.is_open())
	{	// File didn't open...
		std::cout << "Can't find config file" << std::endl;
		std::cout << "Using defaults" << std::endl;
	}
	else
	{	// File DID open, so read it... 
		std::string a;

		infoFile >> a;	// "Game"	//std::cin >> a;
		infoFile >> a;	// "Config"
		infoFile >> a;	// "width"

		infoFile >> width;	// 1080

		infoFile >> a;	// "height"

		infoFile >> height;	// 768

		infoFile >> a;		// Title_Start

		std::stringstream ssTitle;		// Inside "sstream"
		bool bKeepReading = true;
		do
		{
			infoFile >> a;		// Title_End??
			if (a != "Title_End")
			{
				ssTitle << a << " ";
			}
			else
			{	// it IS the end! 
				bKeepReading = false;
				title = ssTitle.str();
			}
		} while (bKeepReading);


	}//if ( ! infoFile.is_open() )




	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);

	window = glfwCreateWindow(width, height,
		title.c_str(),
		NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	glfwSetKeyCallback(window, key_callback);
	glfwMakeContextCurrent(window);
	gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);
	glfwSwapInterval(1);

	std::cout << glGetString(GL_VENDOR) << " "
		<< glGetString(GL_RENDERER) << ", "
		<< glGetString(GL_VERSION) << std::endl;
	std::cout << "Shader language version: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;


	::g_pShaderManager = new cShaderManager();

	cShaderManager::cShader vertShader;
	cShaderManager::cShader fragShader;

	vertShader.fileName = "simpleVert.glsl";
	fragShader.fileName = "simpleFrag.glsl";

	::g_pShaderManager->setBasePath("assets//shaders//");

	// Shader objects are passed by reference so that
	//	we can look at the results if we wanted to. 
	if (!::g_pShaderManager->createProgramFromFile(
		"mySexyShader", vertShader, fragShader))
	{
		std::cout << "Error with shader" << std::endl;
		std::cout << ::g_pShaderManager->getLastError() << std::endl;
		// Should we exit?? 
		return -1;
		//		exit(
	}
	std::cout << "The shaders comipled and linked OK" << std::endl;

	::g_pVAOManager = new cVAOMeshManager();

	GLint sexyShaderID = ::g_pShaderManager->getIDFromFriendlyName("mySexyShader");

	std::string error;
	if (!Load3DModelsIntoMeshManager(sexyShaderID, ::g_pVAOManager, error))
	{
		std::cout << "Not all models were loaded..." << std::endl;
		std::cout << error << std::endl;
	}


	GLint currentProgID = ::g_pShaderManager->getIDFromFriendlyName("mySexyShader");

	// Get the uniform locations for this shader
	mvp_location = glGetUniformLocation(currentProgID, "MVP");		// program, "MVP");
	uniLoc_materialDiffuse = glGetUniformLocation(currentProgID, "materialDiffuse");
	uniLoc_materialAmbient = glGetUniformLocation(currentProgID, "materialAmbient");
	uniLoc_ambientToDiffuseRatio = glGetUniformLocation(currentProgID, "ambientToDiffuseRatio");
	uniLoc_materialSpecular = glGetUniformLocation(currentProgID, "materialSpecular");

	uniLoc_bIsDebugWireFrameObject = glGetUniformLocation(currentProgID, "bIsDebugWireFrameObject");

	uniLoc_eyePosition = glGetUniformLocation(currentProgID, "eyePosition");

	uniLoc_mModel = glGetUniformLocation(currentProgID, "mModel");
	uniLoc_mView = glGetUniformLocation(currentProgID, "mView");
	uniLoc_mProjection = glGetUniformLocation(currentProgID, "mProjection");

	::g_pCollisionManager = new cCollisionManager();

	::g_pLightManager = new cLightManager();

	::g_pGameObjectMediator = new cGameObjectMediator();

	//Load the lights and models from a file
	LoadModelsLightsFromFile();


	::g_pLightManager->LoadShaderUniformLocations(currentProgID);

	::g_pLightManager->vecLights[0].attenuation.y = 0.1f;
	::g_pLightManager->vecLights[0].attenuation.z = 0.0f;



	glEnable(GL_DEPTH);

	// Gets the "current" time "tick" or "step"
	double lastTimeStep = glfwGetTime();

	// Main game or application loop
	while (!glfwWindowShouldClose(window))
	{
		float ratio;
		int width, height;
		glm::mat4x4 p, mvp;

		glfwGetFramebufferSize(window, &width, &height);
		ratio = width / (float)height;
		glViewport(0, 0, width, height);

		// Clear colour AND depth buffer
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		::g_pShaderManager->useShaderProgram("mySexyShader");
		GLint shaderID = ::g_pShaderManager->getIDFromFriendlyName("mySexyShader");

		// Update all the light uniforms...
		// (for the whole scene)
		::g_pLightManager->CopyLightInformationToCurrentShader();

		// Projection and view don't change per scene (maybe)
		p = glm::perspective(0.6f,			// FOV
			ratio,		// Aspect ratio
			0.1f,			// Near (as big as possible)
			1000.0f);	// Far (as small as possible)

		// View or "camera" matrix
		glm::mat4 v = glm::mat4(1.0f);	// identity

		if (::g_cameraOnBall)
		{
			//glm::vec3 curPostion = ::g_pGameObjectMediator->vecGameObjects[3]->GetModelData()->position;
			//v = glm::lookAt(glm::vec3(curPostion.x, curPostion.y + 10, curPostion.z + 30),						// "eye" or "camera" position
			//	curPostion,		// "At" or "target" 
			//	glm::vec3(0.0f, 1.0f, 0.0f));	// "up" vector
			glm::vec3 curPostion = ::g_pGameObjectMediator->vecGameObjects[3]->GetModelData()->position;
			v = glm::lookAt(glm::vec3(curPostion.x, curPostion.y + 30, curPostion.z + 50),						// "eye" or "camera" position
				curPostion,		// "At" or "target" 
				glm::vec3(0.0f, 1.0f, 0.0f));	// "up" vector
		}
		else
		{
			v = glm::lookAt(g_cameraXYZOrig,						// "eye" or "camera" position
				g_cameraTarget_XYZ,		// "At" or "target" 
				glm::vec3(0.0f, 1.0f, 0.0f));	// "up" vector
		}

		glUniformMatrix4fv(uniLoc_mView, 1, GL_FALSE,
			(const GLfloat*)glm::value_ptr(v));
		glUniformMatrix4fv(uniLoc_mProjection, 1, GL_FALSE,
			(const GLfloat*)glm::value_ptr(p));


		// Draw the scene
		unsigned int sizeOfVector = ::g_pGameObjectMediator->vecGameObjects.size();	//*****//
		for (int index = 0; index != sizeOfVector; index++)
		{
			iGameObject* pTheGO = ::g_pGameObjectMediator->vecGameObjects[index];

			DrawObject(pTheGO);

		}//for ( int index = 0...

		// Now many seconds that have elapsed since we last checked
		double curTime = glfwGetTime();
		double deltaTime = curTime - lastTimeStep;

		PhysicsStep(deltaTime);

		lastTimeStep = curTime;

		// Done once per scene 
		glfwSwapBuffers(window);
		glfwPollEvents();
	}// while ( ! glfwWindowShouldClose(window) )


	glfwDestroyWindow(window);
	glfwTerminate();

	// 
	delete ::g_pShaderManager;
	delete ::g_pVAOManager;
	delete ::g_pLightManager;
	delete ::g_pCollisionManager;
	delete ::g_pGameObjectMediator;
	return 0;
}

// Update the world 1 "step" in time
void PhysicsStep(double deltaTime)
{
	const glm::vec3 GRAVITY = glm::vec3(0.0f, -9.0f, 0.0f);

	::g_pCollisionManager->ClearCollisions();
	::g_pGameObjectMediator->UpdateGameObjects(deltaTime);

	::g_pCollisionManager->CalculateCollisions();

	return;
}

// Draw a single object
void DrawObject(iGameObject* pTheGO)
{
	// Is there a game object? 
	if (pTheGO == 0)
	{	// Nothing to draw
		return;		// Skip all for loop code and go to next
	}

	// Was near the draw call, but we need the mesh name
	std::string meshToDraw = pTheGO->GetModelData()->meshName;

	sVAOInfo VAODrawInfo;
	if (::g_pVAOManager->lookupVAOFromName(meshToDraw, VAODrawInfo) == false)
	{	// Didn't find mesh
		return;
	}

	// There IS something to draw
	glm::mat4x4 mModel = glm::mat4x4(1.0f);

	glm::mat4 trans = glm::mat4x4(1.0f);
	trans = glm::translate(trans,
		pTheGO->GetModelData()->position);
	mModel = mModel * trans;

	glm::mat4 matPostRotZ = glm::mat4x4(1.0f);
	matPostRotZ = glm::rotate(matPostRotZ, pTheGO->GetModelData()->orientation2.z,
		glm::vec3(0.0f, 0.0f, 1.0f));
	mModel = mModel * matPostRotZ;

	glm::mat4 matPostRotY = glm::mat4x4(1.0f);
	matPostRotY = glm::rotate(matPostRotY, pTheGO->GetModelData()->orientation2.y,
		glm::vec3(0.0f, 1.0f, 0.0f));
	mModel = mModel * matPostRotY;

	glm::mat4 matPostRotX = glm::mat4x4(1.0f);
	matPostRotX = glm::rotate(matPostRotX, pTheGO->GetModelData()->orientation2.x,
		glm::vec3(1.0f, 0.0f, 0.0f));
	mModel = mModel * matPostRotX;

	float finalScale = pTheGO->GetModelData()->scale;

	glm::mat4 matScale = glm::mat4x4(1.0f);
	matScale = glm::scale(matScale,
		glm::vec3(finalScale,
			finalScale,
			finalScale));
	mModel = mModel * matScale;

	cModelData* tempData = pTheGO->GetModelData();
	tempData->worldMatrix = mModel;

	glUniformMatrix4fv(uniLoc_mModel, 1, GL_FALSE,
		(const GLfloat*)glm::value_ptr(mModel));

	glm::mat4 mWorldInTranpose = glm::inverse(glm::transpose(mModel));
	//pTheGO->mModel = mWorldInTranpose;

	glm::vec4 colour = pTheGO->GetModelData()->diffuseColour;

	glUniform4f(uniLoc_materialDiffuse,
		colour.r,
		colour.g,
		colour.b,
		colour.a);
	//...and all the other object material colours

	if (pTheGO->GetModelData()->bIsWireFrame)
	{
		glUniform1f(uniLoc_bIsDebugWireFrameObject, 1.0f);	// TRUE
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);	// Default											//		glEnable(GL_DEPTH_TEST);		// Test for z and store in z buffer
		glDisable(GL_CULL_FACE);
	}
	else
	{
		glUniform1f(uniLoc_bIsDebugWireFrameObject, 0.0f);	// FALSE
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);	// Default
		glEnable(GL_DEPTH_TEST);		// Test for z and store in z buffer
		glEnable(GL_CULL_FACE);
	}

	glCullFace(GL_BACK);


	glBindVertexArray(VAODrawInfo.VAO_ID);

	glDrawElements(GL_TRIANGLES,
		VAODrawInfo.numberOfIndices,		// testMesh.numberOfTriangles * 3,	// How many vertex indices
		GL_UNSIGNED_INT,					// 32 bit int 
		0);
	// Unbind that VAO
	glBindVertexArray(0);

	return;
}


bool bIsShiftPressed(int mods)
{
	if (mods & GLFW_MOD_SHIFT)
	{
		return true;
	}
	return false;
}

bool bIsCtrlPressed(int mods)
{
	if (mods & GLFW_MOD_CONTROL)
	{
		return true;
	}
	return false;
}

bool bIsAltPressed(int mods)
{
	if (mods & GLFW_MOD_ALT)
	{
		return true;
	}
	return false;
}


bool bIsShiftPressedAlone(int mods)
{
	if ((mods & GLFW_MOD_SHIFT) == GLFW_MOD_SHIFT)
	{
		return true;
	}
	return false;
}

bool bIsCtrlPressedAlone(int mods)
{
	if ((mods & GLFW_MOD_CONTROL) == GLFW_MOD_CONTROL)
	{
		return true;
	}
	return false;
}

bool bIsAltPressedAlone(int mods)
{
	if ((mods & GLFW_MOD_ALT) == GLFW_MOD_ALT)
	{
		return true;
	}
	return false;
}