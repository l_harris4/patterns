#include "cGameObjectMediator.h"
#include "cSphere.h"
#include "cVAOMeshManager.h"
#include "cCollisionManager.h"
#include <glm\vec4.hpp>
#include "cCollision.h"
#include "Physics.h"
#include <iostream>

extern cVAOMeshManager* g_pVAOManager;
extern cCollisionManager*  g_pCollisionManager;

//ctor
cGameObjectMediator::cGameObjectMediator()
{
}

//dtor
cGameObjectMediator::~cGameObjectMediator()
{
	//delete all the game objects
	for (int i = 0; i < vecGameObjects.size(); i++)
	{
		delete vecGameObjects[i];
	}
}

//move the main sphere
void cGameObjectMediator::MoveMainSphere(glm::vec3 force)
{
	((cSphere*)vecGameObjects[3])->velocity += force;
}

//update all the game objects
void cGameObjectMediator::UpdateGameObjects(float deltaTime)
{
	int totalGameObjects = vecGameObjects.size();
	for (int index = 0; index != totalGameObjects; index++)
	{
		iGameObject* pCurGO = vecGameObjects[index];

		pCurGO->Update(&vecGameObjects, deltaTime);

	}//for ( int index...
}

//find all colliding triangles for a specific sphere
bool cGameObjectMediator::GetCollidingTriangles(iGameObject* piSphere, iGameObject* piTriangleObject, std::vector<cPhysTriangle>& closestTriangles)
{
	cGameObject* pTriangleObject = (cGameObject*)piTriangleObject;
	cSphere* pSphere = (cSphere*)piSphere;

	sVAOInfo VAODrawInfo;
	::g_pVAOManager->lookupVAOFromName(pTriangleObject->modelData.meshName, VAODrawInfo);
	int numberOfTriangles = VAODrawInfo.vecPhysTris.size();

	int activeIndex = -1;
	for (int triIndex = 0; triIndex != numberOfTriangles; triIndex++)
	{
		cPhysTriangle curTriangle = VAODrawInfo.vecPhysTris[triIndex];
		//////////////////////////////////
		//Below are the steps needed if the game object can be moved, not just a static model
		//at position 0
		if (pTriangleObject->editable) {
			glm::vec4 tempVec1(curTriangle.vertex[0].x, curTriangle.vertex[0].y, curTriangle.vertex[0].z, 1);
			tempVec1 = pTriangleObject->modelData.worldMatrix * tempVec1;

			glm::vec4 tempVec2(curTriangle.vertex[1].x, curTriangle.vertex[1].y, curTriangle.vertex[1].z, 1);
			tempVec2 = pTriangleObject->modelData.worldMatrix * tempVec2;

			glm::vec4 tempVec3(curTriangle.vertex[2].x, curTriangle.vertex[2].y, curTriangle.vertex[2].z, 1);
			tempVec3 = pTriangleObject->modelData.worldMatrix * tempVec3;

			curTriangle.vertex[0] = glm::vec3(tempVec1.x, tempVec1.y, tempVec1.z);
			curTriangle.vertex[1] = glm::vec3(tempVec2.x, tempVec2.y, tempVec2.z);
			curTriangle.vertex[2] = glm::vec3(tempVec3.x, tempVec3.y, tempVec3.z);
		}
		////////////////////////////////////////////////////////////////////////

		//get the closest point on a triangle
		glm::vec3 theClosestPoint = curTriangle.ClosestPtPointTriangle(pSphere->GetModelData()->position);
		//get the distance to that point
		float thisDistance = glm::distance(pSphere->GetModelData()->position, theClosestPoint);
		//if the distance is less than the sphere radius, then a collision happened
		if (thisDistance < pSphere->radius)
		{
			closestTriangles.push_back(curTriangle);
		}

	}

	if (closestTriangles.size() > 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

//compute all the interactions for a sphere
void cGameObjectMediator::ComputeInteractions(cSphere * sphere)
{
	//reset some attributes
	sphere->collidedThisFrame = false;
	sphere->hittingGround = false;

	//create a new collision
	cCollision collision;
	collision.mainObject = sphere;

	//rotate the sphere
	sphere->modelData.orientation2.x -= 0.03;


	int size = vecGameObjects.size();

	//go through all the other game objects
	for (int indexEO = 0; indexEO != size; indexEO++)
	{
		// Don't test for myself
		if (sphere == vecGameObjects[indexEO])
			continue;

		iGameObject* pOtherObject = vecGameObjects[indexEO];

		//if the other object is sphere
		if ((pOtherObject->GetType() == "sphere" || pOtherObject->GetType() == "grower") && !sphere->hittingSphere) {
			//if the spheres are penetrating 
			if (PenetrationTestSphereSphere(sphere, pOtherObject))
			{
				//push the other object into the collision object
				collision.otherGameObjects.push_back(pOtherObject);
				sphere->collidedThisFrame = true;
				cSphere* pSphereOther = (cSphere*)pOtherObject;

				pSphereOther->TakeDamage();
				if (pSphereOther->GetType() == "grower")
				{
					sphere->ChangeScale(sphere->modelData.scale * 1.7);
					sphere->ChangeRadius(sphere->radius * 1.7);
					sphere->Respawn();
				}
			}
		}
		else if (pOtherObject->GetType() == "cube" || pOtherObject->GetType() == "plane") {

			float distance = 99999; //value way too big for our simulation

			//will have to find if multiple triangles were in range
			std::vector<cPhysTriangle> closestTriangles;
			if (GetCollidingTriangles(sphere, pOtherObject, closestTriangles))
			{
				int trianglesSize = closestTriangles.size();
				//if there was a multiple triangle collision say so
				if (trianglesSize > 1)
				{
					std::cout << "Multi-Triangle Collision" << std::endl;
				}
				//push the triangle into the collision
				for (int i = 0; i < trianglesSize; i++)
				{
					std::pair<cPhysTriangle, iGameObject*> thePair;
					thePair = std::make_pair(closestTriangles[i], pOtherObject);
					collision.triangles.push_back(thePair);
				}
				sphere->hittingGround = true;
				
			}
		}

	}


	sphere->hittingSphere = sphere->collidedThisFrame;

	::g_pCollisionManager->AddCollision(collision);

}
