#ifndef _cCOLLISION_HG_
#define _cCOLLISION_HG_
#include <vector>
#include <glm\vec3.hpp>
#include <utility>
//forward declarations of needed classes
class cPhysTriangle;
class iGameObject;


class cCollision
{
public:
	//ctor and dtor
	cCollision();
	~cCollision();

	//the object that the collision will be computr for
	iGameObject* mainObject;
	//keeping track of the triangles and objects involved
	std::vector<std::pair<cPhysTriangle,iGameObject*>> triangles;
	std::vector<iGameObject*> otherGameObjects;

	//method to actually calculate the collision
	void calcCollision();
};


#endif // !_cCOLLISION_HG_

