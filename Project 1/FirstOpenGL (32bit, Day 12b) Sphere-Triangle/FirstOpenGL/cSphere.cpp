#include "cSphere.h"
#include "cTriangle.h"
#include "cVAOMeshManager.h"
#include "cCollisionManager.h"
#include "Utilities.h"	
#include "cCollision.h"
#include "Physics.h"
#include <iostream>
#include "cGameObjectMediator.h"

extern cGameObjectMediator* g_pGameObjectMediator;

//ctor
cSphere::cSphere()
{
	//set the default values
	this->radius = 1.0f;
	this->collidedThisFrame = false;
	this->editable = true;
	this->health = 3;
	this->hittingSphere = false;
	this->hittingGround = false;

}

//dtor
cSphere::~cSphere()
{
}

//update the sphere based on time
void cSphere::Update(std::vector<iGameObject*>* g_vecGameObjects, double deltaTime)
{
	//store the reset position
	this->resetPosition = modelData.position;

	// New position is based on velocity over time
	glm::vec3 deltaPosition = (float)deltaTime * velocity;
	modelData.position += deltaPosition;
	modelData.oldPosition = modelData.position;

	// New velocity is based on acceleration over time
	glm::vec3 deltaVelocity = ((float)deltaTime * acceleration)
		+ ((float)deltaTime * glm::vec3(0.0f, -9.0f, 0.0f));

	velocity += deltaVelocity;
	holderVelocity = velocity;


	//if the ball is out of bounds, put it back in
	if (modelData.position.y < -30)
	{
		Respawn();
	}

	//tell the mediator to compute all the interations
	g_pGameObjectMediator->ComputeInteractions(this);

}

//change a sphere colour
void cSphere::ChangeColour(glm::vec4 colour)
{
	this->modelData.diffuseColour = colour;
	this->modelData.origColour = colour;
}

//reduce health of the sphere and change the colour
void cSphere::TakeDamage()
{
	this->health -= 1;

	if (health <= 0)
	{
		Respawn();
		//health = 3;
		ChangeColour(glm::vec4(0.0f, 0.0f, 1.0f, 1.0f));
	}
	else if (health == 2)
	{
		ChangeColour(glm::vec4(1.0f, 1.0f, 0.0f, 1.0f));
	}
	else if (health == 1)
	{
		ChangeColour(glm::vec4(1.0f, 0.0f, 0.0f, 1.0f));
	}
}

//reset the health and put the sphere back in bounds
void cSphere::Respawn()
{
	health = 3;
	glm::vec3 newPos = glm::vec3(getRandInRange(-20.0f, 10.0f),
		getRandInRange(4.0f, 10.0f),
		getRandInRange(0.0f, -40.0f));
	modelData.position = newPos;
	glm::vec3 newVel = glm::vec3(getRandInRange(-1.0f, 1.0f),
		getRandInRange(-1.0f, 1.0f),
		0);
	velocity = newVel;

	ChangeColour(glm::vec4(0.0f, 0.0f, 1.0f, 1.0f));
}

//change the radius
void cSphere::ChangeRadius(float newRadius)
{
	this->radius = newRadius;
}

//change the scale
void cSphere::ChangeScale(float newScale)
{
	this->modelData.scale = newScale;
}
