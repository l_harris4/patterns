#include "cGameObjectFactory.h"
#include "cGameObject.h"
#include "cSphere.h"
#include "cDynamicEnv.h"
#include "cStaticEnv.h"
#include "cPhysicsObject.h"
#include "cGameObjectMediator.h"
#include <glm/glm.hpp>

//example classes
#include "cGrowerSphere.h"
#include "cSilo.h"
#include "cMissile.h"

extern cGameObjectMediator* g_pGameObjectMediator;

cGameObjectFactory* cGameObjectFactory::instance = nullptr;

iGameObject * cGameObjectFactory::CreateGameObject(std::string objectType)
{
	iGameObject* pTheObject = NULL;

	//creating different types of objects based on type
	if (objectType == "sphere")
	{
		pTheObject = new cSphere();
		pTheObject->SetType(objectType);
	}
	else if (objectType == "cube")
	{
		pTheObject = new cDynamicEnv();
		pTheObject->SetType(objectType);
	}
	else if (objectType == "plane")
	{
		pTheObject = new cStaticEnv();
		pTheObject->SetType(objectType);
	}
	else if (objectType == "grower")
	{
		pTheObject = new cGrowerSphere();
		pTheObject->SetType(objectType);
	}
	else if (objectType == "silo")
	{
		pTheObject = new cSilo();
		pTheObject->SetType(objectType);
	}
	else if (objectType == "missile")
	{
		pTheObject = new cMissile();
		pTheObject->SetType(objectType);
	}

	return pTheObject;
}

//assemble game object //used for builder pattern
void cGameObjectFactory::AssembleGameObject(iGameObject * pTheGameObject, std::string objectType, std::vector<std::string> arugments)
{
	//go through all the arguments and change the object based on that
	for (int stringIndex = 0; stringIndex < arugments.size(); stringIndex++)
	{
		std::string line = arugments[stringIndex];
		//set the filename
		if (line.find("filename:") != std::string::npos && pTheGameObject->GetType() != "silo") {
			line.replace(0, 10, "");
			((cGameObject*)pTheGameObject)->modelData.meshName = line;
			continue;
		}
		//set the filename or missile name based on if its silo
		if (line.find("filename:") != std::string::npos && pTheGameObject->GetType() == "silo") {
			cSilo* silo = (cSilo*)pTheGameObject;
			line.replace(0, 10, "");
			if (silo->modelData.meshName == "") {	
				((cGameObject*)pTheGameObject)->modelData.meshName = line;
			}
			else
			{
				((cSilo*)pTheGameObject)->missilePlyName = line;
			}
			continue;
		}
		//set if the object is wireframe or not
		if (line.find("wireframe:") != std::string::npos) {
			line.replace(0, 11, "");
			((cGameObject*)pTheGameObject)->modelData.bIsWireFrame = (line == "true");
			continue;
		}
		//set the velocity
		if (line.find("velocity:") != std::string::npos) {
			line.replace(0, 10, "");

			std::string number;
			bool xValue = true;
			glm::vec3 velocity = glm::vec3(0, 0, 0);
			for (int stringIndex = 0; stringIndex < line.size(); stringIndex++)
			{
				if (line[stringIndex] != ',')
				{
					number += line[stringIndex];
				}
				else if (xValue)
				{
					xValue = false;
					velocity.x = (stof(number));
					number = "";
				}
				else
				{
					velocity.y = (stof(number));
					number = "";
				}
			}
			velocity.z = (stof(number));

			if (pTheGameObject->GetType() == "sphere")
			{
				((cSphere*)pTheGameObject)->velocity = velocity;
			}
			else if (pTheGameObject->GetType() == "cube")
			{
				((cDynamicEnv*)pTheGameObject)->velocity = velocity;
			}
			continue;
		}
		//set the position
		if (line.find("position: ") != std::string::npos) {
			line.replace(0, 10, "");

			std::string number;
			bool xValue = true;
			glm::vec3 position = glm::vec3(0, 0, 0);
			for (int stringIndex = 0; stringIndex < line.size(); stringIndex++)
			{
				if (line[stringIndex] != ',')
				{
					number += line[stringIndex];
				}
				else if (xValue)
				{
					xValue = false;
					position.x = stof(number);
					number = "";
				}
				else
				{
					position.y = stof(number);
					number = "";
				}
			}
			position.z = stof(number);
			((cGameObject*)pTheGameObject)->modelData.position = position;
			continue;
		}
		//set the orientation
		if (line.find("orientation: ") != std::string::npos) {
			line.replace(0, 13, "");

			std::string number;
			bool xValue = true;
			glm::vec3 orientation = glm::vec3(0, 0, 0);
			for (int stringIndex = 0; stringIndex < line.size(); stringIndex++)
			{
				if (line[stringIndex] != ',')
				{
					number += line[stringIndex];
				}
				else if (xValue)
				{
					xValue = false;
					orientation.x = glm::radians(stof(number));
					number = "";
				}
				else
				{
					orientation.y = glm::radians(stof(number));
					number = "";
				}
			}
			orientation.z = glm::radians(stof(number));
			((cGameObject*)pTheGameObject)->modelData.orientation2 = orientation;
			continue;
		}
		//set the scale
		if (line.find("scale:") != std::string::npos) {
			line.replace(0, 7, "");
			((cGameObject*)pTheGameObject)->modelData.scale = stof(line);
			continue;
		}
		//set the diffuse colour
		if (line.find("colour:") != std::string::npos) {
			line.replace(0, 8, "");

			std::string number;
			bool rValue = true;
			glm::vec4 colour = glm::vec4(0, 0, 0, 1);
			for (int stringIndex = 0; stringIndex < line.size(); stringIndex++)
			{
				if (line[stringIndex] != ',')
				{
					number += line[stringIndex];
				}
				else if (rValue)
				{
					rValue = false;
					colour.x = stof(number);
					number = "";
				}
				else
				{
					colour.y = stof(number);
					number = "";
				}
			}
			colour.z = stof(number);
			((cGameObject*)pTheGameObject)->modelData.diffuseColour = colour;
			((cGameObject*)pTheGameObject)->modelData.origColour = colour;
			continue;
		}
	}

	//if it is a silo, give it missiles
	if (objectType == "silo")
	{
		cSilo* object = (cSilo*)pTheGameObject;
		for (int i = 0; i < object->maxMissiles; i++)
		{
			//assemble the missile
			cMissile* tempMissile = (cMissile*)CreateGameObject("missile");
			tempMissile->modelData.diffuseColour = object->modelData.diffuseColour;
			tempMissile->modelData.orientation = object->modelData.orientation;
			tempMissile->modelData.orientation2 = object->modelData.orientation2;
			tempMissile->modelData.position = object->modelData.position + glm::vec3(8.5,0,0);
			tempMissile->modelData.meshName = object->missilePlyName;
			tempMissile->modelData.bIsWireFrame = true;
			tempMissile->velocity = glm::vec3(0, 0, 0);
			tempMissile->fired = false;
			//add the missile to the silo
			object->pMissiles.push_back(tempMissile);
			g_pGameObjectMediator->vecGameObjects.push_back(object->pMissiles[i]);

			//TODO add the missile to the vector of objects
		}

	}
}

//get instance //factory pattern
cGameObjectFactory* cGameObjectFactory::GetInstance()
{
	if (instance == nullptr)
		instance = new cGameObjectFactory();
	return instance;
}
