#ifndef _CGROWERSPHERE_HG_
#define _CGROWERSPHERE_HG_
#include "cSphere.h"

//new class based on another class, for demonstrating factory
class cGrowerSphere: public cSphere {
public:
	//ctor and dtor
	cGrowerSphere();
	~cGrowerSphere();
};

#endif // !_CGROWERSPHERE_HG_
