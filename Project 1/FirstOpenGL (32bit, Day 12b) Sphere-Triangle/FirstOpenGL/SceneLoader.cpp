#include <vector>
#include "Utilities.h"
#include <glm/glm.hpp>
#include "cLightManager.h"
#include "cGameObjectFactory.h"
#include "cCollisionManager.h"
#include "cGameObjectMediator.h"

#include "cStaticEnv.h"
#include "cModelData.h"
#include "cSphere.h"

#include <fstream>
#include <string>

extern cLightManager*		g_pLightManager;
extern cGameObjectMediator* g_pGameObjectMediator;


//load the models and light in from a file
bool LoadModelsLightsFromFile()
{
	std::string filename = "ObjectsConfig.txt";
	std::string line;
	std::ifstream inFile(filename.c_str());
	int lightIndex = -1;
	std::vector<std::string> arguments;

	try {
		//go through the file line by line
		while (getline(inFile, line))
		{
			if (line == "")
			{
				continue;
			}
			if (line == "Object Start") {
				continue;
			}
			//build the game object using the builder
			if (line == "Object End") {
				cGameObjectFactory::GetInstance()->AssembleGameObject(
					::g_pGameObjectMediator->vecGameObjects.back(),
					::g_pGameObjectMediator->vecGameObjects.back()->GetType(),
					arguments);

				//for demonstrating singleton
				//cGameObjectFactory* shouldntWork = new cGameObjectFactory();

				/*cGameObjectFactory* A = cGameObjectFactory::GetInstance();
				cGameObjectFactory* B = cGameObjectFactory::GetInstance();*/

				continue;
			}
			//create new light
			if (line == "Light Start") {
				::g_pLightManager->CreateLights(1, true);
				lightIndex++;
				continue;
			}
			//create the new gameobject using the factory
			if (line.find("type:") != std::string::npos) {
				line.replace(0, 6, "");
				::g_pGameObjectMediator->vecGameObjects.push_back(cGameObjectFactory::GetInstance()->CreateGameObject(line));
				arguments.clear();
				continue;
			}
			//set the model for a light
			if (line.find("filename_l:") != std::string::npos) {
				line.replace(0, 12, "");
				::g_pLightManager->vecLights[lightIndex].fileName = line;
				iGameObject* pTempGO = cGameObjectFactory::GetInstance()->CreateGameObject("plane");
				cStaticEnv* psTempGO = (cStaticEnv*)pTempGO;
				cModelData* modelData = psTempGO->GetModelData();

				modelData->position = (::g_pLightManager->vecLights[lightIndex].position);
				::g_pLightManager->vecLights[lightIndex].gameObjectIndex = ::g_pGameObjectMediator->vecGameObjects.size();
				modelData->diffuseColour = (glm::vec4(1.0f, 1.0f, 1.0f, 1.0f));
				modelData->meshName =(line);
				::g_pGameObjectMediator->vecGameObjects.push_back(pTempGO);		// Fastest way to add
				continue;
			}
			//set the attentuation of a light
			if (line.find("attentuation:") != std::string::npos) {
				line.replace(0, 14, "");

				std::string number;
				bool xValue = true;
				for (int stringIndex = 0; stringIndex < line.size(); stringIndex++)
				{
					if (line[stringIndex] != ',')
					{
						number += line[stringIndex];
					}
					else if (xValue)
					{
						xValue = false;
						::g_pLightManager->vecLights[lightIndex].attenuation.x = stof(number);
						number = "";
					}
					else
					{
						::g_pLightManager->vecLights[lightIndex].attenuation.y = stof(number);
						number = "";
					}
				}
				::g_pLightManager->vecLights[lightIndex].attenuation.z = stof(number);
				continue;
			}
			//set the position of a light
			if (line.find("position_l:") != std::string::npos) {
				line.replace(0, 12, "");

				std::string number;
				bool xValue = true;
				for (int stringIndex = 0; stringIndex < line.size(); stringIndex++)
				{
					if (line[stringIndex] != ',')
					{
						number += line[stringIndex];
					}
					else if (xValue)
					{
						xValue = false;
						::g_pLightManager->vecLights[lightIndex].position.x = stof(number);
						number = "";
					}
					else
					{
						::g_pLightManager->vecLights[lightIndex].position.y = stof(number);
						number = "";
					}
				}
				::g_pLightManager->vecLights[lightIndex].position.z = stof(number);
				continue;
				//change the times for the most recent media object
			}
			//set the diffuse colour of a light
			if (line.find("colour_l:") != std::string::npos) {
				line.replace(0, 10, "");

				std::string number;
				bool xValue = true;
				for (int stringIndex = 0; stringIndex < line.size(); stringIndex++)
				{
					if (line[stringIndex] != ',')
					{
						number += line[stringIndex];
					}
					else if (xValue)
					{
						xValue = false;
						::g_pLightManager->vecLights[lightIndex].diffuse.x = stof(number);
						number = "";
					}
					else
					{
						::g_pLightManager->vecLights[lightIndex].diffuse.y = stof(number);
						number = "";
					}
				}
				::g_pLightManager->vecLights[lightIndex].diffuse.z = stof(number);
				continue;
			}
			//push other arguments back, to be used by the builder
			arguments.push_back(line);
		}

	}
	catch (std::exception ex)
	{
		return false;
	}
	return true;
}
