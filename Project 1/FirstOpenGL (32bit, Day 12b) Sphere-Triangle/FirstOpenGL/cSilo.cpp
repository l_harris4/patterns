#include "cSilo.h"
#include "cMissile.h"

cSilo::cSilo()
{
	this->timer = 10;
	this->maxMissiles = 3;
	this->missilePlyName = "";
}

cSilo::~cSilo()
{
	for (int i = 0; i < pMissiles.size(); i++)
	{
		delete pMissiles[i];
	}
}

void cSilo::FireMissile()
{
	for (int i = 0; i < pMissiles.size(); i++)
	{
		if (!pMissiles[i]->fired)
		{
			pMissiles[i]->Fire();
			break;
		}
	}
}

void cSilo::Update(std::vector<iGameObject*>* g_vecGameObjects, double deltaTime)
{
	this->timer -= deltaTime;

	if (timer <= 0)
	{
		FireMissile();
		timer = 10;
	}
}
