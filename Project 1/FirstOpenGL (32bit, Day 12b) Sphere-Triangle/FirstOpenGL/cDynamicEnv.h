#ifndef _CDYNAMICENV_HG_
#define _CDYNAMICENV_HG_
#include "cGameObject.h"
#include "cPhysicsObject.h"

class cDynamicEnv : public cGameObject, public cPhysicsObject
{
public:
	//ctor and dtor
	cDynamicEnv();
	~cDynamicEnv();
	//for updating the object based on time
	virtual void Update(std::vector< iGameObject* >*  g_vecGameObjects, double deltaTime);
};
#endif // !_CDYNAMICENV_HG_

