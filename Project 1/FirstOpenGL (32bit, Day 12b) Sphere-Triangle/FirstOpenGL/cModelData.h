#ifndef _CMODELDATA_HG_
#define _CMODELDATA_HG_

#include <glm\vec3.hpp>
#include <glm\vec4.hpp>
#include <glm\geometric.hpp>
#include <glm/mat4x4.hpp> // glm::mat4
#include <string>

class cModelData
{
public:
	//all data members
	glm::vec3 position;
	glm::vec3 oldPosition;
	glm::mat4x4 worldMatrix;
	float scale;
	std::string meshName;
	glm::vec4 diffuseColour;
	glm::vec4 origColour;
	glm::vec3 orientation;
	glm::vec3 orientation2;
	bool bIsWireFrame;
};
#endif // !_CMODELDATA_HG_

