#ifndef _CSILO_HG_
#define _CSILO_HG_

#include "cGameObject.h"
//#include "cMissile.h"
#include <vector>
#include <string>

//forward declaration of missile
class cMissile;

//new class used to demonstate builder
class cSilo : public cGameObject
{
public:
	//ctor and dtor
	cSilo();
	~cSilo();
	//fire a missile
	void FireMissile();
	//a vector of missiles
	std::vector<cMissile*> pMissiles;
	//timer used for missile firing
	double timer;
	int maxMissiles;
	std::string missilePlyName;
	//update method for updating the silo based on time
	virtual void Update(std::vector< iGameObject* >*  g_vecGameObjects, double deltaTime);
};
#endif // !_CSILO_HG_

