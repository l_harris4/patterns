#include <iostream>
#include "cGameObject.h"
#include "Utilities.h"
#include "cCollisionManager.h"
#include "cCollision.h"
#include "Physics.h"
#include "cVAOMeshManager.h"
#include "cModelData.h"

//ctor
cGameObject::cGameObject()
{
	//set default values
	this->modelData.scale = 1.0f;
	this->modelData.position = glm::vec3(0.0f);
	this->modelData.orientation = glm::vec3(0.0f);
	this->modelData.orientation2 = glm::vec3(0.0f);

	this->modelData.diffuseColour = glm::vec4( 0.0f, 0.0f, 0.0f, 1.0f );
	this->modelData.origColour = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
	this->modelData.worldMatrix = glm::mat4x4(1.0f);

	this->modelData.bIsWireFrame = false;

	return;
}

cGameObject::~cGameObject()
{
	return;
}

void cGameObject::Update(std::vector< iGameObject* >*  g_vecGameObjects, double deltaTime)
{
}

cModelData * cGameObject::GetModelData()
{
	return &modelData;
}

std::string cGameObject::GetType()
{
	return this->typeOfObject;
}

void cGameObject::SetType(std::string value)
{
	this->typeOfObject = value;
}
