#ifndef _cCOLLISIONMANAGER_HG_
#define _cCOLLISIONMANAGER_HG_

#include <vector>
//forward declaration of needed classes
class cCollision;
class cCollisionManagerImp;


class cCollisionManager
{
public:
	//ctor and dtor
	cCollisionManager();
	~cCollisionManager();
	//adding the collision to the vector of collisions
	void AddCollision(cCollision collisionData);

	//clear the vector
	void ClearCollisions();
	//compute all the collisions
	void CalculateCollisions();

private:
	//pointer to the implementation pimpl pattern
	cCollisionManagerImp* pImp;
};

#endif
