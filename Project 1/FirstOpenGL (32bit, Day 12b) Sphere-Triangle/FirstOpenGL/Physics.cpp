#include "Physics.h"
#include "cTriangle.h"
#include "cSphere.h"

#include <glm/glm.hpp>		// glm::distance
#include <glm/geometric.hpp>
#include <math.h>

const float NORMAL_CHECK = 1;
const float Y_ADJUSTMENT = 0.03;
const float PLANE_ADJUSTMENT = 3;
const float LOWER_VELOCITY_RATIO = 0.9f;

//test if spheres are penetrating
bool PenetrationTestSphereSphere(iGameObject* piA, iGameObject* piB)
{
	cSphere* pA = (cSphere*)piA;
	cSphere* pB = (cSphere*)piB;

	float totalRadii = pA->radius + pB->radius;

	// The Pythagorean distance 
	float distance = glm::distance(pA->modelData.position, pB->modelData.position);

	//if the distance is less than the total radii then they are colliding
	if (distance <= totalRadii)
	{
		return true;
	}

	return false;
}

int roundFloat(float f)
{
	return floor(f* 100000);
}

bool equalVector(glm::vec3 first, glm::vec3 second)
{
	return roundFloat(first.x) == roundFloat(second.x) && roundFloat(first.y) == roundFloat(second.y) && roundFloat(first.z) == roundFloat(second.z);
}

//calculate the velocity of a sphere after a collision with triangles
void calcTrianglesSphereCollision(iGameObject * sphereiObject, std::vector<std::pair<cPhysTriangle, iGameObject*>> &theTriangles)
{
	cSphere* sphereObject = (cSphere*)sphereiObject;
	glm::vec3 finalNormal = glm::vec3(0.0f, 0.0f, 0.0f);
	int trianglesSize = theTriangles.size();
	

	//go through all the triangles
	for (int triIndex = 0; triIndex < trianglesSize; triIndex++)
	{
		//current triangle and object that the triangle belongs to
		cPhysTriangle* curTriangle = &theTriangles[triIndex].first;
		cGameObject * curObject = (cGameObject*)theTriangles[triIndex].second;

		//if the object could have moved, then apply the objects world matrix to all the vertices
		if (curObject->editable) {
			
			glm::vec4 tempVec1(curTriangle->vertex[0].x, curTriangle->vertex[0].y, curTriangle->vertex[0].z, 1);
			tempVec1 = curObject->modelData.worldMatrix * tempVec1;

			glm::vec4 tempVec2(curTriangle->vertex[1].x, curTriangle->vertex[1].y, curTriangle->vertex[1].z, 1);
			tempVec2 = curObject->modelData.worldMatrix * tempVec2;

			glm::vec4 tempVec3(curTriangle->vertex[2].x, curTriangle->vertex[2].y, curTriangle->vertex[2].z, 1);
			tempVec3 = curObject->modelData.worldMatrix * tempVec3;

			curTriangle->vertex[0] = glm::vec3(tempVec1.x, tempVec1.y, tempVec1.z);
			curTriangle->vertex[1] = glm::vec3(tempVec2.x, tempVec2.y, tempVec2.z);
			curTriangle->vertex[2] = glm::vec3(tempVec3.x, tempVec3.y, tempVec3.z);
		}

		//calculate the normal of the triangle and add it to the final normal
		glm::vec3 U = curTriangle->vertex[1] - curTriangle->vertex[0];
		glm::vec3 V = curTriangle->vertex[2] - curTriangle->vertex[0];
		glm::vec3 normal = glm::cross(U, V);

		normal = glm::normalize(normal);

		finalNormal += normal;
		
	}

	//normalize the final normal
	finalNormal = glm::normalize(finalNormal);

	sphereObject->velocity = glm::reflect(sphereObject->velocity, finalNormal) *LOWER_VELOCITY_RATIO;

	if (sphereObject->hittingGround)
	{
		sphereObject->modelData.position = sphereObject->resetPosition;
	}
}

//calculate the resulting velocity of a sphere colliding with other spheres
void BounceCalcSphereSpheres(iGameObject * mainiObject, std::vector<iGameObject*> otherObjects)
{
	cSphere* mainObject = (cSphere*)mainiObject;
	glm::vec3 resultingVel = glm::vec3(0.0f, 0.0f, 0.0f);
	int objectsSize = otherObjects.size();

	//go through and total the resulting velocity of interacting with the other spheres
	for (int objectIndex = 0; objectIndex < otherObjects.size(); objectIndex++)
	{
		cSphere* otherSphere = (cSphere*)otherObjects[objectIndex];

		glm::vec3 n = mainObject->modelData.oldPosition - otherSphere->modelData.oldPosition;
		n = glm::normalize(n);

		float a1 = glm::dot(mainObject->holderVelocity, n);
		float a2 = glm::dot(otherSphere->holderVelocity, n);

		float optimizedP = (2.0 * (a1 - a2)) / (2); // mass of 2 assumed

		glm::vec3 v1 = mainObject->holderVelocity - optimizedP * 1 * n;   //1 is a placeholder for mass

		resultingVel += v1;
	}

	mainObject->velocity = resultingVel;
	mainObject->modelData.position = mainObject->resetPosition;
}


//find the closest point to a triangle
glm::vec3 cPhysTriangle::ClosestPtPointTriangle(glm::vec3 p)
{
	return this->ClosestPtPointTriangle(p, this->vertex[0],
		this->vertex[1],
		this->vertex[2]);
}

//find the closest point to a triangle
glm::vec3 cPhysTriangle::ClosestPtPointTriangle(glm::vec3 p, glm::vec3 a,
	glm::vec3 b, glm::vec3 c)
{
	// Check if P in vertex region outside A
	glm::vec3 ab = b - a;
	glm::vec3 ac = c - a;
	glm::vec3 ap = p - a;
	float d1 = glm::dot(ab, ap);
	float d2 = glm::dot(ac, ap);
	if (d1 <= 0.0f && d2 <= 0.0f) return a;

	// Check if P in vertex region outside B
	glm::vec3 bp = p - b;
	float d3 = glm::dot(ab, bp);
	float d4 = glm::dot(ac, bp);
	if (d3 >= 0.0f && d4 <= d3) return b;

	// Check if P in edge region of AB, if so return projection of P onto AB
	float vc = d1*d4 - d3*d2;
	if (vc <= 0.0f && d1 >= 0.0f && d3 <= 0.0f) {
		float v = d1 / (d1 - d3);
		return a + v * ab; 
	}

	// Check if P in vertex region outside C
	glm::vec3 cp = p - c;
	float d5 = glm::dot(ab, cp);
	float d6 = glm::dot(ac, cp);
	if (d6 >= 0.0f && d5 <= d6) return c;

	// Check if P in edge region of AC, if so return projection of P onto AC
	float vb = d5*d2 - d1*d6;
	if (vb <= 0.0f && d2 >= 0.0f && d6 <= 0.0f) {
		float w = d2 / (d2 - d6);
		return a + w * ac; // barycentric coordinates (1-w,0,w)
	}

	// Check if P in edge region of BC, if so return projection of P onto BC
	float va = d3*d6 - d5*d4;
	if (va <= 0.0f && (d4 - d3) >= 0.0f && (d5 - d6) >= 0.0f) {
		float w = (d4 - d3) / ((d4 - d3) + (d5 - d6));
		return b + w * (c - b); // barycentric coordinates (0,1-w,w)
	}

	// P inside face region. Compute Q through its barycentric coordinates (u,v,w)
	float denom = 1.0f / (va + vb + vc);
	float v = vb * denom;
	float w = vc * denom;
	return a + ab * v + ac * w; // = u*a + v*b + w*c, u = va * denom = 1.0f - v - w
}